package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anandtech.xbo.R;

import java.util.List;

import io.fabric.sdk.android.Fabric;

public class TransactionPageDetailsActivity extends AppCompatActivity {

    public String navAmountStr = "",navTransactionType = "",navDateTimeStr = "",navNameStr = "",userNameStr = "",
            navRemainingBalnceStr = "",navPaymentStatusStr = "",navTransactionIdStr = "", navFundingType = "";
    public TextView xboAmountTxt,fromNameTxt,toNameTxt,statusTxt,toolbatTitleTxt,transferIdTxt,mainHeading,dateTxt,
            remainingBlanceTxt;
    public RelativeLayout supportBtn;
    public ImageView backBtn,fromImg,paymentStatusImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_page_details);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }
        //Checking forced crash for crashlytics firebase report
        Fabric.with(TransactionPageDetailsActivity.this, new Crashlytics());


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbatTitleTxt = (TextView) toolbar.findViewById(R.id.toolbar_txt);
        backBtn = (ImageView) toolbar.findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbatTitleTxt.setText("Transaction Details");

        userNameStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserName(TransactionPageDetailsActivity.this)));

        Intent intent = getIntent();
        if (intent != null)
        {
            navAmountStr = intent.getStringExtra("Amount");
            navTransactionType = intent.getStringExtra("TransactionType");
            navNameStr = intent.getStringExtra("Name");
            navRemainingBalnceStr = intent.getStringExtra("RemainingBalance");
            navDateTimeStr = intent.getStringExtra("DateTime");
            navPaymentStatusStr = intent.getStringExtra("PaymentStatus");
            navTransactionIdStr = intent.getStringExtra("TransactionID");
            navFundingType = intent.getStringExtra("FundingType");
        }

        xboAmountTxt = (TextView) findViewById(R.id.xbo_amount);
        fromNameTxt = (TextView) findViewById(R.id.from_name);
        toNameTxt = (TextView) findViewById(R.id.to_name);
        statusTxt = (TextView) findViewById(R.id.status);
        transferIdTxt = (TextView) findViewById(R.id.transferId);
        mainHeading = (TextView) findViewById(R.id.main_heading);
        dateTxt = (TextView) findViewById(R.id.datetxt);
        remainingBlanceTxt = (TextView) findViewById(R.id.remaningBlnce);
        fromImg = (ImageView) findViewById(R.id.from_image);
        paymentStatusImg = (ImageView) findViewById(R.id.icon_payment);
        supportBtn = (RelativeLayout) findViewById(R.id.support_btn);


        transferIdTxt.setText("Transfer ID : " + navTransactionIdStr);
        dateTxt.setText(navDateTimeStr);
        remainingBlanceTxt.setText(navRemainingBalnceStr);

        if (navFundingType.equalsIgnoreCase("XboWallet")) {
            if (navNameStr.equalsIgnoreCase("Wallet")) {
                /*xboAmountTxt.setText("+" + navAmountStr);
                xboAmountTxt.setTextColor(Color.parseColor("#08b3d3"));*/

                if (navPaymentStatusStr.equalsIgnoreCase("Success")) {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#1fc812"));
                }else if (navPaymentStatusStr.equalsIgnoreCase("Pending"))
                {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#ff9800"));

                }else
                {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#fb1b1b"));
                }
                mainHeading.setText("XBO added to your " + navNameStr);

                fromNameTxt.setText("Funding");
                toNameTxt.setText("Your Wallet");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fromImg.setBackgroundResource(R.drawable.ic_bank);
                }

            } else {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fromImg.setBackgroundResource(R.drawable.ic_send);
                }

                if (navTransactionType.equalsIgnoreCase("true")) {

                  /*  xboAmountTxt.setText("-" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#fb1b1b"));*/
                    if (navPaymentStatusStr.equalsIgnoreCase("Success")) {
                        xboAmountTxt.setText("-" + navAmountStr);
                        xboAmountTxt.setTextColor(Color.parseColor("#1fc812"));
                    }else if (navPaymentStatusStr.equalsIgnoreCase("Pending"))
                    {
                        xboAmountTxt.setText("-" + navAmountStr);
                        xboAmountTxt.setTextColor(Color.parseColor("#ff9800"));

                    }else
                    {
                        xboAmountTxt.setText("-" + navAmountStr);
                        xboAmountTxt.setTextColor(Color.parseColor("#fb1b1b"));
                    }

                    mainHeading.setText("XBO sent to " + navNameStr);

                    toNameTxt.setText(navNameStr);
                    fromNameTxt.setText("Your Wallet");

                } else {
                   /* xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#1fc812"));*/

                    if (navPaymentStatusStr.equalsIgnoreCase("Success")) {
                        xboAmountTxt.setText("+" + navAmountStr);
                        xboAmountTxt.setTextColor(Color.parseColor("#1fc812"));
                    }else if (navPaymentStatusStr.equalsIgnoreCase("Pending"))
                    {
                        xboAmountTxt.setText("+" + navAmountStr);
                        xboAmountTxt.setTextColor(Color.parseColor("#ff9800"));

                    }else
                    {
                        xboAmountTxt.setText("+" + navAmountStr);
                        xboAmountTxt.setTextColor(Color.parseColor("#fb1b1b"));
                    }
                    mainHeading.setText("XBO received from " + navNameStr);

                    fromNameTxt.setText(navNameStr);
                    toNameTxt.setText("Your Wallet");

                }
            }
        }else if (navFundingType.equalsIgnoreCase("Bank"))
        {

            if (navPaymentStatusStr.equalsIgnoreCase("Success")) {
                xboAmountTxt.setText("+" + navAmountStr);
                xboAmountTxt.setTextColor(Color.parseColor("#1fc812"));
            }else if (navPaymentStatusStr.equalsIgnoreCase("Pending"))
            {
                xboAmountTxt.setText("+" + navAmountStr);
                xboAmountTxt.setTextColor(Color.parseColor("#ff9800"));

            }else
                {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#fb1b1b"));
                }

            mainHeading.setText("Voucher Received From " + navNameStr);

            toNameTxt.setText("Your Wallet");
            fromNameTxt.setText(navNameStr);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fromImg.setBackgroundResource(R.drawable.ic_bank);
            }

        }else
            {
                if (navPaymentStatusStr.equalsIgnoreCase("Success")) {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#1fc812"));
                }else if (navPaymentStatusStr.equalsIgnoreCase("Pending"))
                {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#ff9800"));

                }else
                {
                    xboAmountTxt.setText("+" + navAmountStr);
                    xboAmountTxt.setTextColor(Color.parseColor("#fb1b1b"));
                }

                mainHeading.setText("XBO Received From " + navNameStr);

                toNameTxt.setText("Your Wallet");
                fromNameTxt.setText(navNameStr);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fromImg.setBackgroundResource(R.drawable.ic_crypto);
                }


            }


        if (navPaymentStatusStr.equalsIgnoreCase("Success")) {
            statusTxt.setText(navPaymentStatusStr);
            statusTxt.setTextColor(Color.parseColor("#1fc812"));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            paymentStatusImg.setBackgroundResource(R.drawable.ic_success);
        }else if (navPaymentStatusStr.equalsIgnoreCase("Pending"))
        {
            statusTxt.setText(navPaymentStatusStr);
            statusTxt.setTextColor(Color.parseColor("#ff9800"));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            paymentStatusImg.setBackgroundResource(R.drawable.ic_pen);
        }else
        {
           statusTxt.setText(navPaymentStatusStr);
            statusTxt.setTextColor(Color.parseColor("#fb1b1b"));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            paymentStatusImg.setBackgroundResource(R.drawable.ic_transaction_failed);
        }

        supportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TransactionPageDetailsActivity.this, SupportActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(TransactionPageDetailsActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(TransactionPageDetailsActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(TransactionPageDetailsActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(TransactionPageDetailsActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(TransactionPageDetailsActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(TransactionPageDetailsActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
        finish();
    }
}
