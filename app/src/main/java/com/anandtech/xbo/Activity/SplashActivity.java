package com.anandtech.xbo.Activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Model.AppVersionModel;
import com.anandtech.xbo.Model.CounterySpinnerModerl;
import com.anandtech.xbo.R;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.NetworkStateReceiver;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;

public class SplashActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    public TextView splashTxt;
    public String status = "";
    //font Path
    public String fontPath = "Font/CORBEL.TTF";
    public String counteryUrl = SettingConstant.BASEURL + "api/UserWebApi/CountriesList";
    public ArrayList<CounterySpinnerModerl> counteryList = new ArrayList<>();
    public ConnectionDetector conn;
    public RetrofitInterface retrofitClientInstance;
    public String currentVersion = "";
    private NetworkStateReceiver networkStateReceiver;
    private  AlertDialog internetDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Checking forced crash for crashlytics firebase report
        Fabric.with(SplashActivity.this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        internetDialog = new AlertDialog.Builder(SplashActivity.this).create();


        conn = new ConnectionDetector(SplashActivity.this);
        status = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getLoginStatus(SplashActivity.this)));
        retrofitClientInstance = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);

        //get APP Current Version
        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        splashTxt = (TextView) findViewById(R.id.splash_quote);

        // Loading Font Face
        Typeface m_typeFace = Typeface.createFromAsset(getAssets(), fontPath);

        // Applying font
        splashTxt.setTypeface(m_typeFace);
        counteryList = SharedPrefs.getCountryList(SplashActivity.this);


    }

    public void getAppVersion()
    {
        Call<AppVersionModel> call = retrofitClientInstance.getAppVersion();
        call.enqueue(new Callback<AppVersionModel>() {
            @Override
            public void onResponse(Call<AppVersionModel> call, retrofit2.Response<AppVersionModel> response) {

               // Log.e("Checking the update", currentVersion + " Null");

                try {
                    if (response.body().getStatus().equalsIgnoreCase("1"))
                    {

                        if (Double.parseDouble(currentVersion) < Double.parseDouble(response.body().getAppVersion()))
                        {
                            showUpdateDialog();
                        }else
                            {
                                if (counteryList == null)
                                {
                                    if (conn.getConnectivityStatus()>0)
                                    {
                                        //call Api
                                        getCounteryName();
                                    }else
                                    {
                                        conn.showNoInternetAlret();
                                    }
                                }else
                                {
                                    switchingPage();
                                }
                            }
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AppVersionModel> call, Throwable t) {

            }
        });
    }

    //API  Calling
    public void getCounteryName()
    {
        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, counteryUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    counteryList = new ArrayList<>();
                    counteryList.add(new CounterySpinnerModerl("0","Please Select Country"));

                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String name = object.getString("name");

                        counteryList.add(new CounterySpinnerModerl(id,name));
                    }

                    //save the countryList
                    SharedPrefs.setCountryList(SplashActivity.this,counteryList);

                    switchingPage();

                    //pDialog.dismiss();

                } catch (JSONException e) {
                  //  Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());


            }
        });
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }

    public void switchingPage()
    {
        //handler Working
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (status.equalsIgnoreCase("1"))
                {
                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                    finish();
                }else {
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                    finish();
                }
            }
        }, 1000);
    }


    private void showUpdateDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.update_message);
        builder.setPositiveButton("Compulsory  Update", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                        ("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=it")));
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
                //background.start();
            }
        });

        builder.setCancelable(false);
        // create alert dialog
        AlertDialog alertDialog = builder.create();

        // show it
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }




    @Override
    public void networkAvailable() {

        //calling Api to check app version
        getAppVersion();
        internetDialog.dismiss();


    }

    @Override
    public void networkUnavailable() {

        showInternetPopUp();
    }

   private void showInternetPopUp()
    {
        // Setting Dialog Title
        internetDialog.setTitle("Connection Failed");
        internetDialog.setMessage("Something is wrong with your internet connection. Please check your internet connection");
        internetDialog.setCancelable(false);


     /*   // Setting OK Button
        internetDialog.setButton(Dialog.BUTTON_POSITIVE,"Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //internetDialog.dismiss();
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });*/
        // Showing Alert Message
        internetDialog.show();


    }
}
