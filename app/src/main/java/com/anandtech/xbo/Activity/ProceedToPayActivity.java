package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.PowerManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class ProceedToPayActivity extends AppCompatActivity {
    public ImageView backBtn;
    public TextView toolbatTitleTxt,tokenAmountTxt,totalVoucherTxt;
    public String sendTokenAPI = SettingConstant.BASEURL + "api/WalletApi/sendmoney";
    public String updateBalanceUrl = SettingConstant.BASEURL + "api/UserWebApi/UserTotalBalanceByuserID";
    public String  reciverWalletAddresStr = "",senderWalletAddressStr = "",userId = "",authKey="";
    public ConnectionDetector conn;
    public EditText amountTokenTxt;
    public Button subBtn;
    public TextView walletAddTxt, convertTxt,totalROITxt;
    public LinearLayout totalLay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proceed_to_pay);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        //Checking forced crash for crashlytics firebase report
        Fabric.with(ProceedToPayActivity.this, new Crashlytics());

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        toolbatTitleTxt = (TextView) toolbar.findViewById(R.id.toolbar_txt);
        backBtn = (ImageView) toolbar.findViewById(R.id.back_btn);
        setSupportActionBar(toolbar);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbatTitleTxt.setText("Receiver Information");

        Intent intent = getIntent();
        if (intent != null)
        {
            reciverWalletAddresStr = intent.getStringExtra("WalletAddres");
        }

        conn = new ConnectionDetector(ProceedToPayActivity.this);

        senderWalletAddressStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getWalletAddress(ProceedToPayActivity.this)));
        userId =UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(ProceedToPayActivity.this)));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(ProceedToPayActivity.this)));

        amountTokenTxt = (EditText) findViewById(R.id.amount_of_token);
        walletAddTxt = (TextView) findViewById(R.id.wallet_add_txt);
        tokenAmountTxt = (TextView)findViewById(R.id.token_amount);
        totalVoucherTxt = (TextView) findViewById(R.id.total_voucher);
        totalROITxt = (TextView) findViewById(R.id.total_roi);
        convertTxt = (TextView) findViewById(R.id.convert_txt);
        totalLay = (LinearLayout) findViewById(R.id.total_roi_lay);
        subBtn = (Button) findViewById(R.id.btn_lay);

        walletAddTxt.setText(reciverWalletAddresStr);

        amountTokenTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    double value = Double.parseDouble(s.toString())/10;
                    convertTxt.setText("Equivalent USD : " + value);
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    convertTxt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (amountTokenTxt.getText().toString().equalsIgnoreCase(""))
                {
                    amountTokenTxt.setError("Please enter XBO Token");
                    amountTokenTxt.requestFocus();
                }else
                    {
                        if (conn.getConnectivityStatus()>0)
                        {
                            sendTokenToReciver(senderWalletAddressStr,reciverWalletAddresStr,
                                    amountTokenTxt.getText().toString().trim(),userId,currentDate(),authKey);
                        }else
                        {
                            conn.showNoInternetAlret();
                        }
                    }
            }
        });


        //call api to update my balance
        if (conn.getConnectivityStatus()>0)
        {
            updateBalance(userId,authKey);
        }else
            {
                conn.showNoInternetAlret();
            }



    }


    /* Working with api*/
    public void sendTokenToReciver(final String senderWalletAdd, final String reciverWalletAdd, final String amount,
                                   final String userId, final String date, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(ProceedToPayActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, sendTokenAPI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Send Token", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (Status.equalsIgnoreCase("1"))
                    {

                        JSONObject object = jsonObject.getJSONObject("RecieverData");
                        String Name = object.getString("Name");
                        String Amount = object.getString("Amount");
                        String TransId = object.getString("TransId");
                        String CreatedDatetime = object.getString("CreatedDatetime");

                        showSuccessPop(Name,Amount,TransId,CreatedDatetime);

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        conn.showLogoutDialog(message,ProceedToPayActivity.this);
                    }else
                    {
                        Toast.makeText(ProceedToPayActivity.this, message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("SenderWalletAddress", senderWalletAdd);
                params.put("RecieverWalletAddress", reciverWalletAdd);
                params.put("Amount", amount);
                params.put("SenderuserId", userId);
                params.put("TransactionDateTime", date);
                params.put("LoginAuthKey",authKey);

                Log.e("Checking Params", params.toString());
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Send Token");
    }

    //update balance API
    public void updateBalance(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(ProceedToPayActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, updateBalanceUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("UpdateBalance", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (Status.equalsIgnoreCase("1"))
                    {
                      String UserTotalBalance = jsonObject.getString("UserTotalBalance");
                      String TotalVoucher = jsonObject.getString("TotalVoucher");
                        String PlotROI = jsonObject.getString("PlotROI");
                      String  sendBoxStatus = jsonObject.getString("SendxboStatus");
                      tokenAmountTxt.setText(UserTotalBalance);
                      totalVoucherTxt.setText(TotalVoucher);
                        totalROITxt.setText(PlotROI);

                        //ROI XBO
                        if (sendBoxStatus.equalsIgnoreCase("1"))
                            totalLay.setVisibility(View.VISIBLE);
                        else
                            totalLay.setVisibility(View.GONE);

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        conn.showLogoutDialog(message,ProceedToPayActivity.this);
                    }else
                    {
                        Toast.makeText(ProceedToPayActivity.this, message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("LoginAuthKey",authKey);

                Log.e("Checking Params", params.toString());
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Send Token");
    }


    private void showSuccessPop(String name, String amount, String transactionId, String date) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(ProceedToPayActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.success_popup_window_layout, null);

        TextView nameTxt = (TextView)view.findViewById(R.id.xbo_name_txt);
        TextView amountTxt = (TextView) view.findViewById(R.id.amount);
        TextView transferId = (TextView) view.findViewById(R.id.transferId);
        TextView dateTime = (TextView) view.findViewById(R.id.date_time);
        Button okBtn = (Button) view.findViewById(R.id.back_to_home);

        amountTxt.setText(amount);
        nameTxt.setText(name);
        transferId.setText("Transfer ID: "+transactionId);
        dateTime.setText(convertDateForTransactionHistory(date));



        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
                ad.dismiss();
            }
        });
    }

    //convert date time to month
    public String convertDateForTransactionHistory(String dtStart)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy' | 'hh:mm");

            String date1 = sdf.format(date);

            return date1;


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(ProceedToPayActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(ProceedToPayActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(ProceedToPayActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(ProceedToPayActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ProceedToPayActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ProceedToPayActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    @Override
    public void onBackPressed() {
      super.onBackPressed();
      finish();

    }
}
