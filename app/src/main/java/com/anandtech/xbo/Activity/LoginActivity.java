package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.BuildConfig;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.PowerManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity {

    //font Path
    public String fontPath = "fonts/Adlanta.otf";
    public Button registerBtn,loginBtn;
    public EditText emailIdTxt, passwordTxt;
    public ConnectionDetector conn;
    public String loginUrl = SettingConstant.BASEURL + "api/UserWebApi/Login";
    public TextView forgotPassTxt;
    public ImageView showPassImg;
    public boolean flag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        //Checking forced crash for crashlytics firebase report
        Fabric.with(LoginActivity.this, new Crashlytics());


        conn = new ConnectionDetector(LoginActivity.this);

        registerBtn = (Button) findViewById(R.id.register_Btn);
        loginBtn = (Button) findViewById(R.id.login_btn);
        emailIdTxt = (EditText) findViewById(R.id.emailIdTxt);
        passwordTxt = (EditText) findViewById(R.id.password_txt);
        forgotPassTxt = (TextView) findViewById(R.id.forgotpass);
        showPassImg = (ImageView) findViewById(R.id.show_pass_btn);

        showPassImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag)
                {
                    showPassImg.setImageResource(R.drawable.ic_hide_pass);
                    passwordTxt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    passwordTxt.setSelection(passwordTxt.getText().length());
                    flag = false;

                }else
                    {
                        showPassImg.setImageResource(R.drawable.ic_show_pass);
                        passwordTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                        passwordTxt.setSelection(passwordTxt.getText().length());
                        flag = true;
                    }

            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isValidEmail(emailIdTxt.getText().toString().trim()))
                {
                    emailIdTxt.setError("Please Enter Valid Email ID");
                    emailIdTxt.requestFocus();

                }else if (passwordTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    passwordTxt.setError("Please enter password");
                    passwordTxt.requestFocus();

                }else {

                    if (conn.getConnectivityStatus()>0)
                    {
                        loginUser(emailIdTxt.getText().toString().trim(),passwordTxt.getText().toString(),currentDate());
                    }else
                        {
                            conn.showNoInternetAlret();
                        }
                }
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);

            }
        });

        forgotPassTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                //openWhatsApp();

            }
        });
    }

    public void loginUser(final String emailId , final String password, final String date)
    {
        final ProgressDialog pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, loginUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("LoginUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        JSONObject object = jsonObject.getJSONObject("Authdata");
                        String UserId = object.getString("UserId");
                        String ReferenceId = object.getString("ReferenceId");
                        String IsVerifid = object.getString("IsVerifid");
                        String EmailId = object.getString("EmailId");
                        String Name = object.getString("Name");
                        String Qrcode = object.getString("Qrcode");
                        String UserBalance = object.getString("UserBalance");
                        String LoginAuthkey = object.getString("LoginAuthkey");
                        String passcode = object.getString("passcode");
                        String UserTypeName = object.getString("UserTypeName");
                        String UserTypeId = object.getString("UserTypeId");

                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setLoginStatus(LoginActivity.this,
                                "1")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserId(LoginActivity.this,
                                UserId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setRefrencId(LoginActivity.this,
                                ReferenceId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setVerified(LoginActivity.this,
                                IsVerifid)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setEmailId(LoginActivity.this,
                                EmailId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setOldPassword(LoginActivity.this,
                                password)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserName(LoginActivity.this,
                                Name)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAddress(LoginActivity.this,
                                Qrcode)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAmount(LoginActivity.this,
                                UserBalance)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAmount(LoginActivity.this,
                                UserBalance)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setAuthKey(LoginActivity.this,
                                LoginAuthkey)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setPassCode(LoginActivity.this,
                                passcode)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeName(LoginActivity.this,
                                UserTypeName)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeID(LoginActivity.this,
                                UserTypeId)));

                        if (!passcode.equalsIgnoreCase("null")) {

                            Intent intent = new Intent(LoginActivity.this, PassCodeActivity.class);
                            intent.putExtra("Nav","4");
                            startActivity(intent);

                        }else
                            {
                                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                                finish();
                            }

                    }else if (Status.equalsIgnoreCase("2"))
                    {
                        showLogoutMessage(Message);

                    }else if (Status.equalsIgnoreCase("3"))
                    {

                        JSONObject object = jsonObject.getJSONObject("Authdata");
                        String UserId = object.getString("UserId");
                        String ReferenceId = object.getString("ReferenceId");
                        String IsVerifid = object.getString("IsVerifid");
                        String EmailId = object.getString("EmailId");

                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setLoginStatus(LoginActivity.this,
                                "1")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserId(LoginActivity.this,
                                UserId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setRefrencId(LoginActivity.this,
                                ReferenceId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setVerified(LoginActivity.this,
                                IsVerifid)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setEmailId(LoginActivity.this,
                                EmailId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setOldPassword(LoginActivity.this,
                                password)));


                        showCustomDialog(Message);


                        //Toast.makeText(LoginActivity.this, Message, Toast.LENGTH_SHORT).show();
                    }else
                        {
                            Toast.makeText(LoginActivity.this, Message, Toast.LENGTH_SHORT).show();
                        }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("EmailId", emailId);
                params.put("password", password);
                params.put("LoginDateTime", date);

                Log.e("Checking params ", params.toString());

                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Login");

    }

    /* validation method*/
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void showCustomDialog(String message) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

                Intent intent = new Intent(LoginActivity.this, VerificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);

            }
        });
    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }

    private void showLogoutMessage(String message) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(LoginActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

            }
        });
    }

}
