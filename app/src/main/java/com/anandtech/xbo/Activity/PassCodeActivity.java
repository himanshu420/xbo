package com.anandtech.xbo.Activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Model.PasscodeModel;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Vibrator;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.R;

import io.fabric.sdk.android.Fabric;
import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassCodeActivity extends AppCompatActivity {

    public static final String TAG = "PinLockView";
    public EditText emailIdTxt;
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private ShimmerLayout shimmerFrameLayout;
    private String TRUE_CODE, navStr = "",userId = "";
    public FrameLayout forgotLay;
    public LinearLayout passLay;
    public TextView forgotbtn;
    public ImageView backBtn;
    public Button sendBtn;
    public ConnectionDetector conn;
    public RetrofitInterface retrofitClientInstance;
    public Vibrator vib;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.content_pass_code);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

      /*  //android O fix bug orientation
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }*/

        //Checking forced crash for crashlytics firebase report
        Fabric.with(PassCodeActivity.this, new Crashlytics());

        conn = new ConnectionDetector(PassCodeActivity.this);
        retrofitClientInstance = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);
        TRUE_CODE = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(PassCodeActivity.this)));
        userId = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(PassCodeActivity.this)));
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        Intent intent = getIntent();
        if (intent != null)
        {
            navStr = intent.getStringExtra("Nav");
        }


        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        shimmerFrameLayout = (ShimmerLayout) findViewById(R.id.shimmer_view_container);
        forgotLay = (FrameLayout) findViewById(R.id.forgot_lay);
        passLay = (LinearLayout) findViewById(R.id.pass_lay);
        forgotbtn = (TextView) findViewById(R.id.forgotBtn);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        emailIdTxt = (EditText) findViewById(R.id.emailid);
        sendBtn = (Button) findViewById(R.id.sendBtn);

        shimmerFrameLayout.startShimmerAnimation();

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);

        mPinLockView.setPinLength(4);

        forgotbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                forgotLay.setVisibility(View.VISIBLE);
                passLay.setVisibility(View.GONE);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                forgotLay.setVisibility(View.GONE);
                passLay.setVisibility(View.VISIBLE);
            }
        });


        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isValidEmail(emailIdTxt.getText().toString().trim()))
                {
                    emailIdTxt.setError("Please enter valid email id");
                    emailIdTxt.requestFocus();
                }else
                    {
                        if (conn.getConnectivityStatus()>0)
                        {
                            forgotPassCode(emailIdTxt.getText().toString().trim(),userId);
                        }else
                            {
                                conn.showNoInternetAlret();
                            }
                    }
            }
        });
       // mPinLockView.setTextColor(ContextCompat.getColor(this, R.color.white));

        // mIndicatorDots.setBackgroundColor(getResources().getColor(R.color.blue_color));
       // mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mPinLockView.resetPinLockView();
    }

    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {
            Log.e(TAG, "Pin complete: " + pin);

            if (pin.length() == 4)
            {

                if (pin.equalsIgnoreCase(TRUE_CODE))
                {
                    Toast.makeText(PassCodeActivity.this, "Passcode Verify Successfully", Toast.LENGTH_SHORT).show();
                    setIsPass();

                    if (navStr.equalsIgnoreCase("1")) {
                        finish();
                    }else if (navStr.equalsIgnoreCase("4"))
                    {
                        Intent intent = new Intent(PassCodeActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                        finish();

                    }else if (navStr.equalsIgnoreCase("5"))
                    {
                         Intent intent = new Intent(PassCodeActivity.this, ResetPasscodeActivity.class);
                         intent.putExtra("Nav","4");
                         startActivity(intent);
                    }else
                        {
                            //finish();
                            Intent intent = new Intent(PassCodeActivity.this, ResetPasscodeActivity.class);
                            intent.putExtra("Nav","3");
                            startActivity(intent);

                        }

                    UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(PassCodeActivity.this,
                            "False")));
                }else
                    {
                        Toast.makeText(PassCodeActivity.this, "Wrong Pass code", Toast.LENGTH_SHORT).show();
                        //vibrate the dots layout

                        mPinLockView.resetPinLockView();
                        vib.vibrate(1000);
                        shakeAnimation();
                    }
            }
        }

        @Override
        public void onEmpty() {
            Log.e(TAG, "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {
            Log.e(TAG, "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };


    private void setIsPass() {
        SharedPreferences.Editor editor = getSharedPreferences("PASS_CODE", MODE_PRIVATE).edit();
        editor.putBoolean("is_pass", true);
        editor.apply();
    }

    private void shakeAnimation() {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
        findViewById(R.id.indicator_dots).startAnimation(shake);
        Toast.makeText(this, "Wrong Password", Toast.LENGTH_SHORT).show();
    }


    public void forgotPassCode(String emailId, String userId)
    {
        final ProgressDialog pDialog = new ProgressDialog(PassCodeActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<PasscodeModel> call = retrofitClientInstance.forgotPassCode(emailId,userId);
        call.enqueue(new Callback<PasscodeModel>() {
            @Override
            public void onResponse(Call<PasscodeModel> call, Response<PasscodeModel> response) {

                try {
                    if(!((Activity) PassCodeActivity.this).isFinishing()) {
                        showCustomDialog(response.body().getMessage(),response.body().getStatus());
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<PasscodeModel> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }


    private void showCustomDialog(String message, final String status) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PassCodeActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

                if (status.equalsIgnoreCase("1")) {
                    forgotLay.setVisibility(View.GONE);
                    passLay.setVisibility(View.VISIBLE);
                }else
                    {
                        emailIdTxt.setText("");
                    }


            }
        });
    }


    /* validation method*/
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //android.os.Process.killProcess(android.os.Process.myPid());
       /* System.gc();
        System.exit(0);*/
    }
}
