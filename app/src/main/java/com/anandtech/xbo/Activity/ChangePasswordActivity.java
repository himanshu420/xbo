package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.os.PowerManager;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.anandtech.xbo.R;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class ChangePasswordActivity extends AppCompatActivity {
    public TextView toolbatTitleTxt;
    public ImageView backBtn,showOldPassBtn,showNewPassBtn,showConfPassBtn;
    public EditText newPassTxt, confirmNewPassTxt,oldPasswordTxt;
    public Button changePassBtn;
    public String changePassUrl = SettingConstant.BASEURL + "api/UserWebApi/ChangePassword";
    public ConnectionDetector conn;
    public String userId = "",oldPasswordStr = "",authKey = "";
    public boolean flag1 = true,flag2 = true,flag3 = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        //Checking forced crash for crashlytics firebase report
        Fabric.with(ChangePasswordActivity.this, new Crashlytics());

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbatTitleTxt = (TextView) toolbar.findViewById(R.id.toolbar_txt);
        backBtn = (ImageView) toolbar.findViewById(R.id.back_btn);
        setSupportActionBar(toolbar);

        conn = new ConnectionDetector(ChangePasswordActivity.this);
        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(ChangePasswordActivity.this)));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(ChangePasswordActivity.this)));
        oldPasswordStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getOldPassword(ChangePasswordActivity.this)));


        toolbatTitleTxt.setText("CHANGE PASSWORD");

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        newPassTxt = (EditText) findViewById(R.id.new_password);
        confirmNewPassTxt = (EditText) findViewById(R.id.confirm_new_password);
        oldPasswordTxt = (EditText) findViewById(R.id.old_password);
        changePassBtn = (Button) findViewById(R.id.change_pass_btn);
        showOldPassBtn = (ImageView) findViewById(R.id.show_pass_btn_for_old);
        showNewPassBtn = (ImageView) findViewById(R.id.show_pass_btn_for_new_pass);
        showConfPassBtn = (ImageView) findViewById(R.id.show_pass_btn_for_confirm_pass);

        showOldPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag1)
                {
                    showOldPassBtn.setImageResource(R.drawable.ic_hide_pass);
                    oldPasswordTxt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    oldPasswordTxt.setSelection(oldPasswordTxt.getText().length());
                    flag1 = false;

                }else
                {
                    showOldPassBtn.setImageResource(R.drawable.ic_show_pass);
                    oldPasswordTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    oldPasswordTxt.setSelection(oldPasswordTxt.getText().length());
                    flag1 = true;
                }
            }
        });

        showNewPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag2)
                {
                    showNewPassBtn.setImageResource(R.drawable.ic_hide_pass);
                    newPassTxt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    newPassTxt.setSelection(newPassTxt.getText().length());
                    flag2 = false;

                }else
                {
                    showNewPassBtn.setImageResource(R.drawable.ic_show_pass);
                    newPassTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    newPassTxt.setSelection(newPassTxt.getText().length());
                    flag2 = true;
                }
            }
        });

        showConfPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag3)
                {
                    showConfPassBtn.setImageResource(R.drawable.ic_hide_pass);
                    confirmNewPassTxt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confirmNewPassTxt.setSelection(confirmNewPassTxt.getText().length());
                    flag3 = false;

                }else
                {
                    showConfPassBtn.setImageResource(R.drawable.ic_show_pass);
                    confirmNewPassTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    confirmNewPassTxt.setSelection(confirmNewPassTxt.getText().length());
                    flag3 = true;
                }
            }
        });



        changePassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (oldPasswordTxt.getText().toString().equalsIgnoreCase(""))
                {
                    oldPasswordTxt.setError("Please enter old password");
                    oldPasswordTxt.requestFocus();

                }else if (!oldPasswordStr.equalsIgnoreCase(oldPasswordTxt.getText().toString()))
                {
                    oldPasswordTxt.setError("Please enter valid old password");
                    oldPasswordTxt.requestFocus();

                }else if (newPassTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    newPassTxt.setError("Please enter password");
                    newPassTxt.requestFocus();
                }else if (newPassTxt.getText().toString().trim().length() < 5)
                {
                    newPassTxt.setError("Password should have minimum five character.");
                    newPassTxt.requestFocus();
                }else if (!newPassTxt.getText().toString().trim().equalsIgnoreCase(confirmNewPassTxt.getText().toString().trim()))
                {
                    confirmNewPassTxt.setError("Password and Confirm password is not matched");
                    confirmNewPassTxt.requestFocus();
                }else
                    {
                        if (conn.getConnectivityStatus()>0)
                        {
                            changePassword(userId,newPassTxt.getText().toString(),authKey);
                        }else
                            {
                                conn.showNoInternetAlret();
                            }
                    }
            }
        });

    }

    /*---API WORK--*/
    public void changePassword(final String UserId ,final String password, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(ChangePasswordActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, changePassUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("LoginUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        Toast.makeText(ChangePasswordActivity.this, Message, Toast.LENGTH_SHORT).show();
                        onBackPressed();

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) ChangePasswordActivity.this).isFinishing()) {
                            conn.showLogoutDialog(Message, ChangePasswordActivity.this);
                        }
                    }else
                    {
                        Toast.makeText(ChangePasswordActivity.this, Message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", UserId);
                params.put("password", password);
                params.put("LoginAuthKey",authKey);

                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Login");

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(ChangePasswordActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(ChangePasswordActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(ChangePasswordActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(ChangePasswordActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ChangePasswordActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ChangePasswordActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
    }

}
