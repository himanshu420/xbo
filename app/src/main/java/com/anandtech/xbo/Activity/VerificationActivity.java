package com.anandtech.xbo.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.anandtech.xbo.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class VerificationActivity extends AppCompatActivity {

    public EditText  veriFicationCodeTxt;
    public Button verificationBtn, backToLoginBtn;
    public String verifyUrl = SettingConstant.BASEURL + "api/UserWebApi/ConfirmationEmailIdverifyOtp";
    public ConnectionDetector conn;
    public String userId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        //Checking forced crash for crashlytics firebase report
        Fabric.with(VerificationActivity.this, new Crashlytics());

        conn = new ConnectionDetector(VerificationActivity.this);
        userId =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(VerificationActivity.this)));

        veriFicationCodeTxt = (EditText) findViewById(R.id.verifyCode);
        verificationBtn = (Button) findViewById(R.id.verification_btn);
        backToLoginBtn = (Button) findViewById(R.id.back_to_login);

        backToLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        verificationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (conn.getConnectivityStatus()>0)
                {
                    if (veriFicationCodeTxt.getText().toString().equalsIgnoreCase(""))
                    {
                        veriFicationCodeTxt.setError("Please enter verification code");
                        veriFicationCodeTxt.requestFocus();
                    }else {
                        verifyuser(veriFicationCodeTxt.getText().toString(),userId,currentDate());
                    }
                }else
                    {
                        conn.showNoInternetAlret();
                    }
            }
        });
    }

    /* API WORK*/
    public void verifyuser(final String verifyOtp, final String userId, final String date)
    {
        final ProgressDialog pDialog = new ProgressDialog(VerificationActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, verifyUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("LoginUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        JSONObject object = jsonObject.getJSONObject("Authdata");
                        String UserId = object.getString("UserId");
                        String ReferenceId = object.getString("ReferenceId");
                        String IsVerifid = object.getString("IsVerifid");
                        String EmailId = object.getString("EmailId");
                        String Name = object.getString("Name");
                        String Qrcode = object.getString("Qrcode");
                        String UserBalance = object.getString("UserBalance");
                        String LoginAuthkey = object.getString("LoginAuthkey");
                        String passcode = object.getString("passcode");
                        String UserTypeId = object.getString("UserTypeId");

                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setLoginStatus(VerificationActivity.this,
                                "1")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserId(VerificationActivity.this,
                                UserId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setRefrencId(VerificationActivity.this,
                                ReferenceId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setVerified(VerificationActivity.this,
                                IsVerifid)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setEmailId(VerificationActivity.this,
                                EmailId)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserName(VerificationActivity.this,
                                Name)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAddress(VerificationActivity.this,
                                Qrcode)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAmount(VerificationActivity.this,
                                UserBalance)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setAuthKey(VerificationActivity.this,
                                LoginAuthkey)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setPassCode(VerificationActivity.this,
                                passcode)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeID(VerificationActivity.this,
                                UserTypeId)));



                        Intent intent = new Intent(VerificationActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                        finish();

                    }else
                    {
                        Toast.makeText(VerificationActivity.this, Message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("verifyOtp", verifyOtp);
                params.put("LoginDateTime", date);
                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Login");

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(VerificationActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
        finish();

    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }

}
