package com.anandtech.xbo.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Adapter.MyStackListAdapter;
import com.anandtech.xbo.Adapter.TransactionHistoryAdapter;
import com.anandtech.xbo.Model.MyStackListModel;
import com.anandtech.xbo.Model.ProfileModel;
import com.anandtech.xbo.Model.TransactionHistoryModel;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.NetworkStateReceiver;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.anandtech.xbo.ViewModels.ProfileViewModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.ceylonlabs.imageviewpopup.ImagePopup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;
import uk.co.senab.photoview.PhotoView;

import android.os.PowerManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    public Button editBtn;
    public TextView nameTxt, emailIdTxt, phoneNpTxt, dobTxt,refCodeTxt, addressTxt,verifiedTxt,totalAmttxt,bankNameTxt,
            bankAccNoTxt,bankIfscCodeTxt,bankBranchNameTxt,totalVoucherTxt,roiAmtTxt;
    public LinearLayout cancelChqBtn, idProof1Btn, idProof2Btn,chqueLay, bankDetailsInnerLay,idProofLay,roiLay;
    public ImageView cancelCheckImg, idProof1Img, idproof2Img,backBtn,verifyImg;
    public String profileUrl = SettingConstant.BASEURL + "api/UserWebApi/UserDetailsByuserID";
    public String profilePicUrl = SettingConstant.BASEURL + "api/UserWebApi/UpdateUserProfilepic";
    public ConnectionDetector conn;
    public CircleImageView profilePicIMg;
    public String userId = "",authKey = "";
    public String Idproofimage1 = "",Idproofimage2 = "",cancelCheque = "",CityID = "",StateID = "",CountryID = "",
            CountryName = "",StateName = "",CityName = "",address = "",dob = "",BankName = "",AccountNo = "",
            IFSCcode = "",BankBranchName = "";
    public RelativeLayout editPrfBtn;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private Bitmap bitmap;
    public String profilePicBase64Str = "";
    public LinearLayout idProofLay1, idProofLay2,paymentLay,bankDetailsLay;
    public ProgressDialog progressDialog;
    public RecyclerView transactionHistRecy;
    public TransactionHistoryAdapter adapter;
    public ArrayList<TransactionHistoryModel> list = new ArrayList<>();
    public String myTransactionListUrl = SettingConstant.BASEURL + "api/FundingApi/fundListByUserID";
    private NetworkStateReceiver networkStateReceiver;
    private  AlertDialog internetDialog;
    private ProfileViewModel profileViewModel;
    private boolean flag = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Checking forced crash for crashlytics firebase report
        Fabric.with(ProfileActivity.this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_profile);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        internetDialog = new AlertDialog.Builder(ProfileActivity.this).create();
        profileViewModel = ViewModelProviders.of(this).get(ProfileViewModel.class);



        conn = new ConnectionDetector(ProfileActivity.this);
        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(ProfileActivity.this)));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(ProfileActivity.this)));
        progressDialog = new ProgressDialog(ProfileActivity.this);
        profileViewModel.init(userId,authKey,ProfileActivity.this);

        editBtn = (Button) findViewById(R.id.edit_btn);
        nameTxt = (TextView) findViewById(R.id.nameTxt);
        verifiedTxt = (TextView) findViewById(R.id.verified);
        emailIdTxt = (TextView) findViewById(R.id.emailIdTxt);
        phoneNpTxt = (TextView) findViewById(R.id.phone_number);
        dobTxt = (TextView) findViewById(R.id.dob_txt);
        refCodeTxt = (TextView) findViewById(R.id.refrence_code);
        addressTxt = (TextView) findViewById(R.id.address);
        totalAmttxt = (TextView) findViewById(R.id.total_amt);
        bankNameTxt = (TextView) findViewById(R.id.bank_name_txt);
        bankAccNoTxt = (TextView) findViewById(R.id.bank_account_txt);
        bankIfscCodeTxt = (TextView) findViewById(R.id.bank_ifsc_code_txt);
        bankBranchNameTxt = (TextView) findViewById(R.id.bank_branch_name_txt);
        totalVoucherTxt = (TextView) findViewById(R.id.total_voucher);
        roiAmtTxt = (TextView) findViewById(R.id.total_ROI);
        cancelChqBtn = (LinearLayout) findViewById(R.id.cancel_chq);
        idProof1Btn = (LinearLayout) findViewById(R.id.id_proof1);
        idProof2Btn = (LinearLayout) findViewById(R.id.id_proof2);
        roiLay = (LinearLayout) findViewById(R.id.roi_lay);
        cancelCheckImg = (ImageView) findViewById(R.id.cancel_check_id_image);
        verifyImg = (ImageView) findViewById(R.id.verifyImg);
        idProof1Img = (ImageView) findViewById(R.id.idproofimg);
        idproof2Img = (ImageView) findViewById(R.id.id_proof2img);
        profilePicIMg = (CircleImageView)findViewById(R.id.profile_pic_iamge);
        backBtn = (ImageView) findViewById(R.id.back_btn);
        editPrfBtn = (RelativeLayout) findViewById(R.id.relative_lay);
        idProofLay1 =  (LinearLayout) findViewById(R.id.idproof1lay);
        idProofLay2 = (LinearLayout) findViewById(R.id.idproof2lay);
        bankDetailsLay = (LinearLayout) findViewById(R.id.bank_detail_information_lay);
        chqueLay = (LinearLayout) findViewById(R.id.checqLay);
        bankDetailsInnerLay = (LinearLayout) findViewById(R.id.bank_details_lay);
        idProofLay = (LinearLayout) findViewById(R.id.id_proof_lay);




        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        cancelChqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopUp(SettingConstant.BASEURL +cancelCheque);

            }
        });

        idProof2Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopUp(SettingConstant.BASEURL +Idproofimage2);


            }
        });

        idProof1Img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopUp(SettingConstant.BASEURL +Idproofimage1);

            }
        });

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                intent.putExtra("Name",nameTxt.getText().toString());
                intent.putExtra("EMAILID",emailIdTxt.getText().toString());
                intent.putExtra("MobileNo",phoneNpTxt.getText().toString());
                intent.putExtra("DOB",dob);
                intent.putExtra("RefrenceCode",refCodeTxt.getText().toString());
                intent.putExtra("address",address);
                intent.putExtra("counteryName",CountryName);
                intent.putExtra("countryId",CountryID);
                intent.putExtra("stateName",StateName);
                intent.putExtra("stateId",StateID);
                intent.putExtra("cityName",CityName);
                intent.putExtra("cityId",CityID);
                intent.putExtra("idproof1",Idproofimage1);
                intent.putExtra("idproof2",Idproofimage2);
                intent.putExtra("CancelChque",cancelCheque);
                intent.putExtra("BankName",BankName);
                intent.putExtra("AccountNumber",AccountNo);
                intent.putExtra("IfscCode",IFSCcode);
                intent.putExtra("BankBranch",BankBranchName);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        editPrfBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
            }
        });

        profileViewModel.getProfileLiveData().observe(this, new Observer<ProfileModel>() {
            @Override
            public void onChanged(ProfileModel profileModel) {

                Log.e("Checking the Prrof","Observe....");

                if (profileModel.getStatus().equalsIgnoreCase("1"))
                {
                    address = profileModel.getResultModels().getAddress();
                    CountryName = profileModel.getResultModels().getCountryName();
                    StateName = profileModel.getResultModels().getStateName();
                    CityName = profileModel.getResultModels().getCityName();
                    Idproofimage1 = profileModel.getResultModels().getIdproof1();
                    Idproofimage2 = profileModel.getResultModels().getIdproof2();
                    cancelCheque = profileModel.getResultModels().getCancelCheque();
                    BankName = profileModel.getResultModels().getBankName();
                    AccountNo = profileModel.getResultModels().getAccountNo();
                    IFSCcode = profileModel.getResultModels().getIFSCcode();
                    BankBranchName = profileModel.getResultModels().getBankBranchName();
                    dob = profileModel.getResultModels().getDob();

                    totalAmttxt.setText(profileModel.getResultModels().getTotalXboToken());
                    totalVoucherTxt.setText(profileModel.getResultModels().getAmount());
                    nameTxt.setText(profileModel.getResultModels().getName());
                    emailIdTxt.setText(profileModel.getResultModels().getEmailId());
                    addressTxt.setText(address + ", " +CountryName + ", " +StateName +", "+CityName);
                    dobTxt.setText(convertDate(dob));
                    roiAmtTxt.setText(profileModel.getResultModels().getROI());
                    phoneNpTxt.setText(profileModel.getResultModels().getPhoneNumber());
                    refCodeTxt.setText(profileModel.getResultModels().getUniqecode());

                    //checking ROI AMount
                    if (profileModel.getResultModels().getROI().equalsIgnoreCase(""))
                        roiLay.setVisibility(View.GONE);
                    else
                        roiLay.setVisibility(View.VISIBLE);

                    //checking Verification User or Not
                    if (profileModel.getResultModels().getVerificationStatus().equalsIgnoreCase("true"))
                    {
                        verifyImg.setImageResource(R.drawable.ic_verified_protection);
                        verifiedTxt.setText("Verified");
                        verifiedTxt.setTextColor(getResources().getColor(R.color.green_color));
                    }else
                        {
                            verifyImg.setImageResource(R.drawable.ic_unverified);
                            verifiedTxt.setText("Unverified");
                            verifiedTxt.setTextColor(getResources().getColor(R.color.red_color));
                        }

                    //set profile pic
                    Glide.with(ProfileActivity.this)
                            .load(SettingConstant.BASEURL + profileModel.getResultModels().getProfilePicture())
                            .apply(new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .placeholder(R.drawable.ic_loading)
                                    .error(R.drawable.prf))
                            .into(profilePicIMg);

                    //Check idproof image null or not
                    if (Idproofimage1.equalsIgnoreCase("NA") &&
                            Idproofimage2.equalsIgnoreCase("NA"))
                    {
                        idProofLay.setVisibility(View.GONE);
                    }else if (Idproofimage1.equalsIgnoreCase("NA"))
                    {
                        idProofLay.setVisibility(View.VISIBLE);
                        idProofLay1.setVisibility(View.GONE);
                        idProofLay2.setVisibility(View.VISIBLE);

                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + Idproofimage2)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .placeholder(R.drawable.ic_loading)
                                        .skipMemoryCache(true))
                                .into(idproof2Img);

                    }else if (Idproofimage2.equalsIgnoreCase("NA"))
                    {
                        idProofLay.setVisibility(View.VISIBLE);
                        idProofLay2.setVisibility(View.GONE);
                        idProofLay1.setVisibility(View.VISIBLE);

                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + Idproofimage1)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .placeholder(R.drawable.ic_loading)
                                        .skipMemoryCache(true))
                                .into(idProof1Img);
                    }else
                    {
                        idProofLay.setVisibility(View.VISIBLE);
                        idProofLay2.setVisibility(View.VISIBLE);
                        idProofLay1.setVisibility(View.VISIBLE);

                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + Idproofimage1)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .placeholder(R.drawable.ic_loading)
                                        .skipMemoryCache(true))
                                .into(idProof1Img);

                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + Idproofimage2)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .placeholder(R.drawable.ic_loading)
                                        .skipMemoryCache(true))
                                .into(idproof2Img);
                    }



                    if (cancelCheque.equalsIgnoreCase("NA") && BankName.equalsIgnoreCase("NA"))
                    {
                        bankDetailsLay.setVisibility(View.GONE);
                    }else if (!cancelCheque.equalsIgnoreCase("NA"))
                    {
                        bankDetailsLay.setVisibility(View.VISIBLE);
                        chqueLay.setVisibility(View.VISIBLE);
                        bankDetailsInnerLay.setVisibility(View.GONE);


                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + cancelCheque)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .placeholder(R.drawable.ic_loading)
                                        .skipMemoryCache(true))
                                .into(cancelCheckImg);
                    }else
                    {
                        bankDetailsLay.setVisibility(View.VISIBLE);
                        chqueLay.setVisibility(View.GONE);
                        bankDetailsInnerLay.setVisibility(View.VISIBLE);

                        bankNameTxt.setText(BankName);
                        bankAccNoTxt.setText(AccountNo);
                        bankIfscCodeTxt.setText(IFSCcode);
                        bankBranchNameTxt.setText(BankBranchName);


                    }



                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

       // getProfileDetails(userId,authKey);

        if (!flag) {
            Log.e("Checking resume", "Resume");
            profileViewModel.init(userId, authKey, ProfileActivity.this);
        }
        flag = false;
    }

   /* *//*API WORK*//*
    public void getProfileDetails(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(ProfileActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, profileUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("My Profile", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (Status.equalsIgnoreCase("1")) {
                        JSONObject object = jsonObject.getJSONObject("result");

                        String name = object.getString("Name");
                        String emailid = object.getString("EmailId");
                        address = object.getString("address1");
                        dob = object.getString("Dob");
                        String phonenumber = object.getString("PhoneNumber");
                        Idproofimage1 = object.getString("Idproof1");
                        Idproofimage2 = object.getString("Idproof2");
                        String profilepic = object.getString("ProfilePicture");
                        String uniqecode = object.getString("uniqecode");
                        cancelCheque = object.getString("CancelCheque");
                        String VerificationStatus = object.getString("VerificationStatus");
                        CityID = object.getString("CityID");
                        StateID = object.getString("StateID");
                        CountryID = object.getString("CountryID");
                        CountryName = object.getString("CountryName");
                        StateName = object.getString("StateName");
                        CityName = object.getString("CityName");
                        String amount = object.getString("amount");
                        String TotalXboToken = object.getString("TotalXboToken");
                        BankName = object.getString("BankName");
                        AccountNo = object.getString("AccountNo");
                        IFSCcode = object.getString("IFSCcode");
                        BankBranchName = object.getString("BankBranchName");

                        totalAmttxt.setText(TotalXboToken);
                        totalVoucherTxt.setText(amount);

                        if (VerificationStatus.equalsIgnoreCase("true"))
                        {
                            verifyImg.setImageResource(R.drawable.ic_verified_protection);
                            verifiedTxt.setText("Verified");
                            verifiedTxt.setTextColor(getResources().getColor(R.color.green_color));
                        }else
                            {
                                verifyImg.setImageResource(R.drawable.ic_unverified);
                                verifiedTxt.setText("Unverified");
                                verifiedTxt.setTextColor(getResources().getColor(R.color.red_color));
                            }

                       *//* //convert date string to Sep format
                        SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yyyy a");
                        String date = format.format(Date.parse(dob));*//*

                        //nameTxt.setText(name);
                        emailIdTxt.setText(emailid);
                        addressTxt.setText(address + ", " +CountryName + ", " +StateName +", "+CityName);
                        dobTxt.setText(convertDate(dob));
                        phoneNpTxt.setText(phonenumber);
                        refCodeTxt.setText(uniqecode);

                        //set profile pic
                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + profilepic)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .placeholder(R.drawable.ic_loading)
                                        .error(R.drawable.prf))
                                .into(profilePicIMg);



                        //Check idproof image null or not
                        if (Idproofimage1.equalsIgnoreCase("NA") &&
                                Idproofimage2.equalsIgnoreCase("NA"))
                        {
                            idProofLay.setVisibility(View.GONE);
                        }else if (Idproofimage1.equalsIgnoreCase("NA"))
                        {
                            idProofLay.setVisibility(View.VISIBLE);
                            idProofLay1.setVisibility(View.GONE);
                            idProofLay2.setVisibility(View.VISIBLE);

                            Glide.with(ProfileActivity.this)
                                    .load(SettingConstant.BASEURL + Idproofimage2)
                                    .apply(new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.ic_loading)
                                            .skipMemoryCache(true))
                                    .into(idproof2Img);

                        }else if (Idproofimage2.equalsIgnoreCase("NA"))
                        {
                            idProofLay.setVisibility(View.VISIBLE);
                            idProofLay2.setVisibility(View.GONE);
                            idProofLay1.setVisibility(View.VISIBLE);

                            Glide.with(ProfileActivity.this)
                                    .load(SettingConstant.BASEURL + Idproofimage1)
                                    .apply(new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.ic_loading)
                                            .skipMemoryCache(true))
                                    .into(idProof1Img);
                        }else
                            {
                                idProofLay.setVisibility(View.VISIBLE);
                                idProofLay2.setVisibility(View.VISIBLE);
                                idProofLay1.setVisibility(View.VISIBLE);

                                Glide.with(ProfileActivity.this)
                                        .load(SettingConstant.BASEURL + Idproofimage1)
                                        .apply(new RequestOptions()
                                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                .placeholder(R.drawable.ic_loading)
                                                .skipMemoryCache(true))
                                        .into(idProof1Img);

                                Glide.with(ProfileActivity.this)
                                        .load(SettingConstant.BASEURL + Idproofimage2)
                                        .apply(new RequestOptions()
                                                .diskCacheStrategy(DiskCacheStrategy.NONE)
                                                .placeholder(R.drawable.ic_loading)
                                                .skipMemoryCache(true))
                                        .into(idproof2Img);
                            }

                        //check bank details null or not
                        if (cancelCheque.equalsIgnoreCase("NA") && BankName.equalsIgnoreCase("NA"))
                        {
                            bankDetailsLay.setVisibility(View.GONE);
                        }else if (!cancelCheque.equalsIgnoreCase("NA"))
                        {
                            bankDetailsLay.setVisibility(View.VISIBLE);
                            chqueLay.setVisibility(View.VISIBLE);
                            bankDetailsInnerLay.setVisibility(View.GONE);


                            Glide.with(ProfileActivity.this)
                                    .load(SettingConstant.BASEURL + cancelCheque)
                                    .apply(new RequestOptions()
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.ic_loading)
                                            .skipMemoryCache(true))
                                    .into(cancelCheckImg);
                        }else
                            {
                                bankDetailsLay.setVisibility(View.VISIBLE);
                                chqueLay.setVisibility(View.GONE);
                                bankDetailsInnerLay.setVisibility(View.VISIBLE);

                                bankNameTxt.setText(BankName);
                                bankAccNoTxt.setText(AccountNo);
                                bankIfscCodeTxt.setText(IFSCcode);
                                bankBranchNameTxt.setText(BankBranchName);


                            }
                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) ProfileActivity.this).isFinishing()) {
                            conn.showLogoutDialog(message, ProfileActivity.this);
                        }
                    }else
                        {
                            Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                        }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("LoginAuthKey",authKey);
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Profile");
    }*/

    /*API WORK FOR TRANSACTION HISTORY*//*
    public void getTransactionList(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(ProfileActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, myTransactionListUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("My Profile", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (list.size()>0)
                    {
                        list.clear();
                    }

                    if (Status.equalsIgnoreCase("1")) {
                        paymentLay.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i = 0; i<jsonArray.length(); i++) {

                            JSONObject object = jsonArray.getJSONObject(i);
                            String amount = object.getString("amount");
                            String paymentType = object.getString("fundingType");
                            String referenceCode = object.getString("referenceCode");
                            String fundingDatetime = object.getString("fundingDatetime");


                            list.add(new TransactionHistoryModel(fundingDatetime,amount,referenceCode,paymentType));

                        }

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) ProfileActivity.this).isFinishing()) {
                            conn.showLogoutDialog(message, ProfileActivity.this);
                        }
                    }else
                    {
                        paymentLay.setVisibility(View.GONE);
                        // noDataTxt.setVisibility(View.VISIBLE);
                        //Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                    adapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("usrId", userId);
                params.put("LoginAuthKey",authKey);
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Profile");
    }*/


    public void setProfilePic(final String userId, final String profilepic, final String authKey)
    {
        Log.e("Base 64 Image", profilepic);

        final ProgressDialog pDialog = new ProgressDialog(ProfileActivity.this);
        pDialog.setMessage("Please Wait updated your profile...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, profilePicUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Profile", profilepic);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (Status.equalsIgnoreCase("1")) {

                        String ProfilePic = jsonObject.getString("ProfilePic");

                        //set profile pic
                        Glide.with(ProfileActivity.this)
                                .load(SettingConstant.BASEURL + ProfilePic)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                        .skipMemoryCache(true)
                                        .placeholder(R.drawable.ic_loading)
                                        .error(R.drawable.prf))
                                .into(profilePicIMg);

                        Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) ProfileActivity.this).isFinishing()) {
                            conn.showLogoutDialog(message, ProfileActivity.this);
                        }
                    }else
                    {
                        Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("ProfilePicture", profilepic);
                params.put("LoginAuthKey",authKey);

                Log.e("Checking Params", params.toString());
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Profile");
    }

    //convert date time to month
    public String convertDate(String dtStart)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

            String date1 = sdf.format(date);

            return date1;


        } catch (ParseException e) {
            e.printStackTrace();
        }

       return "";
    }

    public void showPopUp(String imageUrl)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = ProfileActivity.this.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_custom_layout, null);

        ImageView imageView = (PhotoView) view.findViewById(R.id.image);
        ImageView croosBtn = (ImageView) view.findViewById(R.id.croosbtn);
        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final AlertDialog ad = builder.show();


        croosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

            }
        });
        Glide.with(ProfileActivity.this)
                .load(imageUrl)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.ic_loading)
                        .skipMemoryCache(true))
                .into(imageView);

    }
    private void selectImage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            }else {

                try {
                    PackageManager pm = getPackageManager();
                    int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                    if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ProfileActivity.this);
                        builder.setTitle("Select Option");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (options[item].equals("Take Photo")) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);
                                } else if (options[item].equals("Choose From Gallery")) {
                                    dialog.dismiss();
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                                } else if (options[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    } else
                        Toast.makeText(ProfileActivity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }else
            {
                try {
                    PackageManager pm = getPackageManager();
                    int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                    if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ProfileActivity.this);
                        builder.setTitle("Select Option");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (options[item].equals("Take Photo")) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);
                                } else if (options[item].equals("Choose From Gallery")) {
                                    dialog.dismiss();
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                                } else if (options[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    } else
                        Toast.makeText(ProfileActivity.this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ProfileActivity.this);
                    builder.setTitle("Select Option");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {
                                dialog.dismiss();
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);
                            } else if (options[item].equals("Choose From Gallery")) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                            } else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();

                } else {

                    Toast.makeText(ProfileActivity.this, "camera permission denied", Toast.LENGTH_LONG).show();

                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");

                convert(userId,bitmap);
                profilePicIMg.setImageBitmap(bitmap);


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            try {
                Uri selectedImage = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);

                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, 500, true);

              //  profilePicBase64Str = convert(scaled);

                convert(userId,scaled);
                profilePicIMg.setImageBitmap(scaled);

              //  setProfilePic(userId,profilePicBase64Str);



            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void convert(String userId ,Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        profilePicBase64Str = Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

        setProfilePic(userId,profilePicBase64Str,authKey);

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(ProfileActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(ProfileActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(ProfileActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(ProfileActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ProfileActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ProfileActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        profileViewModel.getProfileLiveData().removeObservers(this);
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void networkAvailable() {

        internetDialog.dismiss();
    }

    @Override
    public void networkUnavailable() {

        showInternetPopUp();
    }

    private void showInternetPopUp()
    {
        // Setting Dialog Title
        internetDialog.setTitle("Connection Failed");
        internetDialog.setMessage("Something is wrong with your internet connection. Please check your internet connection");
        internetDialog.setCancelable(false);

       /* // Setting OK Button
        internetDialog.setButton(Dialog.BUTTON_POSITIVE,"Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //internetDialog.dismiss();
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });*/

        // Showing Alert Message
        internetDialog.show();

    }

}
