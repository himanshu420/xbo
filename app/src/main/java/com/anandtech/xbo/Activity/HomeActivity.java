package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import com.anandtech.xbo.BuildConfig;
import com.anandtech.xbo.Fragment.DashboardFragment;
import com.anandtech.xbo.Fragment.SettingFragment;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AndroidBug5497Workaround;
import com.anandtech.xbo.Source.NetworkStateReceiver;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.DisplayCutout;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.fabric.sdk.android.Fabric;

import static com.google.android.gms.common.util.Strings.capitalize;

public class HomeActivity extends AppCompatActivity implements DashboardFragment.OnFragmentInteractionListener,
        NetworkStateReceiver.NetworkStateReceiverListener {

    public static int navigationItemIndex = 0;
    private boolean shouldLoadHomeFragOnBackPress = true;
    public TextView toolbatTitleTxt;
    public ImageView backBtn,shareBtn,settingBtn;
    public String referenceIdString = "",userName = "";
    public LinearLayout mainAppLay;
    private NetworkStateReceiver networkStateReceiver;
    private  AlertDialog internetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Checking forced crash for crashlytics firebase report
        Fabric.with(HomeActivity.this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        internetDialog = new AlertDialog.Builder(HomeActivity.this).create();



        Toolbar toolbar = findViewById(R.id.my_toolbar);
        toolbatTitleTxt = (TextView) toolbar.findViewById(R.id.toolbar_txt);
        backBtn = (ImageView) toolbar.findViewById(R.id.back_btn);
        shareBtn = (ImageView) toolbar.findViewById(R.id.share_btn);
        settingBtn = (ImageView) toolbar.findViewById(R.id.setting_btn);
        setSupportActionBar(toolbar);

        referenceIdString  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getRefrenceId(HomeActivity.this)));
        userName =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserName(HomeActivity.this)));

        mainAppLay = (LinearLayout) findViewById(R.id.main_app_lay);
       // snacbarLay = (LinearLayout) findViewById(R.id.snackbar_lay);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        shareBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Share Option Method
                openShareOption(referenceIdString,userName);
            }
        });

        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                navigationItemIndex = 3;
                backBtn.setVisibility(View.VISIBLE);
                shareBtn.setVisibility(View.GONE);
                loadFragment(new SettingFragment());
                mainAppLay.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        });

        loadFragment(new DashboardFragment());



    }

    private void openShareOption(String ReferId,String userName) {

        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "My application name");
            String shareMessage= "I just signed up on upcoming futuristic Crypto Currency Exchange XBOND’s initial sale of Exchange Tradable and Hybrid Tether Token and got XBO tokens worth INR 100. XBO ETT and Hybrid Tether Token will provide 70% fees discount on buying and selling of cryptos on the XBOND’s exchange platform and it has a Hybrid Tether value 10 cents per token. They are giving away XBO worth INR 100 to anyone who signs up early and extra XBO worth INR 25 who signs up using this referral link.\n" +
                    "    You can purchase more XBO through the app as well. So hurry up and don’t miss out on the Initial Token sale program which carries a ROI as well.\n\n";
            shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"&referrer="+ReferId;
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            startActivity(Intent.createChooser(shareIntent, "choose one"));
        } catch(Exception e) {
            //e.toString();
        }
    }

    private Fragment loadFragment(Fragment fragment) {

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_layout, fragment)
                .addToBackStack(null)
                .setCustomAnimations( R.anim.push_right_in,
                        R.anim.push_left_out, R.anim.push_left_in, R.anim.push_right_out)
                .commit();
        return fragment;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void onBackPressed() {

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than home
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than home
            if (navigationItemIndex != 0) {

                navigationItemIndex = 0;
                loadFragment(new DashboardFragment());

                toolbatTitleTxt.setText("XBO ETT Wallet");
                backBtn.setVisibility(View.GONE);
                shareBtn.setVisibility(View.VISIBLE);

                mainAppLay.setBackgroundColor(Color.parseColor("#a7c283"));

                return;
            }
        }
        finish();

    }

    @Override
    public void onFragmentInteraction(int index, String tollbarTitle) {

        navigationItemIndex = index;
        toolbatTitleTxt.setText(tollbarTitle);

        if (index == 6)
        {
            backBtn.setVisibility(View.VISIBLE);
            shareBtn.setVisibility(View.GONE);

            mainAppLay.setBackgroundColor(getResources().getColor(R.color.first_gradient));

        }else if (index != 0)
        {

            backBtn.setVisibility(View.VISIBLE);
            shareBtn.setVisibility(View.GONE);

            mainAppLay.setBackgroundColor(Color.parseColor("#ffffff"));

        }else
            {
                backBtn.setVisibility(View.GONE);
                shareBtn.setVisibility(View.VISIBLE);

                mainAppLay.setBackgroundColor(Color.parseColor("#a7c283"));
            }
    }

    //hide the bottom home and back button and status bar
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        immersiveMode();
        getWindow().getDecorView().setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        delayedHide(1500);
                    }
                });


    }

    public void immersiveMode() {
        final View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY

        );
    }

    private final Handler mHideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            immersiveMode();
        }
    };

    private void delayedHide(int delayMillis) {
        mHideHandler.removeMessages(0);
        mHideHandler.sendEmptyMessageDelayed(0, delayMillis);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();
        AndroidBug5497Workaround.assistActivity(HomeActivity.this);

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(HomeActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(HomeActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(HomeActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(HomeActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(HomeActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(HomeActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    @Override
    protected void onResume() {
        super.onResume();

        //hide keyboard
        hideKeyboard();
    }

    public void hideKeyboard() {

        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        if (imm.isAcceptingText()) {
            getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
            );
        }
    }

    @Override
    public void networkAvailable() {

        Log.e("Checking Internet", "Network is avilable");
        internetDialog.dismiss();


     /*   snacbarLay.setVisibility(View.VISIBLE);

        //handler Working
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                snacbarLay.setVisibility(View.GONE);

            }
        }, 2000);*/




       /* Snackbar mSnackBar = Snackbar.make(mainAppLay,"Network is avilable",Snackbar.LENGTH_LONG);
        TextView mainTextView = (TextView) (mSnackBar.getView()).findViewById(R.id.snackbar_text);
        mSnackBar.getView().setBackgroundColor(Color.parseColor("#76FF03"));
        mainTextView.setTextColor(Color.WHITE);
        mainTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._14sdp));
        mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        mSnackBar.show();*/

    }

    @Override
    public void networkUnavailable() {

        Log.e("Checking Internet", "Network is not avilable");

       // Snackbar.make(mainAppLay,"Network is not avilable",Snackbar.LENGTH_LONG).show();

        showInternetPopUp();

      /*  Snackbar mSnackBar = Snackbar.make(mainAppLay,"Network is not avilable",Snackbar.LENGTH_LONG);
        TextView mainTextView = (TextView) (mSnackBar.getView()).findViewById(R.id.snackbar_text);
        mSnackBar.getView().setBackgroundColor(Color.parseColor("#B71C1C"));
        mainTextView.setTextColor(Color.WHITE);
        mainTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen._14sdp));
        mainTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        mSnackBar.show();*/

    }

    void showInternetPopUp()
    {
        // Setting Dialog Title
        internetDialog.setTitle("Connection Failed");
        internetDialog.setMessage("Something is wrong with your internet connection. Please check your internet connection");
        internetDialog.setCancelable(false);


     /*   // Setting OK Button
        internetDialog.setButton(Dialog.BUTTON_POSITIVE,"Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //internetDialog.dismiss();
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });*/
        // Showing Alert Message
        internetDialog.show();


    }

}
