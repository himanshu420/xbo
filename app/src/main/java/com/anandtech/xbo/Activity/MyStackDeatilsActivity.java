package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Adapter.MyStackListAdapter;
import com.anandtech.xbo.Adapter.TransactionHistoryAdapter;
import com.anandtech.xbo.Model.CreateSubAgentModel;
import com.anandtech.xbo.Model.MyStackListModel;
import com.anandtech.xbo.Model.TransactionHistoryModel;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.NetworkStateReceiver;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import uk.co.senab.photoview.PhotoView;

import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyStackDeatilsActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {
    public TextView toolbatTitleTxt;
    public ImageView backBtn;
    public TextView emailIdTxt,phoneTxt,dojTxt,totalVouTxt,totalXboTxt,stateNameTxt,cityNameTxt,countryNameTxt,
            walletAddTxt,interducerNameTxt;
    public ImageView profilePicImg;
    public LinearLayout cityNameLay, accountInfoLay;
    public Button customerSupportBtn;
    public LinearLayout paymentLay;
    public String nameStr ="", profilePicStr="", emailIdStr="", phoneNoStr ="", conteryNameStr="", stateNameStr=""
            , amountStr="", citynameStr="", totalXboStr="", walletAddressStr="", joiningDateStr="",joinId = "",
            introDucerNameStr = "",refrenceId = "",agentRefrenceId;
    public RecyclerView transactionHistRecy;
    public TransactionHistoryAdapter adapter;
    public ArrayList<TransactionHistoryModel> list = new ArrayList<>();
    public String myTransactionListUrl = SettingConstant.BASEURL + "api/FundingApi/fundListdata";
    public ConnectionDetector conn;
    public String userId = "",userTypeId = "";
    public RelativeLayout profileBtn;
    public Button subAdminBtn;
    public RetrofitInterface retrofitClientInstance;
    private NetworkStateReceiver networkStateReceiver;
    private  AlertDialog internetDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Checking forced crash for crashlytics firebase report
        Fabric.with(MyStackDeatilsActivity.this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_stack_deatils);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        internetDialog = new AlertDialog.Builder(MyStackDeatilsActivity.this).create();

        Toolbar toolbar = findViewById(R.id.my_toolbar);
        toolbatTitleTxt = (TextView) toolbar.findViewById(R.id.toolbar_txt);
        backBtn = (ImageView) toolbar.findViewById(R.id.back_btn);
        setSupportActionBar(toolbar);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();

            }
        });

        Intent intent = getIntent();
        if (intent != null)
        {
            nameStr = intent.getStringExtra("Name");
            profilePicStr = intent.getStringExtra("ProfilePic");
            emailIdStr = intent.getStringExtra("Email");
            phoneNoStr = intent.getStringExtra("PhoneNo");
            conteryNameStr = intent.getStringExtra("CountryName");
            stateNameStr = intent.getStringExtra("StateName");
            amountStr = intent.getStringExtra("Amount");
            citynameStr = intent.getStringExtra("CityName");
            totalXboStr = intent.getStringExtra("TotalXboToken");
            walletAddressStr = intent.getStringExtra("WalletAddress");
            joiningDateStr = intent.getStringExtra("CreatedDateTime");
            joinId = intent.getStringExtra("JoinId");
            introDucerNameStr = intent.getStringExtra("IntroducerName");
            refrenceId = intent.getStringExtra("RefrenceId");
            agentRefrenceId = intent.getStringExtra("AgentRefrenceId");
        }

        conn = new ConnectionDetector(MyStackDeatilsActivity.this);
        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(MyStackDeatilsActivity.this)));
        userTypeId =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserTypeID(MyStackDeatilsActivity.this)));
        retrofitClientInstance = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);

        emailIdTxt = (TextView) findViewById(R.id.emailIdTxt);
        phoneTxt = (TextView) findViewById(R.id.phone_number);
        dojTxt = (TextView) findViewById(R.id.date_of_joing);
        totalVouTxt = (TextView) findViewById(R.id.total_voucher);
        totalXboTxt = (TextView) findViewById(R.id.total_xbo);
        countryNameTxt = (TextView) findViewById(R.id.country_name);
        stateNameTxt = (TextView) findViewById(R.id.state_name);
        cityNameTxt = (TextView) findViewById(R.id.city_name);
        walletAddTxt = (TextView) findViewById(R.id.wallet_address);
        interducerNameTxt = (TextView) findViewById(R.id.introducer_name);
        profilePicImg = (ImageView) findViewById(R.id.profilePic);
        accountInfoLay = (LinearLayout) findViewById(R.id.account_info_lay);
        customerSupportBtn = (Button) findViewById(R.id.sub_btn);
        paymentLay = (LinearLayout) findViewById(R.id.payment_lay);
        cityNameLay = (LinearLayout) findViewById(R.id.city_name_lay);
        transactionHistRecy = (RecyclerView) findViewById(R.id.transaction_hist);
        profileBtn = (RelativeLayout) findViewById(R.id.show_profile_image);
        subAdminBtn = (Button) findViewById(R.id.sub_admin_btn);

        if (userTypeId.equalsIgnoreCase("1"))
        {
            subAdminBtn.setVisibility(View.GONE);
        }else if (userTypeId.equalsIgnoreCase("3"))
        {
            subAdminBtn.setVisibility(View.GONE);
        }else
            {
                if (refrenceId.equalsIgnoreCase(userId) && agentRefrenceId.equalsIgnoreCase(""))
                {
                    subAdminBtn.setVisibility(View.VISIBLE);
                }else
                    {
                        subAdminBtn.setVisibility(View.GONE);
                    }
            }

        if (amountStr.equalsIgnoreCase("NA"))
        {
            accountInfoLay.setVisibility(View.GONE);
            paymentLay.setVisibility(View.GONE);
        }else {

            accountInfoLay.setVisibility(View.VISIBLE);

        }

        if (citynameStr.equalsIgnoreCase("NA"))
        {
            cityNameLay.setVisibility(View.GONE);
        }else
        {
            cityNameLay.setVisibility(View.VISIBLE);
            cityNameTxt.setText(citynameStr);
        }


        toolbatTitleTxt.setText(nameStr);
        emailIdTxt.setText(emailIdStr);
        phoneTxt.setText(phoneNoStr);
        dojTxt.setText(joiningDateStr);
        walletAddTxt.setText(walletAddressStr);
        totalXboTxt.setText(totalXboStr);
        totalVouTxt.setText(amountStr);
        countryNameTxt.setText(conteryNameStr);
        stateNameTxt.setText(stateNameStr);
        interducerNameTxt.setText(introDucerNameStr);

        //set Profile Pic
        if (!profilePicStr.equalsIgnoreCase("NA")) {

            Glide.with(MyStackDeatilsActivity.this)
                    .load(SettingConstant.BASEURL + profilePicStr)
                    .apply(new RequestOptions()
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .placeholder(R.drawable.ic_loading)
                            .skipMemoryCache(true))
                    .into(profilePicImg);
        }

        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopUp(SettingConstant.BASEURL +profilePicStr);
            }
        });

        customerSupportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MyStackDeatilsActivity.this, SupportActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });


        adapter = new TransactionHistoryAdapter(list,MyStackDeatilsActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MyStackDeatilsActivity.this);
        transactionHistRecy.setLayoutManager(mLayoutManager);
        transactionHistRecy.setItemAnimator(new DefaultItemAnimator());
        transactionHistRecy.setAdapter(adapter);

        subAdminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createSubAgent(userId,joinId);
            }
        });

    }

    public void showPopUp(String imageUrl)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MyStackDeatilsActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = MyStackDeatilsActivity.this.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.dialog_custom_layout, null);

        ImageView imageView = (PhotoView) view.findViewById(R.id.image);
        ImageView croosBtn = (ImageView) view.findViewById(R.id.croosbtn);
        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final AlertDialog ad = builder.show();


        croosBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

            }
        });

        Glide.with(MyStackDeatilsActivity.this)
                .load(imageUrl)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .placeholder(R.drawable.ic_loading)
                        .skipMemoryCache(true))
                .into(imageView);

    }

    /*API WORK*/
    public void getTransactionList(final String userId,final String joinId)
    {
        final ProgressDialog pDialog = new ProgressDialog(MyStackDeatilsActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, myTransactionListUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("My Funding List", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (list.size()>0)
                    {
                        list.clear();
                    }

                    if (Status.equalsIgnoreCase("1")) {
                        paymentLay.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i = 0; i<jsonArray.length(); i++) {

                            JSONObject object = jsonArray.getJSONObject(i);
                            String amount = object.getString("amount");
                            String paymentType = object.getString("fundingType");
                            String referenceCode = object.getString("referenceCode");
                            String fundingDatetime = object.getString("fundingDatetime");


                            list.add(new TransactionHistoryModel(fundingDatetime,amount,referenceCode,paymentType));

                        }

                    }else
                    {
                        paymentLay.setVisibility(View.GONE);
                       // noDataTxt.setVisibility(View.VISIBLE);
                       // Toast.makeText(MyStackDeatilsActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                    adapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("usrId", userId);
                params.put("JoinId", joinId);
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Profile");
    }

    public void createSubAgent(String userId, String joinId)
    {
        final ProgressDialog pDialog = new ProgressDialog(MyStackDeatilsActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<CreateSubAgentModel> call = retrofitClientInstance.createSubAgent(userId,joinId);
        call.enqueue(new Callback<CreateSubAgentModel>() {
            @Override
            public void onResponse(Call<CreateSubAgentModel> call, retrofit2.Response<CreateSubAgentModel> response) {

                if (response.body().getStatus().equalsIgnoreCase("1"))
                {
                    Toast.makeText(MyStackDeatilsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }else
                    {
                        Toast.makeText(MyStackDeatilsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                pDialog.dismiss();

            }

            @Override
            public void onFailure(Call<CreateSubAgentModel> call, Throwable t) {

                pDialog.dismiss();
            }
        });


    }

    //convert date time to month
    public String convertDate(String dtStart)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

            String date1 = sdf.format(date);

            return date1;


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(MyStackDeatilsActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(MyStackDeatilsActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(MyStackDeatilsActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(MyStackDeatilsActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(MyStackDeatilsActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(MyStackDeatilsActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
    }

    @Override
    public void networkAvailable() {

        //Call the transaction List API
        getTransactionList(userId,joinId);
        internetDialog.dismiss();
    }

    @Override
    public void networkUnavailable() {

        showInternetPopUp();
    }

   private void showInternetPopUp()
    {
        // Setting Dialog Title
        internetDialog.setTitle("Connection Failed");
        internetDialog.setMessage("Something is wrong with your internet connection. Please check your internet connection");
        internetDialog.setCancelable(false);
        // Showing Alert Message
        internetDialog.show();

    }
}
