package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Model.PasscodeModel;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.andrognito.pinlockview.IndicatorDots;
import com.andrognito.pinlockview.PinLockListener;
import com.andrognito.pinlockview.PinLockView;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;

import android.os.PowerManager;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.R;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.supercharge.shimmerlayout.ShimmerLayout;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasscodeActivity extends AppCompatActivity {
    public TextView mainHeadingTxt,skipBtn,skipForFingerBtn;
    private PinLockView mPinLockView;
    private IndicatorDots mIndicatorDots;
    private ShimmerLayout shimmerFrameLayout;
    public boolean count  = true;
    public String pincode = "",userId = "",navStr = "",fingerEnableStr = "";
    public Vibrator vib;
    public LinearLayout fingerPrintLay,passCodeLay;
    public ConnectionDetector conn;
    public RetrofitInterface retrofitClientInstance;
    public SwitchCompat switchCompat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.content_reset_passcode);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        /*//android O fix bug orientation
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }*/

        //Checking forced crash for crashlytics firebase report
        Fabric.with(ResetPasscodeActivity.this, new Crashlytics());

        retrofitClientInstance = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);
        userId = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(ResetPasscodeActivity.this)));
        fingerEnableStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(ResetPasscodeActivity.this)));
        conn = new ConnectionDetector(ResetPasscodeActivity.this);

        Intent intent = getIntent();
        if (intent != null)
        {
            navStr = intent.getStringExtra("Nav");
        }

        mainHeadingTxt = (TextView) findViewById(R.id.main_heading);
        skipBtn = (TextView) findViewById(R.id.skip_btn);
        skipForFingerBtn = (TextView) findViewById(R.id.skip_btn_finger);
        mPinLockView = (PinLockView) findViewById(R.id.pin_lock_view);
        mIndicatorDots = (IndicatorDots) findViewById(R.id.indicator_dots);
        shimmerFrameLayout = (ShimmerLayout) findViewById(R.id.shimmer_view_container);
        fingerPrintLay = (LinearLayout) findViewById(R.id.fingerprint_enable_lay);
        passCodeLay = (LinearLayout) findViewById(R.id.pass_code_lay);
        switchCompat = (SwitchCompat) findViewById(R.id.switcherBtn);

        shimmerFrameLayout.startShimmerAnimation();

        mPinLockView.attachIndicatorDots(mIndicatorDots);
        mPinLockView.setPinLockListener(mPinLockListener);
        mPinLockView.setPinLength(4);

        mIndicatorDots.setIndicatorType(IndicatorDots.IndicatorType.FILL_WITH_ANIMATION);

        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        if (navStr.equalsIgnoreCase("3"))
        {
            fingerPrintLay.setVisibility(View.VISIBLE);
            passCodeLay.setVisibility(View.GONE);
        }

        if (fingerEnableStr.equalsIgnoreCase("True"))
        {
            switchCompat.setChecked(true);
        }else
            {
                switchCompat.setChecked(false);
            }

        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (navStr.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(ResetPasscodeActivity.this, VerificationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                }else if (navStr.equalsIgnoreCase("4"))
                {
                    Intent intent = new Intent(ResetPasscodeActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                    finish();

                }else
                {
                    onBackPressed();
                }
            }
        });

        skipForFingerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (navStr.equalsIgnoreCase("1")) {
                    Intent intent = new Intent(ResetPasscodeActivity.this, VerificationActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                }else if (navStr.equalsIgnoreCase("4"))
                {
                    Intent intent = new Intent(ResetPasscodeActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                    finish();

                }else if (navStr.equalsIgnoreCase("3"))
                {
                    Intent intent = new Intent(ResetPasscodeActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                    finish();
                }else
                {
                    finish();
                }
            }
        });

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                {
                    //set fingerPrint Enable
                    UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setFingerPrintEnable(ResetPasscodeActivity.this,"True")));

                    if (navStr.equalsIgnoreCase("1")) {
                        Intent intent = new Intent(ResetPasscodeActivity.this, VerificationActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                    }else if (navStr.equalsIgnoreCase("3"))
                    {
                        Intent intent = new Intent(ResetPasscodeActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                        finish();
                    }else
                    {
                        onBackPressed();
                    }
                }else
                    {
                        Toast.makeText(ResetPasscodeActivity.this, "Fingerprint is disabled", Toast.LENGTH_SHORT).show();
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setFingerPrintEnable(ResetPasscodeActivity.this,"False")));

                        if (navStr.equalsIgnoreCase("3"))
                        {
                            Intent intent = new Intent(ResetPasscodeActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                            finish();
                        }
                    }
            }
        });



    }

    private PinLockListener mPinLockListener = new PinLockListener() {
        @Override
        public void onComplete(String pin) {

            if (pin.length() == 4)
            {
                if (count)
                {
                    count = false;
                    mainHeadingTxt.setText("Confirm Your 4 Digit Passcode");
                    mainHeadingTxt.setTextColor(getResources().getColor(R.color.green_pass_btn));
                    pincode = pin;
                    mPinLockView.resetPinLockView();
                }else
                    {
                        if (pincode.equalsIgnoreCase(pin))
                        {
                            Toast.makeText(ResetPasscodeActivity.this, "Succes", Toast.LENGTH_SHORT).show();

                            if (conn.getConnectivityStatus()>0) {
                                setPassCode(userId, pin);
                            }else
                                {
                                    conn.showNoInternetAlret();
                                    count = true;
                                    mainHeadingTxt.setText("Create 4 Digit Passcode");
                                    mainHeadingTxt.setTextColor(getResources().getColor(R.color.yellow_color));
                                }

                        }else
                            {
                                count = true;
                                mPinLockView.resetPinLockView();
                                mainHeadingTxt.setText("Your 4 digit Passcode is not match! please try again");
                                mainHeadingTxt.setTextColor(getResources().getColor(R.color.red_color));
                                vib.vibrate(1000);
                                shakeAnimation();
                            }

                    }

            }
        }

        @Override
        public void onEmpty() {
            Log.e("ds", "Pin empty");
        }

        @Override
        public void onPinChange(int pinLength, String intermediatePin) {

            if (count) {
                mainHeadingTxt.setText("Create 4 Digit Passcode");
                mainHeadingTxt.setTextColor(getResources().getColor(R.color.yellow_color));
            }

            Log.e("sd", "Pin changed, new length " + pinLength + " with intermediate pin " + intermediatePin);
        }
    };


    private void shakeAnimation() {
        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake_anim);
        findViewById(R.id.main_heading).startAnimation(shake);
        Toast.makeText(this, "Wrong Password", Toast.LENGTH_SHORT).show();
    }

    public void setPassCode(String userId, final String passcode)
    {
        final ProgressDialog pDialog = new ProgressDialog(ResetPasscodeActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.setCancelable(false);
        pDialog.show();

        Call<PasscodeModel> call = retrofitClientInstance.setPassCodeApi(userId,passcode);
        call.enqueue(new Callback<PasscodeModel>() {
            @Override
            public void onResponse(Call<PasscodeModel> call, Response<PasscodeModel> response) {

                try {
                    if (response.body().getStatus().equalsIgnoreCase("1"))
                    {
                        //set passcode
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setPassCode(ResetPasscodeActivity.this,passcode)));

                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                            // Check whether the device has a Fingerprint sensor.
                            if (!fingerprintManager.isHardwareDetected()) {

                                fingerPrintLay.setVisibility(View.GONE);
                                passCodeLay.setVisibility(View.VISIBLE);

                                if (navStr.equalsIgnoreCase("1")) {
                                    Intent intent = new Intent(ResetPasscodeActivity.this, VerificationActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                                }else
                                {
                                    onBackPressed();
                                }

                            }else
                                {
                                    fingerPrintLay.setVisibility(View.VISIBLE);
                                    passCodeLay.setVisibility(View.GONE);
                                }
                        }else
                            {
                                if (navStr.equalsIgnoreCase("1")) {
                                    Intent intent = new Intent(ResetPasscodeActivity.this, VerificationActivity.class);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                                }else
                                    {
                                        onBackPressed();
                                    }
                            }
                    }else
                        {
                            Toast.makeText(ResetPasscodeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<PasscodeModel> call, Throwable t) {
                pDialog.dismiss();
            }
        });
    }


    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(ResetPasscodeActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(ResetPasscodeActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(ResetPasscodeActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        if (!navStr.equalsIgnoreCase("1")) {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav", "1");
                            startActivity(intent);
                        }
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            if (!navStr.equalsIgnoreCase("1")) {
                                Intent intent = new Intent(this, PassCodeActivity.class);
                                intent.putExtra("Nav", "1");
                                startActivity(intent);
                            }
                        }
                    }
                } else {
                    if (!navStr.equalsIgnoreCase("1")) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav", "1");
                        startActivity(intent);
                    }
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(ResetPasscodeActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ResetPasscodeActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(ResetPasscodeActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }



    @Override
    public void onBackPressed() {
        /*if (navStr.equalsIgnoreCase("4"))
        {*/
            Intent intent = new Intent(ResetPasscodeActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            finish();

       /* }else {
            super.onBackPressed();
        }*/
    }
}
