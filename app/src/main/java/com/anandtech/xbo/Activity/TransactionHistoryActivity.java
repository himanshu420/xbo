package com.anandtech.xbo.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.fingerprint.FingerprintManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Adapter.TransactionHistoryAdapter;
import com.anandtech.xbo.Adapter.TransactionListAdapter;
import com.anandtech.xbo.Model.TransactionHistoryResultListModel;
import com.anandtech.xbo.Model.TransactionListModel;
import com.anandtech.xbo.Repositories.TransactionHistoryRepostry;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.NetworkStateReceiver;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.anandtech.xbo.ViewModels.TransactionHistoryViewModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.PowerManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

public class TransactionHistoryActivity extends AppCompatActivity implements NetworkStateReceiver.NetworkStateReceiverListener {

    public ImageView backBtn;
    public TextView toolbatTitleTxt,mainBlanceTxt,recyclerHeadingTxt;
    FrameLayout noDataTxt;
    public TransactionListAdapter adapter;
    public RecyclerView trabnsactionHistory;
    public ConnectionDetector conn;
    public String userId = "",authKey = "";
    public TransactionHistoryViewModel transactionHistoryViewModel;
    public ArrayList<TransactionHistoryResultListModel> transactionList = new ArrayList<>();
    private NetworkStateReceiver networkStateReceiver;
    private androidx.appcompat.app.AlertDialog internetDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //Checking forced crash for crashlytics firebase report
        Fabric.with(TransactionHistoryActivity.this, new Crashlytics());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }


        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        internetDialog = new androidx.appcompat.app.AlertDialog.Builder(TransactionHistoryActivity.this).create();


        Toolbar toolbar = findViewById(R.id.transaction_toolbar);
        setSupportActionBar(toolbar);
        toolbatTitleTxt = (TextView) toolbar.findViewById(R.id.toolbar_txt);
        backBtn = (ImageView) toolbar.findViewById(R.id.back_btn);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        toolbatTitleTxt.setText("Transaction History");
        //getSupportActionBar().setElevation(0);

        conn = new ConnectionDetector(TransactionHistoryActivity.this);
        userId = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(TransactionHistoryActivity.this)));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(TransactionHistoryActivity.this)));
        //mainBlanaceStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getWalletAmount(TransactionHistoryActivity.this)));

        trabnsactionHistory = (RecyclerView) findViewById(R.id.recycler_for_transaction_list);
        mainBlanceTxt = (TextView) findViewById(R.id.main_balance);
        noDataTxt = (FrameLayout) findViewById(R.id.no_transaction);

        recyclerHeadingTxt = (TextView) findViewById(R.id.recycler_heading);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TransactionHistoryActivity.this);
        trabnsactionHistory.setLayoutManager(mLayoutManager);
        trabnsactionHistory.setItemAnimator(new DefaultItemAnimator());

        transactionHistoryViewModel = ViewModelProviders.of(TransactionHistoryActivity.this).get(TransactionHistoryViewModel.class);
        transactionHistoryViewModel.init(userId,authKey,TransactionHistoryActivity.this);

        transactionHistoryViewModel.getTransactionHistoryData().observe(TransactionHistoryActivity.this, new Observer<TransactionListModel>() {
            @Override
            public void onChanged(TransactionListModel transactionListModels) {

                Log.e("Checking the working","Yes Work.....");
                transactionList = transactionListModels.getResult();
                mainBlanceTxt.setText(transactionListModels.getUpdateBlnc());


                initRecyclerView();

            }
        });
    }

    public void initRecyclerView()
    {

        //transactionHistoryViewModel.getTransactionHistoryData().getValue()
        adapter = new TransactionListAdapter(transactionHistoryViewModel.getTransactionHistoryData().getValue().getResult(),TransactionHistoryActivity.this);
        trabnsactionHistory.setAdapter(adapter);

        if (transactionHistoryViewModel.getTransactionHistoryData().getValue().getResult().size() == 0)
        {
            noDataTxt.setVisibility(View.VISIBLE);
            recyclerHeadingTxt.setVisibility(View.GONE);
        }else
            {
                noDataTxt.setVisibility(View.GONE);
                recyclerHeadingTxt.setVisibility(View.VISIBLE);
            }
    }

    /* Working with api*/
   /* public void transactionHistory(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(TransactionHistoryActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, transactionHistoryUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Transaction History", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        String updatedbalance = jsonObject.getString("updatedbalance");
                        mainBlanceTxt.setText(updatedbalance);

                        if (transactionList.size()>0)
                        {
                            transactionList.clear();
                        }
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i= 0; i<jsonArray.length(); i++)
                        {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String transId = object.getString("transId");
                            String name = object.getString("name");
                            String amount = object.getString("amount");
                            String RemainingAmount = object.getString("RemainingAmount");
                            String CreatedDatetime = object.getString("CreatedDatetime");
                            String TransactionType = object.getString("TransactionType");
                            String PaymentStatus = object.getString("PaymentStatus");

                            transactionList.add(new TransactionListModel(transId,name,amount,RemainingAmount,CreatedDatetime,
                                    TransactionType,PaymentStatus));

                        }
                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        showLogoutDialog(message);
                    }else if (Status.equalsIgnoreCase("2"))
                    {
                        String updatedbalance = jsonObject.getString("updatedbalance");
                        mainBlanceTxt.setText(updatedbalance);
                    }else
                    {
                        Toast.makeText(TransactionHistoryActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                    if (transactionList == null || transactionList.size() == 0)
                    {
                        noDataTxt.setVisibility(View.VISIBLE);
                        recyclerHeadingTxt.setVisibility(View.GONE);
                        trabnsactionHistory.setVisibility(View.GONE);
                    }else
                        {
                            noDataTxt.setVisibility(View.GONE);
                            trabnsactionHistory.setVisibility(View.VISIBLE);
                            recyclerHeadingTxt.setVisibility(View.VISIBLE);
                        }


                    adapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();

            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("usrId", userId);
                params.put("LoginAuthKey",authKey);

                Log.e("Checking Params", params.toString());
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Transaction History");
    }*/

    /*@Override
    protected void onPause() {
        super.onPause();

        Log.e("Checking the Remove Observe", "Yes Remove This....");

        transactionHistoryViewModel.clearData();

       // transactionHistoryViewModel.getTransactionHistoryData().removeObservers(this);
    }*/

   /* @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("onDestroy()", "Call onDestroy....");

    }*/

    @SuppressLint("SetTextI18n")
    @Override
    protected void onStart() {
        super.onStart();

        String screenStatus =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getScreenStatus(TransactionHistoryActivity.this)));
        String fingerEnable =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getFingerPrintEnable(TransactionHistoryActivity.this)));
        String passCode =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(TransactionHistoryActivity.this)));

        Log.e("Get passCode", passCode + " Null");


        if (!passCode.equalsIgnoreCase("null")) {
            if (screenStatus.equalsIgnoreCase("True")) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);

                    // Check whether the device has a Fingerprint sensor.
                    if (!fingerprintManager.isHardwareDetected()) {
                        Intent intent = new Intent(this, PassCodeActivity.class);
                        intent.putExtra("Nav","1");
                        startActivity(intent);
                    } else {
                        if (fingerEnable.equalsIgnoreCase("True")) {
                            Intent intent = new Intent(this, FingerPrintEnableActivity.class);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(this, PassCodeActivity.class);
                            intent.putExtra("Nav","1");
                            startActivity(intent);
                        }
                    }
                } else {
                    Intent intent = new Intent(this, PassCodeActivity.class);
                    intent.putExtra("Nav","1");
                    startActivity(intent);
                }
            } else {
                Log.e("Result", "The code you typed is right!");
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();

        if (isScreenOn)
        {
            if (isAppIsInBackground(TransactionHistoryActivity.this))
            {
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(TransactionHistoryActivity.this,
                        "True")));
            }else {
                Log.e("Checking The Screen", "No BackGround");
            }
        }else
        {
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(TransactionHistoryActivity.this,
                    "True")));
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        transactionHistoryViewModel.getTransactionHistoryData().removeObservers(this);
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }


    @Override
    public void networkAvailable() {

        internetDialog.dismiss();
    }

    @Override
    public void networkUnavailable() {

        showInternetPopUp();
    }

    private void showInternetPopUp()
    {
        // Setting Dialog Title
        internetDialog.setTitle("Connection Failed");
        internetDialog.setMessage("Something is wrong with your internet connection. Please check your internet connection");
        internetDialog.setCancelable(false);

       /* // Setting OK Button
        internetDialog.setButton(Dialog.BUTTON_POSITIVE,"Retry", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                //internetDialog.dismiss();
                startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });*/

        // Showing Alert Message
        internetDialog.show();

    }
}
