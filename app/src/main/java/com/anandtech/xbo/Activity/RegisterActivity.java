package com.anandtech.xbo.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import com.anandtech.xbo.Model.CitySpinnerModel;
import com.anandtech.xbo.Model.CounterySpinnerModerl;
import com.anandtech.xbo.Model.StateListModel;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.transition.TransitionManager;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

public class RegisterActivity extends AppCompatActivity {
    public LinearLayout dobBtn;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    public TextView dobTxt,verifyText;
    public RelativeLayout cancelCheckBtn,govtBtn;
    private Bitmap bitmap;
   // private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    public ImageView firstIdImage,secondImage,firstCrossBtn,secondCrossBtn,cancelCheckThumbnail,cancekChckCrossBtn;
    public LinearLayout imageLay,cancelCheckForLay,cancelCheckLay;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String firstBase64="" , secondBase64="",cancelCheckBase64 = "";
    public ConstraintLayout constraintLayout;
    public LinearLayout cityLay,bankDetailsLay,cancelChqLay;
    Uri image;
   // public String counteryUrl = SettingConstant.BASEURL + "CountriesList";
    public String stateUrl = SettingConstant.BASEURL + "api/UserWebApi/StatesByCountryID";
    public String cityUrl = SettingConstant.BASEURL + "api/UserWebApi/CitiesByStateID";
    public String registerUrl = SettingConstant.BASEURL + "api/UserWebApi/PostUserRegister";
    public String verifyRefresnceIdURL = SettingConstant.BASEURL + "api/UserWebApi/CheckRefferalId";
    public ArrayList<StateListModel> stateList = new ArrayList<>();
    public SearchableSpinner counterySpinner,stateSpinner,citySpinner;
    public ArrayAdapter<CounterySpinnerModerl> counteryAdapter ;
    public ArrayAdapter<StateListModel> stateAdapter;
    public ArrayAdapter<CitySpinnerModel> cityAdapter;
    public ArrayList<CounterySpinnerModerl> counteryList = new ArrayList<>();
    public ArrayList<CitySpinnerModel> cityList = new ArrayList<>();
    public ConnectionDetector conn;
    public EditText nameTxt, emailIdTxt, passwordTxt, confirmPassTxt, phoneNumberTxt, addressTxt,bankNameTxt,accountNumberTxt,
            bankIfscCodeTxt,bankBranchNameTxt,refferTxt;
    public ImageView showPassBtn, showConfmPassBtn,verifyImage;
    public CheckBox checkBox;
    public RelativeLayout image1Lay, image2Lay;
    public Button registerBtn, bacToLoginBtn;
    public boolean flag1 = true,flag2 = true;
    public String counteryStr = "", stateStr = "", checkStr = "no", checkingStrForClickDoc = "",cityStr = "",
            countryIdStr = "",stateIdStr = "", cityIdStr = "",mCameraFileName = "",fileNameStr = "",radioBtnStr = "1",shareReferId = "";
    public RadioGroup radioGroup;
    public RadioButton cancelChqBtn, bankDetailsBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(R.color.first_gradient));
        }

        //Checking forced crash for crashlytics firebase report
        Fabric.with(RegisterActivity.this, new Crashlytics());

        conn = new ConnectionDetector(RegisterActivity.this);
        shareReferId = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getShareRefer(RegisterActivity.this)));

        Log.e("Checking Share Refer Id", shareReferId + " Null");

        dobBtn = (LinearLayout) findViewById(R.id.dob_btn);
        dobTxt = (TextView) findViewById(R.id.dob_txt);
        verifyText = (TextView) findViewById(R.id.text_for_verify);
        firstIdImage  = (ImageView) findViewById(R.id.first_id_image);
        secondImage = (ImageView) findViewById(R.id.second_id_image); 
        firstCrossBtn = (ImageView) findViewById(R.id.first_cross_btn);
        secondCrossBtn = (ImageView) findViewById(R.id.second_cross_btn); 
        imageLay = (LinearLayout) findViewById(R.id.image_layout);
        constraintLayout = (ConstraintLayout) findViewById(R.id.constraint);
        counterySpinner = (SearchableSpinner) findViewById(R.id.countery_spinner);
        stateSpinner = (SearchableSpinner) findViewById(R.id.state_spinner);
        citySpinner = (SearchableSpinner) findViewById(R.id.city_spinner);
        cityLay = (LinearLayout) findViewById(R.id.city_lay);
        nameTxt = (EditText) findViewById(R.id.nameTxt);
        emailIdTxt = (EditText) findViewById(R.id.emailIdTxt);
        passwordTxt = (EditText) findViewById(R.id.password);
        confirmPassTxt = (EditText) findViewById(R.id.confirm_password);
        phoneNumberTxt = (EditText) findViewById(R.id.phone_number);
        addressTxt = (EditText) findViewById(R.id.adress);
        bankNameTxt = (EditText) findViewById(R.id.bank_nametxt);
        refferTxt = (EditText) findViewById(R.id.reffer_txt);
        accountNumberTxt = (EditText) findViewById(R.id.bank_account_txt);
        bankIfscCodeTxt = (EditText) findViewById(R.id.bank_ifsc_code_txt);
        bankBranchNameTxt = (EditText) findViewById(R.id.bank_branch_name_txt);
        cancelCheckLay = (LinearLayout) findViewById(R.id.cancel_check_layout);
        bankDetailsLay =  (LinearLayout) findViewById(R.id.bank_details_lay);
        cancelCheckForLay = (LinearLayout) findViewById(R.id.cancel_chque_lay);
        cancelCheckThumbnail = (ImageView) findViewById(R.id.cancel_check_id_image);
        cancekChckCrossBtn = (ImageView) findViewById(R.id.cancel_cross_btn);
        verifyImage = (ImageView) findViewById(R.id.image_for_verify);
        image1Lay = (RelativeLayout) findViewById(R.id.image1lay);
        image2Lay = (RelativeLayout) findViewById(R.id.image2lay);
        cancelCheckBtn = (RelativeLayout) findViewById(R.id.cancelCheck_btn);
        govtBtn = (RelativeLayout) findViewById(R.id.govt_btn);
        checkBox = (CheckBox) findViewById(R.id.check_bx);
        registerBtn = (Button) findViewById(R.id.register_Btn);
        bacToLoginBtn = (Button) findViewById(R.id.loginBtn);
        showPassBtn = (ImageView) findViewById(R.id.show_pass_btn);
        showConfmPassBtn = (ImageView) findViewById(R.id.show_conf_pass);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        cancelChqBtn = (RadioButton) findViewById(R.id.cancel_chque_btn);
        bankDetailsBtn = (RadioButton) findViewById(R.id.bank_details_btn);

        if (shareReferId.length() == 6)
        {
            refferTxt.setText(shareReferId);
            refferTxt.setEnabled(false);

            verifyText.setVisibility(View.VISIBLE);
            verifyImage.setVisibility(View.VISIBLE);
            verifyImage.setBackgroundResource(R.drawable.right_green);
          //  verifyImage.setPadding(R.dimen._3sdp,R.dimen._3sdp,R.dimen._3sdp,R.dimen._3sdp);
            verifyText.setTextColor(Color.parseColor("#43a047"));
        }

        cancelChqBtn.setChecked(true);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.cancel_chque_btn)
                {
                    radioBtnStr = "1";
                    cancelCheckForLay.setVisibility(View.VISIBLE);
                    bankDetailsLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else if (checkedId == R.id.bank_details_btn)
                {
                    radioBtnStr = "2";
                    cancelCheckForLay.setVisibility(View.GONE);
                    bankDetailsLay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else if (checkedId == R.id.later)
                {
                    radioBtnStr = "3";
                    cancelCheckForLay.setVisibility(View.GONE);
                    bankDetailsLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }
            }
        });


        showPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag1)
                {
                    showPassBtn.setImageResource(R.drawable.ic_hide_pass);
                    passwordTxt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    passwordTxt.setSelection(passwordTxt.getText().length());
                    flag1 = false;

                }else
                {
                    showPassBtn.setImageResource(R.drawable.ic_show_pass);
                    passwordTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    passwordTxt.setSelection(passwordTxt.getText().length());
                    flag1 = true;
                }
            }
        });

        showConfmPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (flag1)
                {
                    showConfmPassBtn.setImageResource(R.drawable.ic_hide_pass);
                    confirmPassTxt.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confirmPassTxt.setSelection(confirmPassTxt.getText().length());
                    flag1 = false;

                }else
                {
                    showConfmPassBtn.setImageResource(R.drawable.ic_show_pass);
                    confirmPassTxt.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
                    confirmPassTxt.setSelection(confirmPassTxt.getText().length());
                    flag1 = true;
                }
            }
        });


        counteryList = SharedPrefs.getCountryList(RegisterActivity.this);

        //Countery List Spinner
        //stateSpinner.getBackground().setColorFilter(getResources().getColor(R.color.status_color), PorterDuff.Mode.SRC_ATOP);
        //checking the android os version...
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            counteryAdapter = new ArrayAdapter<CounterySpinnerModerl>(RegisterActivity.this, R.layout.customizespinner, counteryList);
            counteryAdapter.setDropDownViewResource(R.layout.customizespinner);
            counterySpinner.setAdapter(counteryAdapter);

            //state List Spinner
            stateList.add(new StateListModel("0", "Please Select State"));
            stateAdapter = new ArrayAdapter<StateListModel>(RegisterActivity.this, R.layout.customizespinner, stateList);
            stateAdapter.setDropDownViewResource(R.layout.customizespinner);
            stateSpinner.setAdapter(stateAdapter);

            //City List Spinner
            cityList.add(new CitySpinnerModel("0", "Please Select City"));
            cityAdapter = new ArrayAdapter<CitySpinnerModel>(RegisterActivity.this, R.layout.customizespinner, cityList);
            cityAdapter.setDropDownViewResource(R.layout.customizespinner);
            citySpinner.setAdapter(cityAdapter);

        }else
            {
                counteryAdapter = new ArrayAdapter<CounterySpinnerModerl>(RegisterActivity.this, R.layout.support_simple_spinner_dropdown_item, counteryList);
                counteryAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                counterySpinner.setAdapter(counteryAdapter);

                //state List Spinner
                stateList.add(new StateListModel("0", "Please Select State"));
                stateAdapter = new ArrayAdapter<StateListModel>(RegisterActivity.this, R.layout.support_simple_spinner_dropdown_item, stateList);
                stateAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                stateSpinner.setAdapter(stateAdapter);

                //City List Spinner
                cityList.add(new CitySpinnerModel("0", "Please Select City"));
                cityAdapter = new ArrayAdapter<CitySpinnerModel>(RegisterActivity.this, R.layout.support_simple_spinner_dropdown_item, cityList);
                cityAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                citySpinner.setAdapter(cityAdapter);
            }

        counterySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                stateAdapter.clear();
                stateAdapter.notifyDataSetChanged();
                counteryStr = counteryList.get(position).getCounteryName();
                countryIdStr = counteryList.get(position).getCounteryId();
                getStateList(counteryList.get(position).getCounteryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked)
                {
                    checkStr = "yes";
                }else
                    {
                     checkStr = "no";
                    }
            }
        });

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cityAdapter.clear();
                cityAdapter.notifyDataSetChanged();
                stateStr = stateList.get(position).getStateName();
                stateIdStr = stateList.get(position).getStateId();
                getCityList(stateList.get(position).getStateId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cityStr = cityList.get(position).getCityName();
                cityIdStr = cityList.get(position).getCityId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dobBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                // TODO Auto-generated method stub
                Calendar newCalendar = Calendar.getInstance();
                fromDatePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dobTxt.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                        newCalendar.get(Calendar.DAY_OF_MONTH));
                fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                fromDatePickerDialog.show();


            }
        });
        
        cancelCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkingStrForClickDoc = "1";
                
                if (!cancelCheckBase64.equalsIgnoreCase(""))
                { 
                    Toast.makeText(RegisterActivity.this, "You have already selected cancel cheque image", Toast.LENGTH_SHORT).show();
                }else {
                    selectImage();

                    //change the name of create file with the help of Variable Name.
                    fileNameStr = "CancelCheque";

                }
            }
        });
        
        cancelCheckThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelCheckBase64 = "";
                if (cancelCheckBase64.equalsIgnoreCase(""))
                {
                    cancelCheckLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else
                {
                    // firstIdImage.setVisibility(View.GONE);

                    cancelCheckThumbnail.setImageResource(android.R.color.transparent);
                }
            }
        });

        govtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkingStrForClickDoc = "2";

                if (!firstBase64.equalsIgnoreCase("") && !secondBase64.equalsIgnoreCase("")) {

                    Toast.makeText(RegisterActivity.this, "You have already selected id Proof", Toast.LENGTH_SHORT).show();
                }else
                    {
                        selectImage();

                        //change the name of create file with the help of below condition.
                        if (firstBase64.equalsIgnoreCase(""))
                        {
                            fileNameStr = "FirstIdProof";
                        }else if (secondBase64.equalsIgnoreCase(""))
                        {
                            fileNameStr = "SecondIdProof";
                        }
                    }
            }
        });
        
        firstIdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                firstBase64 = "";
                if (firstBase64.equalsIgnoreCase("") && secondBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else if (!firstBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.VISIBLE);
                    image2Lay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else if (!secondBase64.equalsIgnoreCase(""))
                {

                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.GONE);
                    image2Lay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else
                {
                    // firstIdImage.setVisibility(View.GONE);

                    firstIdImage.setImageResource(android.R.color.transparent);
                }
               /* if (firstBase64.equalsIgnoreCase("") && secondBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else
                    {
                       // firstIdImage.setVisibility(View.GONE);

                        firstIdImage.setImageResource(android.R.color.transparent);
                    }*/
            }
        });
        
        secondImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                secondBase64 = "";
                if (firstBase64.equalsIgnoreCase("") && secondBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else if (!firstBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.VISIBLE);
                    image2Lay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else if (!secondBase64.equalsIgnoreCase(""))
                {

                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.GONE);
                    image2Lay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else
                {
                    //secondImage.setVisibility(View.GONE);
                    secondImage.setImageResource(android.R.color.transparent);
                }
               /* if (firstBase64.equalsIgnoreCase("") && secondBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else
                {
                    //secondImage.setVisibility(View.GONE);
                    secondImage.setImageResource(android.R.color.transparent);
                }*/
            }
        });

        refferTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length() == 6) {
                    getVerifyRefrenceId(refferTxt.getText().toString().trim());
                }else if (s.toString().length() == 0)
                {
                    verifyText.setVisibility(View.GONE);
                    verifyImage.setVisibility(View.GONE);

                }else if (s.toString().length()<=5)
                {
                    verifyText.setVisibility(View.VISIBLE);
                    verifyImage.setVisibility(View.VISIBLE);

                    verifyImage.setBackgroundResource(R.drawable.cross_red);
                    verifyText.setText("Invalid Reference Code");
                    verifyText.setTextColor(getResources().getColor(R.color.red_color));
                }else
                    {
                        verifyText.setVisibility(View.GONE);
                        verifyImage.setVisibility(View.GONE);
                    }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nameTxt.getText().toString().equalsIgnoreCase(""))
                {
                    nameTxt.setError("Please enter valid name");
                    nameTxt.requestFocus();
                }else if (!isValidEmail(emailIdTxt.getText().toString().trim()))
                {
                    emailIdTxt.setError("Please Enter Valid Email ID");
                    emailIdTxt.requestFocus();
                }else if (passwordTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    passwordTxt.setError("Please enter password");
                    passwordTxt.requestFocus();
                }else if (passwordTxt.getText().toString().trim().length() < 5)
                {
                    passwordTxt.setError("Password should have minimum five character.");
                    passwordTxt.requestFocus();
                }else if (!passwordTxt.getText().toString().trim().equalsIgnoreCase(confirmPassTxt.getText().toString().trim()))
                {
                    confirmPassTxt.setError("Passwords do not match");
                    confirmPassTxt.requestFocus();
                }else if (dobTxt.getText().toString().trim().equalsIgnoreCase("D.O.B *"))
                {
                    Toast.makeText(RegisterActivity.this, "Please enter date of birth", Toast.LENGTH_SHORT).show();
                    dobTxt.requestFocus();
                }else if (phoneNumberTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    phoneNumberTxt.setError("Please enter phone number");
                    phoneNumberTxt.requestFocus();
                }else if (addressTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    addressTxt.setError("Please enter address");
                    addressTxt.requestFocus();
                }else if (counteryStr.equalsIgnoreCase("Please Select Country"))
                {
                    Toast.makeText(RegisterActivity.this, "Please select Country.", Toast.LENGTH_SHORT).show();
                }else if (stateStr.equalsIgnoreCase("Please Select State"))
                {
                    Toast.makeText(RegisterActivity.this, "Please select state", Toast.LENGTH_SHORT).show();
                }else
                    {

                        if (radioBtnStr.equalsIgnoreCase("1")) {

                            if (cancelCheckBase64.equalsIgnoreCase(""))
                            {
                                Toast.makeText(RegisterActivity.this, "Please select cancel chque image.", Toast.LENGTH_SHORT).show();
                            }else if (checkStr.equalsIgnoreCase("no"))
                            {
                                Toast.makeText(RegisterActivity.this, "Please check term and condition", Toast.LENGTH_SHORT).show();
                            }else {
                                if (conn.getConnectivityStatus() > 0) {
                                    setUserRegister(nameTxt.getText().toString().trim(), emailIdTxt.getText().toString().trim(),
                                            addressTxt.getText().toString().trim(), dobTxt.getText().toString().trim(),
                                            phoneNumberTxt.getText().toString().trim(),
                                            passwordTxt.getText().toString().trim(), firstBase64, secondBase64,
                                            cancelCheckBase64, countryIdStr, stateIdStr, cityIdStr, "", "",
                                            "", "", refferTxt.getText().toString().trim(),currentDate());
                                } else {
                                    conn.showNoInternetAlret();
                                }
                            }

                        }else if (radioBtnStr.equalsIgnoreCase("2"))
                            {
                                if (bankNameTxt.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    bankNameTxt.setError("Please enter bank name");
                                    bankNameTxt.requestFocus();
                                }else if (accountNumberTxt.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    accountNumberTxt.setError("Please enter bank account number");
                                    accountNumberTxt.requestFocus();
                                }else if (bankIfscCodeTxt.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    bankIfscCodeTxt.setError("Please enter bank IFSC code");
                                    bankIfscCodeTxt.requestFocus();
                                }else if (bankBranchNameTxt.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    bankBranchNameTxt.setError("Please enter bank branch name");
                                    bankBranchNameTxt.requestFocus();
                                }else if (checkStr.equalsIgnoreCase("no"))
                                {
                                    Toast.makeText(RegisterActivity.this, "Please check term and condition", Toast.LENGTH_SHORT).show();
                                }else
                                    {
                                        if (conn.getConnectivityStatus() > 0) {
                                            setUserRegister(nameTxt.getText().toString().trim(), emailIdTxt.getText().toString().trim(),
                                                    addressTxt.getText().toString().trim(), dobTxt.getText().toString().trim(),
                                                    phoneNumberTxt.getText().toString().trim(),
                                                    passwordTxt.getText().toString().trim(), firstBase64, secondBase64,
                                                    cancelCheckBase64, countryIdStr, stateIdStr, cityIdStr,bankNameTxt.getText().toString().trim()
                                                    ,accountNumberTxt.getText().toString().trim(),
                                                    bankIfscCodeTxt.getText().toString().trim(),
                                                    bankBranchNameTxt.getText().toString().trim(),refferTxt.getText().toString().trim(),currentDate());
                                        } else {
                                            conn.showNoInternetAlret();
                                        }
                                    }
                            }else {
                            if (checkStr.equalsIgnoreCase("no")) {
                                Toast.makeText(RegisterActivity.this, "Please check term and condition", Toast.LENGTH_SHORT).show();
                            } else {
                                if (conn.getConnectivityStatus() > 0) {
                                    setUserRegister(nameTxt.getText().toString().trim(), emailIdTxt.getText().toString().trim(),
                                            addressTxt.getText().toString().trim(), dobTxt.getText().toString().trim(),
                                            phoneNumberTxt.getText().toString().trim(),
                                            passwordTxt.getText().toString().trim(), firstBase64, secondBase64,
                                            "", countryIdStr, stateIdStr, cityIdStr, "", "",
                                            "", "", refferTxt.getText().toString().trim(),currentDate());
                                } else {
                                    conn.showNoInternetAlret();
                                }
                            }
                        }
                    }
            }
        });

        bacToLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

   /* public void getCounteryName()
    {
        final ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, counteryUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Country Name", response);

                    if (counteryList.size()>0)
                    {
                        counteryList.clear();
                    }
                    counteryList.add(new CounterySpinnerModerl("0","Please Select Country"));

                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String name = object.getString("name");

                        counteryList.add(new CounterySpinnerModerl(id,name));
                    }

                    counteryAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        });
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");


    }*/

    public void getStateList(final String counteryId)
    {
        final ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, stateUrl + "?CountryId=" + counteryId , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("State Name", response);
                    Log.e("State Name", counteryId);

                    if (stateList.size()>0)
                    {
                        stateList.clear();
                    }
                    stateList.add(new StateListModel("0","Please Select State"));
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String name = object.getString("name");

                        stateList.add(new StateListModel(id,name));
                    }

                    stateAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

               pDialog.dismiss();


            }
        })/*{
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("CountryId", counteryId);

                 Log.e("Checking the Map", counteryId);
                return params;
            }

        }*/;
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }


    public void getCityList(final String stateId)
    {
        /*final ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();*/

        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, cityUrl + "?stateId=" + stateId , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("City Name", response);

                    if (cityList.size()>0)
                    {
                        cityList.clear();
                    }
                    cityList.add(new CitySpinnerModel("0","Please Select City"));
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String name = object.getString("name");

                        cityList.add(new CitySpinnerModel(id,name));
                    }

                    Log.e("checking the city list" , cityList.size() + "");

                    if (cityList.size() == 1)
                    {
                        cityLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }else
                        {
                            cityLay.setVisibility(View.VISIBLE);
                            TransitionManager.beginDelayedTransition(constraintLayout);
                        }

                    cityAdapter.notifyDataSetChanged();
                    //pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                //pDialog.dismiss();


            }
        })/*{
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("CountryId", counteryId);

                 Log.e("Checking the Map", counteryId);
                return params;
            }

        }*/;
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }

    public void setUserRegister(final String name,final String emailId, final String address, final String dob, final String phoneNo,
                                final String password, final String idproof1, final String idproof2,
                                final String cancelCheck, final String counteryId, final String stateId,
                                final String cityId, final String bankName, final String bankAccountNo, final String bankIfscCode,
                                final String bankBranchName, final String shareReferId,final String date)
    {

        Log.e("Cancel Cheque", cancelCheck);
        Log.e("Id Proof1", idproof1);
        Log.e("Id Proof2", idproof2);

        final ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, registerUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("registerUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        String UserID = jsonObject.getString("UserID");

                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setOldPassword(RegisterActivity.this,
                                password)));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserId(RegisterActivity.this,
                                UserID)));

                        //call Popup

                        if(!((Activity) RegisterActivity.this).isFinishing()) {
                            showEnablePassCode(Message);
                        }

                        //Toast.makeText(RegisterActivity.this, Message, Toast.LENGTH_SHORT).show();


                      /* Intent intent = new Intent(RegisterActivity.this, VerificationActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);*/

                    }else
                    {
                        Toast.makeText(RegisterActivity.this, Message, Toast.LENGTH_SHORT).show();
                    }



                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Address", address);
                params.put("CancelCheque", cancelCheck);
                params.put("Dob", dob);
                params.put("EmailId", emailId);
                params.put("Idproof1", idproof1);
                params.put("Idproof2", idproof2);
                params.put("Name", name);
                params.put("PhoneNumber", phoneNo);
                params.put("password", password);
                params.put("CountryID", counteryId);
                params.put("StateID", stateId);
                params.put("CityID", cityId);
                params.put("BankName", bankName);
                params.put("AccountNo", bankAccountNo);
                params.put("IFSCcode", bankIfscCode);
                params.put("BankBranchName", bankBranchName);
                params.put("ShareRefferalID",shareReferId);
                params.put("TransactionDateTime",date);

                Log.e("Checking params", params.toString());
                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }

    /* Verify Refrence Id*/
    public void getVerifyRefrenceId(final String shareReferId)
    {
        final ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, verifyRefresnceIdURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Verify Refrence Code", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");
                    if (Status.equalsIgnoreCase("1"))
                    {
                        verifyText.setVisibility(View.VISIBLE);
                        verifyImage.setVisibility(View.VISIBLE);
                        verifyImage.setBackgroundResource(R.drawable.right_green);
                      //  verifyImage.setPadding(R.dimen._3sdp,R.dimen._3sdp,R.dimen._3sdp,R.dimen._3sdp);
                        verifyText.setTextColor(Color.parseColor("#43a047"));
                        verifyText.setText("Valid Reference Code");

                    }else
                        {

                            verifyText.setVisibility(View.VISIBLE);
                            verifyImage.setVisibility(View.VISIBLE);
                            verifyImage.setBackgroundResource(R.drawable.cross_red);
                            verifyText.setText("Invalid Reference Code");
                            verifyText.setTextColor(getResources().getColor(R.color.red_color));
                            Toast.makeText(RegisterActivity.this, Message, Toast.LENGTH_SHORT).show();
                        }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ShareRefferalID", shareReferId);

                 Log.e("Checking the Map", params.toString());
                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Verify refrence ID");

    }


    private void selectImage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_CAMERA_REQUEST_CODE);
            }else {

                try {
                    PackageManager pm = getPackageManager();
                    int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                    if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setTitle("Select Option");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (options[item].equals("Take Photo")) {
                                    dialog.dismiss();
                                    cameraIntent();
                                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                                } else if (options[item].equals("Choose From Gallery")) {
                                    dialog.dismiss();
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                                } else if (options[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    } else
                        Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }else
            {
                try {
                    PackageManager pm = getPackageManager();
                    int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
                    if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        builder.setTitle("Select Option");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (options[item].equals("Take Photo")) {
                                    dialog.dismiss();
                                    cameraIntent();
                                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                                } else if (options[item].equals("Choose From Gallery")) {
                                    dialog.dismiss();
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                                } else if (options[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    } else
                        Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setTitle("Select Option");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {
                                dialog.dismiss();
                                cameraIntent();
                               /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                            } else if (options[item].equals("Choose From Gallery")) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                            } else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();

                } else {

                    Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();

                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
            cancelCheckLay.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(constraintLayout);
        } else {
            imageLay.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(constraintLayout);
        }
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                /*Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");*/

                String checkStatus = "";

                if (data != null) {
                    image = data.getData();
                    checkStatus = "close";
                } else {
                    checkStatus = "Open";
                }
                if (image == null && mCameraFileName != null) {
                    image = Uri.fromFile(new File(mCameraFileName));
                }
                File file = new File(mCameraFileName);
                if (!file.exists()) {
                    file.mkdir();
                }

                InputStream image_stream = getContentResolver().openInputStream(image);
                bitmap = BitmapFactory.decodeStream(image_stream);
                bitmap = Bitmap.createScaledBitmap(bitmap, 640, 360, true);


                if (checkStatus.equalsIgnoreCase("Open")) {
                    if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                        cancelCheckBase64 = convert(bitmap);
                        cancelCheckThumbnail.setImageBitmap(bitmap);

                        image = null;

                    } else {

                        if (firstBase64.equalsIgnoreCase("")) {
                            firstBase64 = convert(bitmap);
                            firstIdImage.setImageBitmap(bitmap);
                            image = null;
                            imageLay.setVisibility(View.VISIBLE);
                            image1Lay.setVisibility(View.VISIBLE);

                        } else {
                            secondBase64 = convert(bitmap);
                            secondImage.setImageBitmap(bitmap);
                            image = null;
                            imageLay.setVisibility(View.VISIBLE);
                            image2Lay.setVisibility(View.VISIBLE);

                        }

                    }
                } else {
                    if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                        cancelCheckLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    } else {
                        if (!firstBase64.equalsIgnoreCase("") && !secondBase64.equalsIgnoreCase("")) {
                            imageLay.setVisibility(View.VISIBLE);
                            image1Lay.setVisibility(View.VISIBLE);
                            image2Lay.setVisibility(View.VISIBLE);
                            TransitionManager.beginDelayedTransition(constraintLayout);

                        } else if (!firstBase64.equalsIgnoreCase("")) {
                            imageLay.setVisibility(View.VISIBLE);
                            image1Lay.setVisibility(View.VISIBLE);
                            image2Lay.setVisibility(View.GONE);
                            TransitionManager.beginDelayedTransition(constraintLayout);
                        } else if (!secondBase64.equalsIgnoreCase("")) {

                            imageLay.setVisibility(View.VISIBLE);
                            image1Lay.setVisibility(View.GONE);
                            image2Lay.setVisibility(View.VISIBLE);
                            TransitionManager.beginDelayedTransition(constraintLayout);

                        } else {
                            imageLay.setVisibility(View.GONE);
                            TransitionManager.beginDelayedTransition(constraintLayout);
                        }

                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            try {
                Uri selectedImage = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                //compressed image
                // int nh = (int) ( bitmap.getHeight() * (700.0 / bitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, 500, true);

                if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                    cancelCheckBase64 = convert(scaled);
                    cancelCheckThumbnail.setImageBitmap(scaled);
                } else {

                    if (firstBase64.equalsIgnoreCase("")) {
                        firstBase64 = convert(scaled);
                        firstIdImage.setImageBitmap(scaled);
                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.VISIBLE);
                    } else {
                        secondBase64 = convert(scaled);
                        secondImage.setImageBitmap(scaled);
                        imageLay.setVisibility(View.VISIBLE);
                        image2Lay.setVisibility(View.VISIBLE);
                    }
                }


            } catch (Exception e) {
                if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                    cancelCheckLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                } else {
                    if (!firstBase64.equalsIgnoreCase("") && !secondBase64.equalsIgnoreCase("")) {
                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.VISIBLE);
                        image2Lay.setVisibility(View.VISIBLE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                    } else if (!firstBase64.equalsIgnoreCase("")) {
                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.VISIBLE);
                        image2Lay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    } else if (!secondBase64.equalsIgnoreCase("")) {

                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.GONE);
                        image2Lay.setVisibility(View.VISIBLE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                    } else {
                        imageLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }
                    e.printStackTrace();
                }
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String convert(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    /* validation method*/
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return !TextUtils.isEmpty(s) && PASSWORD_PATTERN.matcher(s).matches();
    }

    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

      /*  Date date = new Date();
        DateFormat df = new SimpleDateFormat("-mm-ss");*/

        String newPicFile = fileNameStr + ".jpg";
        String outPath = "/sdcard/" + newPicFile;
        File outFile = new File(outPath);

        mCameraFileName = outFile.toString();
        Uri outuri = Uri.fromFile(outFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outuri);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);
    }

    private void showCustomDialog(String message) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(RegisterActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = RegisterActivity.this.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

                Intent intent = new Intent(RegisterActivity.this, VerificationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);

            }
        });
    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }

    private void showEnablePassCode(final String Message) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(RegisterActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.passcode_enable_popup, null);

        /* TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);*/
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);
        Button noBtn = (Button) view.findViewById(R.id.no_btn);


        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

                Intent intent = new Intent(RegisterActivity.this, ResetPasscodeActivity.class);
                intent.putExtra("Nav","1");
                startActivity(intent);
                //finish();


            }
        });

        noBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();
                //call Popup
                if(!((Activity) RegisterActivity.this).isFinishing()) {
                    showCustomDialog(Message);
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_left_in,
                R.anim.push_right_out);
    }


}
