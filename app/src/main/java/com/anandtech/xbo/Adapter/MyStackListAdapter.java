package com.anandtech.xbo.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anandtech.xbo.Activity.HomeActivity;
import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.MyStackDeatilsActivity;
import com.anandtech.xbo.Model.MyStackListModel;
import com.anandtech.xbo.Model.MyStatusListDataModel;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class MyStackListAdapter extends RecyclerView.Adapter<MyStackListAdapter.ViewHolder>
{
    public Activity activity;
    public ArrayList<MyStatusListDataModel> list = new ArrayList<>();

    public MyStackListAdapter(Activity activity, ArrayList<MyStatusListDataModel> list)
    {
        this.activity = activity;
        this.list = list;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.my_stacks_list_item,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final MyStatusListDataModel model = list.get(position);

        holder.nameTxt.setText(model.getName());
        holder.emailIDTxt.setText(model.getEmailId());
        holder.phoneNoTxt.setText(model.getPhoneNumber());

       String userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(activity)));

       if (model.getReferenceId().equalsIgnoreCase(userId) && model.getAgentReferenceId().equalsIgnoreCase("")) {
           holder.colorTile.setBackgroundResource(R.color.second_gradient);
           holder.userTypeTxt.setText("Joiner");
       } else if (model.getReferenceId().equalsIgnoreCase(userId) && model.getAgentReferenceId().equalsIgnoreCase(userId))
       {
           holder.colorTile.setBackgroundResource(R.color.sky_blue);
           holder.userTypeTxt.setText("Sub-Agent");
       }else {

           holder.colorTile.setBackgroundResource(R.color.olive_color);
           holder.userTypeTxt.setText("Sub-Joiner");
       }


        holder.detailBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, MyStackDeatilsActivity.class);
                intent.putExtra("Name", model.getName());
                intent.putExtra("ProfilePic", model.getProfilePicture());
                intent.putExtra("Email", model.getEmailId());
                intent.putExtra("PhoneNo", model.getPhoneNumber());
                intent.putExtra("CountryName", model.getCountryName());
                intent.putExtra("Amount", model.getAmount());
                intent.putExtra("StateName", model.getStateName());
                intent.putExtra("CityName", model.getCityName());
                intent.putExtra("TotalXboToken", model.getTotalXboToken());
                intent.putExtra("WalletAddress", model.getWalletAddress());
                intent.putExtra("CreatedDateTime", model.getCreatedDateTime());
                intent.putExtra("JoinId", model.getJoinId());
                intent.putExtra("IntroducerName",model.getRefrenceUserName());
                intent.putExtra("RefrenceId",model.getReferenceId());
                intent.putExtra("AgentRefrenceId",model.getAgentReferenceId());
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public CardView detailBtn;
        public RelativeLayout colorTile;
        public TextView nameTxt, emailIDTxt,phoneNoTxt,userTypeTxt;

        public ViewHolder(View itemView) {
            super(itemView);

            detailBtn = (CardView) itemView.findViewById(R.id.detailBtn);

            nameTxt = (TextView) itemView.findViewById(R.id.nameTxt);
            emailIDTxt = (TextView) itemView.findViewById(R.id.emailIdTxt);
            phoneNoTxt = (TextView) itemView.findViewById(R.id.phone_number);
            colorTile = (RelativeLayout) itemView.findViewById(R.id.color_tile);
            userTypeTxt = (TextView) itemView.findViewById(R.id.user_type);

        }
    }

    //convert date time to month
    public String convertDate(String dtStart)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

            String date1 = sdf.format(date);

            return date1;


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

}
