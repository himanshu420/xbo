package com.anandtech.xbo.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anandtech.xbo.Model.TransactionHistoryModel;
import com.anandtech.xbo.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class TransactionHistoryAdapter extends RecyclerView.Adapter<TransactionHistoryAdapter.ViewHolder>
{
    public ArrayList<TransactionHistoryModel> list = new ArrayList<>();
    public Activity activity;

    public TransactionHistoryAdapter(ArrayList<TransactionHistoryModel> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.transaction_history_layout,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        TransactionHistoryModel model = list.get(position);

        if (model.getPaymentType().equalsIgnoreCase("Bank"))
        {
            holder.xboCoinImg.setVisibility(View.GONE);

        }else if (model.getPaymentType().equalsIgnoreCase("CryptoCurrency"))
        {
            holder.xboCoinImg.setVisibility(View.VISIBLE);
        }

        holder.dateTxt.setText(convertDateForTransactionHistory(model.getDate()));
        holder.amountTxt.setText(model.getAmount());
        holder.paymentTypeTxt.setText(model.getPaymentType());
        holder.refNoTxt.setText(model.getRefNo());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView dateTxt, refNoTxt, amountTxt,paymentTypeTxt;
        public ImageView xboCoinImg;

        public ViewHolder(View itemView) {
            super(itemView);

            dateTxt = (TextView) itemView.findViewById(R.id.datetxt);
            refNoTxt = (TextView) itemView.findViewById(R.id.ref_codetxt);
            paymentTypeTxt = (TextView) itemView.findViewById(R.id.payment_type);
            amountTxt = (TextView) itemView.findViewById(R.id.amount);
            xboCoinImg = (ImageView) itemView.findViewById(R.id.xbo_coin);
        }
    }

    //convert date time to month
    public String convertDateForTransactionHistory(String dtStart)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

            String date1 = sdf.format(date);

            return date1;


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }
}
