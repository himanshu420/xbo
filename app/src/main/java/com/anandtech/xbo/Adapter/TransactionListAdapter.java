package com.anandtech.xbo.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.anandtech.xbo.Activity.TransactionHistoryActivity;
import com.anandtech.xbo.Activity.TransactionPageDetailsActivity;
import com.anandtech.xbo.Model.TransactionHistoryResultListModel;
import com.anandtech.xbo.Model.TransactionListModel;
import com.anandtech.xbo.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class TransactionListAdapter extends RecyclerView.Adapter<TransactionListAdapter.ViewHolder>
{

    public ArrayList<TransactionHistoryResultListModel> transactionList = new ArrayList<>();
    public Activity activity;

    public TransactionListAdapter(ArrayList<TransactionHistoryResultListModel> transactionList, Activity activity) {
        this.transactionList = transactionList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.transaction_history_item_layout,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final TransactionHistoryResultListModel model = transactionList.get(position);

        holder.dateTxt.setText(model.getCreateDateTime());
        holder.remaingRsTxt.setText(model.getRemaningAmount());

        if (model.getPaymentStatus().equalsIgnoreCase("Success")) {
            holder.statusTxt.setText(model.getPaymentStatus());
            holder.statusTxt.setTextColor(Color.parseColor("#1fc812"));
        }else if (model.getPaymentStatus().equalsIgnoreCase("Pending"))
            {
                holder.statusTxt.setText(model.getPaymentStatus());
                holder.statusTxt.setTextColor(Color.parseColor("#ff9800"));
            }else
                {
                    holder.statusTxt.setText(model.getPaymentStatus());
                    holder.statusTxt.setTextColor(Color.parseColor("#fb1b1b"));
                }

        if (model.getFundingType().equalsIgnoreCase("XboWallet"))
        {
        if (model.getName().equalsIgnoreCase("Wallet"))
        {
            holder.layoutShape.setBackgroundResource(R.drawable.add_wallet_layout_shape);
            holder.amountRsTxt.setText("+" + model.getAmount());
            holder.amountRsTxt.setTextColor(Color.parseColor("#08b3d3"));
            holder.mainHeadingTxt.setText("Added to your " + model.getName());

            holder.chnageSenderImg.setImageResource(R.drawable.ic_bank);

        }else {

            if (model.getTransactionType().equalsIgnoreCase("true")) {
                holder.layoutShape.setBackgroundResource(R.drawable.sender_layout_shape);
                holder.amountRsTxt.setText("-" + model.getAmount());
                holder.amountRsTxt.setTextColor(Color.parseColor("#fb1b1b"));
                holder.mainHeadingTxt.setText("XBO sent to " + model.getName());

                holder.chnageSenderImg.setImageResource(R.drawable.ic_send);

            } else {
                holder.layoutShape.setBackgroundResource(R.drawable.reciver_layout_shape);
                holder.amountRsTxt.setText("+" + model.getAmount());
                holder.amountRsTxt.setTextColor(Color.parseColor("#1fc812"));
                holder.mainHeadingTxt.setText("XBO Received from " + model.getName());
                holder.chnageSenderImg.setImageResource(R.drawable.ic_recieve);
            }
        }
        }else if (model.getFundingType().equalsIgnoreCase("Bank"))
        {


            if (model.getWalletAddress().equalsIgnoreCase("")) {
                holder.layoutShape.setBackgroundResource(R.drawable.add_wallet_layout_shape);
                holder.amountRsTxt.setText("+" + model.getAmount());
                holder.amountRsTxt.setTextColor(Color.parseColor("#08b3d3"));
                holder.mainHeadingTxt.setText("Added Voucher From " + model.getName());

                holder.chnageSenderImg.setImageResource(R.drawable.ic_bank);
            }else
                {
                    holder.layoutShape.setBackgroundResource(R.drawable.reciver_layout_shape);
                    holder.amountRsTxt.setText("+" + model.getAmount());
                    holder.amountRsTxt.setTextColor(Color.parseColor("#1fc812"));
                    holder.mainHeadingTxt.setText("XBO Received from " + model.getWalletAddress());
                    holder.chnageSenderImg.setImageResource(R.drawable.ic_recieve);
                }

        }else
        {
            if (model.getWalletAddress().equalsIgnoreCase("")) {
                holder.layoutShape.setBackgroundResource(R.drawable.add_wallet_layout_shape);
                holder.amountRsTxt.setText("+" + model.getAmount());
                holder.amountRsTxt.setTextColor(Color.parseColor("#08b3d3"));
                holder.mainHeadingTxt.setText("Added XBO To Your Wallet");

                holder.chnageSenderImg.setImageResource(R.drawable.ic_crypto);
            }else
                {
                    holder.layoutShape.setBackgroundResource(R.drawable.reciver_layout_shape);
                    holder.amountRsTxt.setText("+" + model.getAmount());
                    holder.amountRsTxt.setTextColor(Color.parseColor("#1fc812"));
                    holder.mainHeadingTxt.setText("XBO Received from " + model.getWalletAddress());
                    holder.chnageSenderImg.setImageResource(R.drawable.ic_recieve);
                }

        }


        holder.layoutShape.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(activity, TransactionPageDetailsActivity.class);
                    intent.putExtra("Amount",model.getAmount());
                    intent.putExtra("TransactionType",model.getTransactionType());
                    intent.putExtra("Name",model.getName());
                    intent.putExtra("RemainingBalance",model.getRemaningAmount());
                    intent.putExtra("DateTime",holder.dateTxt.getText().toString());
                    intent.putExtra("PaymentStatus",model.getPaymentStatus());
                    intent.putExtra("TransactionID",model.getTransactionId());
                    intent.putExtra("FundingType",model.getFundingType());
                    activity.startActivity(intent);
                    activity.overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
                }
            });

    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mainHeadingTxt, amountRsTxt, dateTxt,remaingRsTxt,statusTxt;
        public ImageView chnageSenderImg;
        public LinearLayout layoutShape;

        public ViewHolder(View itemView) {
            super(itemView);

            mainHeadingTxt = (TextView) itemView.findViewById(R.id.main_heading);
            amountRsTxt = (TextView) itemView.findViewById(R.id.xbo_amount);
            dateTxt = (TextView) itemView.findViewById(R.id.date);
            remaingRsTxt = (TextView) itemView.findViewById(R.id.remaning_rs);
            chnageSenderImg = (ImageView) itemView.findViewById(R.id.sender_img);
            layoutShape = (LinearLayout) itemView.findViewById(R.id.layoutshape);
            statusTxt = (TextView) itemView.findViewById(R.id.status_txt);
        }
    }

    //convert date time to month
    public String convertDateForTransactionHistory(String dtStart)
    {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date date = format.parse(dtStart);
            System.out.println(date);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy' | 'hh:mm");

            String date1 = sdf.format(date);

            return date1;


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }
}
