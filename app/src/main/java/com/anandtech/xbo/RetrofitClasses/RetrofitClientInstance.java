package com.anandtech.xbo.RetrofitClasses;

import com.anandtech.xbo.Source.SettingConstant;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance
{
    private static Retrofit retrofit;

    public static Retrofit getRetrofitInstances()
    {
        if (retrofit == null)
        {

            retrofit = new Retrofit.Builder().
                    baseUrl(SettingConstant.BASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
