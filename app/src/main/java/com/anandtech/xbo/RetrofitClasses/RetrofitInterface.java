package com.anandtech.xbo.RetrofitClasses;

import com.anandtech.xbo.Model.AppVersionModel;
import com.anandtech.xbo.Model.CreateSubAgentModel;
import com.anandtech.xbo.Model.MyStatusDataModel;
import com.anandtech.xbo.Model.PasscodeModel;
import com.anandtech.xbo.Model.ProfileModel;
import com.anandtech.xbo.Model.TransactionListModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RetrofitInterface
{

    @FormUrlEncoded
    @POST("api/WalletApi/WalletTransactionListByUserID")
    Call<TransactionListModel> getTransactionHistory(@Field("usrId") String userId, @Field("LoginAuthKey") String authKey);

    @GET("api/UserWebApi/CheckAppVersion")
    Call<AppVersionModel> getAppVersion();

    @FormUrlEncoded
    @POST("api/UserWebApi/SetPassCode")
    Call<PasscodeModel> setPassCodeApi(@Field("UserId") String userId, @Field("PassCode") String passCode);

    @FormUrlEncoded
    @POST("api/UserWebApi/ForgotPasscode")
    Call<PasscodeModel> forgotPassCode(@Field("EmailId") String emailId, @Field("UserId") String userId);

    @FormUrlEncoded
    @POST("api/JoinApi/ChangeUserRolebyAgent")
    Call<CreateSubAgentModel> createSubAgent(@Field("UserId") String userId, @Field("JoinId") String joinId);

    @FormUrlEncoded
    @POST("api/JoinApi/JoiningList")
    Call<MyStatusDataModel> getStatusList(@Field("ReferenceId") String userId, @Field("LoginAuthKey") String authKey);

    @FormUrlEncoded
    @POST("api/UserWebApi/UserDetailsByuserID")
    Call<ProfileModel> getProfileData(@Field("UserId") String UserId, @Field("LoginAuthKey") String authKey);
}
