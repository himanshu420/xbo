package com.anandtech.xbo.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.MyStackDeatilsActivity;
import com.anandtech.xbo.Activity.ProfileActivity;
import com.anandtech.xbo.Adapter.MyStackListAdapter;
import com.anandtech.xbo.Model.MyStackListModel;
import com.anandtech.xbo.Model.MyStatusDataModel;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.anandtech.xbo.ViewModels.MyStatusListViewModel;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyStackFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class MyStackFragment extends Fragment {

    static  class ViewLifeCycleOwner implements LifecycleOwner
    {
        private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);

        @Override
        public LifecycleRegistry getLifecycle() {
            return lifecycleRegistry;
        }
    }

    @Nullable
    private ViewLifeCycleOwner viewLifeCycleOwner;

    public MyStackListAdapter adapter;
    public ArrayList<MyStackListModel> list = new ArrayList<>();
    public RecyclerView stackListRecy;
    public ConnectionDetector conn;
    public String userId="",authKey = "",userTypeId = "";
    public FrameLayout noDataTxt;
    public LinearLayout colorLay;
    public MyStatusListViewModel myStatusListViewModel;
    public boolean flag = true;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewLifeCycleOwner = new ViewLifeCycleOwner();
        viewLifeCycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_CREATE);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (viewLifeCycleOwner != null)
        {
            viewLifeCycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_START);
        }
    }

    public MyStackFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Checking forced crash for crashlytics firebase report
        Fabric.with(getActivity(), new Crashlytics());

        conn = new ConnectionDetector(getActivity());
        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(getActivity())));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(getActivity())));
        userTypeId =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserTypeID(getActivity())));


        myStatusListViewModel = ViewModelProviders.of(getActivity()).get(MyStatusListViewModel.class);
        myStatusListViewModel.init(userId,authKey,getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_my_stack, container, false);



        noDataTxt = (FrameLayout) rootView.findViewById(R.id.no_data);
        colorLay = (LinearLayout) rootView.findViewById(R.id.color_lay);

        stackListRecy = (RecyclerView) rootView.findViewById(R.id.states_List);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        stackListRecy.setLayoutManager(mLayoutManager);
        stackListRecy.setItemAnimator(new DefaultItemAnimator());


        if (userTypeId.equalsIgnoreCase("1"))
        {
            colorLay.setVisibility(View.GONE);
        }else
            {
                colorLay.setVisibility(View.VISIBLE);
            }


        return rootView;
    }

    public void init()
    {
        noDataTxt.setVisibility(View.GONE);
        stackListRecy.setVisibility(View.VISIBLE);

        adapter = new MyStackListAdapter(getActivity(), myStatusListViewModel.getStatusList().getValue().getMyStatusList());
        stackListRecy.setAdapter(adapter);
    }

   /* *//*API WORK*//*
    public void getProfileDetails(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, myStackListUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("My Status", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (list.size()>0)
                    {
                        list.clear();
                    }

                    if (Status.equalsIgnoreCase("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i = 0; i<jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String name = object.getString("Name");
                            String emailid = object.getString("EmailId");
                            String phonenumber = object.getString("PhoneNumber");
                            String amount = object.getString("amount");
                            String CreatedDateTime = object.getString("CreatedDateTime");
                            String WalletAddress = object.getString("WalletAddress");
                            String ProfilePicture = object.getString("ProfilePicture");
                            String ReferenceId = object.getString("ReferenceId");
                            String TotalXboToken = object.getString("TotalXboToken");
                            String joinId = object.getString("joinId");
                            String CountryName = object.getString("CountryName");
                            String StateName = object.getString("StateName");
                            String CityName = object.getString("CityName");
                            String RefrenceUserName = object.getString("RefrenceUserName");
                            String AgentReferenceId = object.getString("AgentReferenceId");


                            list.add(new MyStackListModel(joinId,name,emailid,phonenumber,amount,ProfilePicture,
                                    ReferenceId,CountryName,StateName,CityName,TotalXboToken,WalletAddress,CreatedDateTime,
                                    RefrenceUserName,AgentReferenceId));

                        }

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            conn.showLogoutDialog(message, getActivity());
                        }
                    }else
                    {
                        stackListRecy.setVisibility(View.GONE);
                        noDataTxt.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }

                    adapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReferenceId", userId);
                params.put("LoginAuthKey",authKey);
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Profile");
    }*/

    @Override
    public void onResume() {
        super.onResume();

        if (viewLifeCycleOwner != null) {
            viewLifeCycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_RESUME);


            if (!flag) {
                Log.e("Checking resume", "Resume");
                myStatusListViewModel.init(userId, authKey, getActivity());
            }
            flag = false;
        }



    }

    @Override
    public void onPause() {
        if (viewLifeCycleOwner != null) {
            viewLifeCycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_PAUSE);
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        if (viewLifeCycleOwner != null) {
            viewLifeCycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_STOP);
        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        if (viewLifeCycleOwner != null) {
            viewLifeCycleOwner.getLifecycle().handleLifecycleEvent(Lifecycle.Event.ON_DESTROY);
            myStatusListViewModel.getStatusList().removeObservers(viewLifeCycleOwner);
            viewLifeCycleOwner = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //call the API
        myStatusListViewModel.getStatusList().observe(viewLifeCycleOwner, new Observer<MyStatusDataModel>() {
            @Override
            public void onChanged(MyStatusDataModel myStatusDataModel) {

                Log.e("Checking resume", "Observe.....");

                if (myStatusDataModel.getStatus().equalsIgnoreCase("1")) {
                    init();
                }else
                {
                    noDataTxt.setVisibility(View.VISIBLE);
                    stackListRecy.setVisibility(View.GONE);
                }
            }
        });
    }

    /* // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
*/
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
