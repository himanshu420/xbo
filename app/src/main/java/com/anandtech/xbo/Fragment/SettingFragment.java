package com.anandtech.xbo.Fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.transition.TransitionManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.anandtech.xbo.Activity.ChangePasswordActivity;
import com.anandtech.xbo.Activity.HomeActivity;
import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.MyStackDeatilsActivity;
import com.anandtech.xbo.Activity.PassCodeActivity;
import com.anandtech.xbo.Activity.RegisterActivity;
import com.anandtech.xbo.Activity.ResetPasscodeActivity;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static android.content.Context.FINGERPRINT_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public RelativeLayout changPassBtn,logoutBtn,securityBtn,passCodeBtn, fingerBtn;
    public LinearLayout subLay;
    public FrameLayout mainLay;
    public ConnectionDetector conn;
    public String userId = "", authkey = "",passCode = "";
    public String logutUrl = SettingConstant.BASEURL + "api/UserWebApi/Logout";
    public boolean flag = true;
    public View view;

    private OnFragmentInteractionListener mListener;

    public SettingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SettingFragment newInstance(String param1, String param2) {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Checking forced crash for crashlytics firebase report
        Fabric.with(getActivity(), new Crashlytics());

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_setting, container, false);

        conn = new ConnectionDetector(getActivity());
        userId =  UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(getActivity())));
        authkey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(getActivity())));


        changPassBtn = (RelativeLayout) rootView.findViewById(R.id.change_pass_btn);
        logoutBtn = (RelativeLayout) rootView.findViewById(R.id.logout_btn);
        securityBtn = (RelativeLayout) rootView.findViewById(R.id.security_btn);
        passCodeBtn = (RelativeLayout) rootView.findViewById(R.id.passcodeBtn);
        fingerBtn = (RelativeLayout) rootView.findViewById(R.id.fingerBtn);
        subLay = (LinearLayout) rootView.findViewById(R.id.sub_lay);
        mainLay = (FrameLayout) rootView.findViewById(R.id.main_lay);
        view = (View) rootView.findViewById(R.id.finger_view);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);

            // Check whether the device has a Fingerprint sensor.
            if (!fingerprintManager.isHardwareDetected())
            {
              fingerBtn.setVisibility(View.GONE);
              view.setVisibility(View.GONE);
            }else
                {
                    fingerBtn.setVisibility(View.VISIBLE);
                    view.setVisibility(View.VISIBLE);
                }
        }

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //logout button
                if (conn.getConnectivityStatus()>0) {

                    logoutUser(userId, authkey, currentDate());
                }else
                    {
                        conn.showNoInternetAlret();
                    }

            }
        });
        changPassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);

            }
        });

        securityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag) {
                    subLay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(mainLay);
                    flag = false;
                }else
                    {
                        subLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(mainLay);

                        flag = true;
                    }


            }
        });

        passCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passCode = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(getActivity())));

                if (passCode.equalsIgnoreCase("null") || passCode.equalsIgnoreCase("") ||
                        passCode == null)
                {
                    Intent intent = new Intent(getActivity(), ResetPasscodeActivity.class);
                    intent.putExtra("Nav","2");
                    startActivity(intent);

                }else {
                    Intent intent = new Intent(getActivity(), PassCodeActivity.class);
                    intent.putExtra("Nav", "5");
                    startActivity(intent);
                    getActivity().finish();
                }

            }
        });

        fingerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                passCode = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getPassCode(getActivity())));

                Log.e("Checking the pass", passCode + " Null---");
                if (passCode.equalsIgnoreCase("null") || passCode.equalsIgnoreCase(""))
                {
                    Intent intent = new Intent(getActivity(), ResetPasscodeActivity.class);
                    intent.putExtra("Nav","2");
                    startActivity(intent);

                }else
                    {
                        Intent intent = new Intent(getActivity(), PassCodeActivity.class);
                        intent.putExtra("Nav","2");
                        startActivity(intent);
                        getActivity().finish();
                    }
            }
        });
        return rootView;
    }

    public void logoutUser(final String userId ,final String authKey, final String date)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, logutUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("LoginUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setLoginStatus(getActivity(),
                                "0")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setVerified(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserId(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setRefrencId(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setEmailId(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setOldPassword(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserName(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAddress(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAmount(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setAuthKey(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setPassCode(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setFingerPrintEnable(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeName(getActivity(),
                                "")));
                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeID(getActivity(),
                                "")));

                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);

                    }else
                    {
                        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("LogOutDateTime", date);
                params.put("LoginAuthKey", authKey);

                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Login");

    }

    /*// TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }*/

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }
}
