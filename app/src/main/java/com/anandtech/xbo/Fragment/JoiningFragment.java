package com.anandtech.xbo.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.transition.TransitionManager;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.VerificationActivity;
import com.anandtech.xbo.Model.CitySpinnerModel;
import com.anandtech.xbo.Model.CounterySpinnerModerl;
import com.anandtech.xbo.Model.JoiningNameListModel;
import com.anandtech.xbo.Model.StateListModel;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AndroidBug5497Workaround;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import io.fabric.sdk.android.Fabric;

import static android.content.Context.CLIPBOARD_SERVICE;
import static android.graphics.Color.BLACK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link JoiningFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link JoiningFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JoiningFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public SearchableSpinner counterySpinner,stateSpinner,citySpinner;
    public ArrayAdapter<CounterySpinnerModerl> counteryAdapter ;
    public ArrayAdapter<StateListModel> stateAdapter;
    public ArrayAdapter<CitySpinnerModel> cityAdapter;
    public ArrayAdapter<JoiningNameListModel> typeAdapter;
    public ArrayList<CounterySpinnerModerl> counteryList = new ArrayList<>();
    public ArrayList<JoiningNameListModel> paymentTypeList = new ArrayList<>();
    public ArrayList<CitySpinnerModel> cityList = new ArrayList<>();
    public ArrayList<StateListModel> stateList = new ArrayList<>();
    public String counteryStr = "", stateStr = "";
    public LinearLayout cityLay;
    public ConstraintLayout constraintLayout;
    //public String typeUrl = SettingConstant.BASEURL + "PaymentTypeList";
    public String stateUrl = SettingConstant.BASEURL + "api/UserWebApi/StatesByCountryID";
    public String cityUrl = SettingConstant.BASEURL + "api/UserWebApi/CitiesByStateID";
    public String joiningUrl =SettingConstant.BASEURL + "api/JoinApi/MLMJoining";
    public LinearLayout dobBtn,cancelCheckForLay,bankDetailsLay;
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    public TextView dobTxt,walletaddressTxt;
    public RelativeLayout cancelCheckBtn,govtBtn;
    Uri image;
    private Bitmap bitmap;
    public ImageView firstIdImage,secondImage,firstCrossBtn,secondCrossBtn,cancelCheckThumbnail,cancekChckCrossBtn,walletAddressQrImg;
    public LinearLayout imageLay,cancelCheckLay,paymentLay;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public String firstBase64="" , secondBase64="",cancelCheckBase64 = "",checkingStrForClickDoc = "",
            paymentStatus="0",cityStr = "",userId = "",refrenceCodeStr = "",countryIdStr = "", stateIdStr = "",
            checkClickLay = "",authKey = "",
            cityIdStr = "",mCameraFileName = "",fileNameStr = "",radioBtnStr = "1";
    public EditText nameTxt, emailIdTxt, passwordTxt, confirmPassTxt, phoneNumberTxt, addressTxt,
            amountTxt,refrenceNoTxt,paymentTypeTxt,bankNameTxt,accountNumberTxt,bankIfscCodeTxt,
            bankBranchNameTxt,cryptoAmountTxt,cryptoTransactionIdTxt,senderAddresssTxt,autocalXBoTxt,autoCalInrTxt;
    public CheckBox checkBox;
    public ConnectionDetector conn;
    public Button subBtn,copyBtn,clickLinkBtn;
    public RelativeLayout image1Lay, image2Lay;
    public RadioGroup radioGroup;
    public RadioButton cancelChqBtn, bankDetailsBtn,laterBtn;
    public TextView bankbtn,cryptoBtn,cardBtn,dateOfTransaction;
    public LinearLayout bankLay,cryptoLay,cardLay,dateTimeBtn;
    public boolean flag = true,flag1 = true,flag2 = true;
    //public ImagePopup imagePopup;

    public JoiningFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment JoiningFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JoiningFragment newInstance(String param1, String param2) {
        JoiningFragment fragment = new JoiningFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //solved the full Mode open issue in home Button
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //Checking forced crash for crashlytics firebase report
        Fabric.with(getActivity(), new Crashlytics());

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_joining, container, false);

        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(getActivity())));

        counterySpinner = (SearchableSpinner) rootView.findViewById(R.id.countery_spinner);
        stateSpinner = (SearchableSpinner) rootView.findViewById(R.id.state_spinner);
        citySpinner = (SearchableSpinner) rootView.findViewById(R.id.city_spinner);
       // typeSpinner = (SearchableSpinner) rootView.findViewById(R.id.type_spinner);
        constraintLayout = (ConstraintLayout) rootView.findViewById(R.id.constraint);
        cityLay = (LinearLayout) rootView.findViewById(R.id.city_lay);
        dobBtn = (LinearLayout) rootView.findViewById(R.id.dob_btn);
        dobTxt = (TextView) rootView.findViewById(R.id.dob_txt);
        govtBtn = (RelativeLayout) rootView.findViewById(R.id.govt_btn);
        firstIdImage  = (ImageView) rootView.findViewById(R.id.first_id_image);
        secondImage = (ImageView) rootView.findViewById(R.id.second_id_image);
        firstCrossBtn = (ImageView) rootView.findViewById(R.id.first_cross_btn);
        secondCrossBtn = (ImageView) rootView.findViewById(R.id.second_cross_btn);
        imageLay = (LinearLayout) rootView.findViewById(R.id.image_layout);
        nameTxt = (EditText) rootView.findViewById(R.id.nameTxt);
        emailIdTxt = (EditText) rootView.findViewById(R.id.emailIdTxt);
        autocalXBoTxt = (EditText) rootView.findViewById(R.id.auto_cal_xbo);
        passwordTxt = (EditText) rootView.findViewById(R.id.password);
        confirmPassTxt = (EditText) rootView.findViewById(R.id.confirm_password);
        phoneNumberTxt = (EditText) rootView.findViewById(R.id.phone_number);
        addressTxt = (EditText) rootView.findViewById(R.id.adress);
        amountTxt = (EditText) rootView.findViewById(R.id.amount);
        refrenceNoTxt = (EditText) rootView.findViewById(R.id.refrence_no);
        paymentTypeTxt = (EditText) rootView.findViewById(R.id.payment_type);
        bankNameTxt = (EditText) rootView.findViewById(R.id.bank_nametxt);
        accountNumberTxt = (EditText) rootView.findViewById(R.id.bank_account_txt);
        bankIfscCodeTxt = (EditText) rootView.findViewById(R.id.bank_ifsc_code_txt);
        bankBranchNameTxt = (EditText) rootView.findViewById(R.id.bank_branch_name_txt);
        cryptoAmountTxt = (EditText) rootView.findViewById(R.id.amount_for_crypto);
        autoCalInrTxt = (EditText) rootView.findViewById(R.id.auto_cal_vou);
        cryptoTransactionIdTxt = (EditText) rootView.findViewById(R.id.crypto_refrence_no);
        senderAddresssTxt = (EditText) rootView.findViewById(R.id.sender_address);
        cancelCheckBtn = (RelativeLayout) rootView.findViewById(R.id.cancelCheck_btn);
        cancelCheckLay = (LinearLayout) rootView.findViewById(R.id.cancel_check_layout);
        cancelCheckForLay = (LinearLayout) rootView.findViewById(R.id.cancel_chque_lay);
        paymentLay = (LinearLayout) rootView.findViewById(R.id.payment_lay);
        bankDetailsLay =  (LinearLayout) rootView.findViewById(R.id.bank_details_lay);
        cancelCheckThumbnail = (ImageView) rootView.findViewById(R.id.cancel_check_id_image);
        image1Lay = (RelativeLayout) rootView.findViewById(R.id.image1lay);
        image2Lay = (RelativeLayout) rootView.findViewById(R.id.image2lay);
        cancekChckCrossBtn = (ImageView) rootView.findViewById(R.id.cancel_cross_btn);
        checkBox = (CheckBox) rootView.findViewById(R.id.check_bx);
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radio_group);
        cancelChqBtn = (RadioButton) rootView.findViewById(R.id.cancel_chque_btn);
        bankDetailsBtn = (RadioButton) rootView.findViewById(R.id.bank_details_btn);
        laterBtn = (RadioButton) rootView.findViewById(R.id.latter_btn);
        bankbtn = (TextView) rootView.findViewById(R.id.bank_btn);
        cryptoBtn = (TextView) rootView.findViewById(R.id.cryptoBtn);
        cardBtn = (TextView) rootView.findViewById(R.id.card_btn);
        bankLay = (LinearLayout) rootView.findViewById(R.id.bank_lay);
        cryptoLay = (LinearLayout) rootView.findViewById(R.id.crypto_lay);
        cardLay = (LinearLayout) rootView.findViewById(R.id.card_lay);
        subBtn = (Button) rootView.findViewById(R.id.sub_btn);
        copyBtn = (Button) rootView.findViewById(R.id.copybtn);
        walletaddressTxt = (TextView) rootView.findViewById(R.id.wallet_address_txt);
        walletAddressQrImg = (ImageView) rootView.findViewById(R.id.qr_for_crypto);
        clickLinkBtn = (Button) rootView.findViewById(R.id.clcikable_link);
        dateTimeBtn = (LinearLayout) rootView.findViewById(R.id.date_time_btn);
        dateOfTransaction = (TextView) rootView.findViewById(R.id.date_of_trans);

        bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
        bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
        bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

        cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
        cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
        cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

        cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
        cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
        cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

        cancelChqBtn.setChecked(true);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.cancel_chque_btn)
                {
                    radioBtnStr = "1";
                    cancelCheckForLay.setVisibility(View.VISIBLE);
                    bankDetailsLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else if (checkedId == R.id.bank_details_btn)
                {
                    radioBtnStr = "2";
                    cancelCheckForLay.setVisibility(View.GONE);
                    bankDetailsLay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else
                    {
                        radioBtnStr = "3";
                        cancelCheckForLay.setVisibility(View.GONE);
                        bankDetailsLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }
            }
        });

        bankbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag) {
                    bankbtn.setBackgroundResource(R.drawable.round_shape_layout);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_white_icon, 0, R.drawable.ic_uparrow_white, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.white));
                    bankLay.setVisibility(View.VISIBLE);


                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    if (!flag1)
                    {
                        cryptoLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                        flag1 = true;
                    }else if (!flag2)
                    {
                        cardLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        flag2 = true;
                    }

                    flag = false;
                    checkClickLay = "Bank";

                }else
                {
                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    flag = true;
                    checkClickLay = "";
                }
            }
        });

        cryptoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag1) {
                    cryptoBtn.setBackgroundResource(R.drawable.round_shape_layout);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_white_icon, 0, R.drawable.ic_uparrow_white, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.white));
                    cryptoLay.setVisibility(View.VISIBLE);
                    bankLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));


                    if (!flag)
                    {
                        bankLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                        flag = true;
                    }else if (!flag2)
                    {
                        cardLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        flag2 = true;
                    }


                    flag1 = false;
                    checkClickLay = "CryptoCurrency";

                }else
                {
                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    flag1 = true;
                    checkClickLay = "";

                }
            }
        });

        cardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag2) {
                    cardBtn.setBackgroundResource(R.drawable.round_shape_layout);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_white_icon, 0, R.drawable.ic_uparrow_white, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.white));
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);


                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));


                    if (!flag1)
                    {
                        cryptoLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                        flag1 = true;
                    }else if (!flag)
                    {
                        bankLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        flag = true;
                    }

                    flag2 = false;
                    checkClickLay = "Card";

                }else
                {
                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));
                    cardLay.setVisibility(View.GONE);
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);


                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    flag2 = true;
                    checkClickLay = "";

                }

            }
        });

        dateTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                // TODO Auto-generated method stub
                Calendar newCalendar = Calendar.getInstance();
                fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dateOfTransaction.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                        newCalendar.get(Calendar.DAY_OF_MONTH));
                Calendar cal = Calendar.getInstance();

                cal.add(Calendar.DAY_OF_MONTH, -5);
                Date result = cal.getTime();

                fromDatePickerDialog.getDatePicker().setMinDate(result.getTime());
                fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                fromDatePickerDialog.show();
            }
        });


        copyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //copy wallet address
                // place your TextView's text in clipboard
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(walletaddressTxt.getText().toString().trim());

                Toast.makeText(getActivity(), "Wallet Address Copied", Toast.LENGTH_SHORT).show();
            }
        });


        counteryList = SharedPrefs.getCountryList(getActivity());
        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(getActivity())));
        refrenceCodeStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getRefrenceId(getActivity())));
        conn = new ConnectionDetector(getActivity());

        //refrenceNoTxt.setText(refrenceCodeStr);

        //Countery List Spinner
        //stateSpinner.getBackground().setColorFilter(getResources().getColor(R.color.status_color), PorterDuff.Mode.SRC_ATOP);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            counteryAdapter = new ArrayAdapter<CounterySpinnerModerl>(getActivity(), R.layout.customizespinner, counteryList);
            counteryAdapter.setDropDownViewResource(R.layout.customizespinner);
            counterySpinner.setAdapter(counteryAdapter);

            //state List Spinner
            stateList.add(new StateListModel("0", "Please Select State"));
            stateAdapter = new ArrayAdapter<StateListModel>(getActivity(), R.layout.customizespinner, stateList);
            stateAdapter.setDropDownViewResource(R.layout.customizespinner);
            stateSpinner.setAdapter(stateAdapter);

            //City List Spinner
            cityList.add(new CitySpinnerModel("0", "Please Select City"));
            cityAdapter = new ArrayAdapter<CitySpinnerModel>(getActivity(), R.layout.customizespinner, cityList);
            cityAdapter.setDropDownViewResource(R.layout.customizespinner);
            citySpinner.setAdapter(cityAdapter);
        }else
            {
                counteryAdapter = new ArrayAdapter<CounterySpinnerModerl>(getActivity(), R.layout.support_simple_spinner_dropdown_item, counteryList);
                counteryAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                counterySpinner.setAdapter(counteryAdapter);

                //state List Spinner
                stateList.add(new StateListModel("0", "Please Select State"));
                stateAdapter = new ArrayAdapter<StateListModel>(getActivity(), R.layout.support_simple_spinner_dropdown_item, stateList);
                stateAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                stateSpinner.setAdapter(stateAdapter);

                //City List Spinner
                cityList.add(new CitySpinnerModel("0", "Please Select City"));
                cityAdapter = new ArrayAdapter<CitySpinnerModel>(getActivity(), R.layout.support_simple_spinner_dropdown_item, cityList);
                cityAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                citySpinner.setAdapter(cityAdapter);
            }

       /* //Payment Type Spinner
        paymentTypeList.add(new JoiningNameListModel("0","Please Select Payment Type"));
        typeAdapter = new ArrayAdapter<JoiningNameListModel>(getActivity(), R.layout.customizespinner, paymentTypeList);
        typeAdapter.setDropDownViewResource(R.layout.customizespinner);
        typeSpinner.setAdapter(typeAdapter);*/

        counterySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                stateAdapter.clear();
                stateAdapter.notifyDataSetChanged();
                countryIdStr = counteryList.get(position).getCounteryId();
                counteryStr = counteryList.get(position).getCounteryName();
                getStateList(counteryList.get(position).getCounteryId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cityAdapter.clear();
                cityAdapter.notifyDataSetChanged();
                stateIdStr = stateList.get(position).getStateId();
                stateStr = stateList.get(position).getStateName();
                getCityList(stateList.get(position).getStateId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                cityStr = cityList.get(position).getCityName();
                cityIdStr = cityList.get(position).getCityId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dobBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                // TODO Auto-generated method stub
                Calendar newCalendar = Calendar.getInstance();
                fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dobTxt.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                        newCalendar.get(Calendar.DAY_OF_MONTH));
                fromDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                fromDatePickerDialog.show();


            }
        });

        cancelCheckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkingStrForClickDoc = "1";

                if (!cancelCheckBase64.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), "You have already selected cancel cheque image", Toast.LENGTH_SHORT).show();
                }else {
                    selectImage();

                    //change the name of create file with the help of Variable Name.
                    fileNameStr = "CancelCheque";
                }
            }
        });



        cancelCheckThumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //imagePopup.viewPopup();

                cancelCheckBase64 = "";
                if (cancelCheckBase64.equalsIgnoreCase(""))
                {
                    cancelCheckLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else
                {
                    // firstIdImage.setVisibility(View.GONE);

                    cancelCheckThumbnail.setImageResource(android.R.color.transparent);
                }
            }
        });

        govtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkingStrForClickDoc = "2";

                if (!firstBase64.equalsIgnoreCase("") && !secondBase64.equalsIgnoreCase("")) {

                    Toast.makeText(getActivity(), "You have already selected id Proof", Toast.LENGTH_SHORT).show();
                }else
                {
                    selectImage();

                    //change the name of create file with the help of below condition.
                    if (firstBase64.equalsIgnoreCase(""))
                    {
                        fileNameStr = "FirstIdProof";
                    }else if (secondBase64.equalsIgnoreCase(""))
                    {
                        fileNameStr = "SecondIdProof";
                    }
                }
            }
        });

        firstIdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firstBase64 = "";
                if (firstBase64.equalsIgnoreCase("") && secondBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else if (!firstBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.VISIBLE);
                    image2Lay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else if (!secondBase64.equalsIgnoreCase(""))
                {

                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.GONE);
                    image2Lay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else
                {
                    // firstIdImage.setVisibility(View.GONE);

                    firstIdImage.setImageResource(android.R.color.transparent);
                }
            }
        });

        secondImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                secondBase64 = "";
                if (firstBase64.equalsIgnoreCase("") && secondBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else if (!firstBase64.equalsIgnoreCase(""))
                {
                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.VISIBLE);
                    image2Lay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else if (!secondBase64.equalsIgnoreCase(""))
                {

                    imageLay.setVisibility(View.VISIBLE);
                    image1Lay.setVisibility(View.GONE);
                    image2Lay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                }else
                {
                    //secondImage.setVisibility(View.GONE);
                    secondImage.setImageResource(android.R.color.transparent);
                }
            }
        });

        clickLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://remitano.com/btcusdt/in"));
                startActivity(browserIntent);
            }
        });

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    paymentLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    paymentStatus = "1";

                    paymentTypeTxt.setText("");
                    amountTxt.setText("");

                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.GONE);

                    flag = true;
                    flag1 = true;
                    flag2 = true;
                    checkClickLay = "";

                }
                else {
                    paymentLay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                    paymentStatus = "0";
                }
            }
        });

        amountTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    Double cal = Double.parseDouble(s.toString()) * 1;
                    autoCalInrTxt.setText(String.valueOf(cal) + " ("+"INR" +")");
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    autoCalInrTxt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cryptoAmountTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    float cal = Float.parseFloat(s.toString()) * 10;
                    autocalXBoTxt.setText(String.valueOf(cal) + " ("+"XBO" +")");
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    autocalXBoTxt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nameTxt.getText().toString().equalsIgnoreCase(""))
                {
                    nameTxt.setError("Please enter valid name");
                    nameTxt.requestFocus();
                }else if (!isValidEmail(emailIdTxt.getText().toString().trim()))
                {
                    emailIdTxt.setError("Please Enter Valid Email ID");
                    emailIdTxt.requestFocus();
                }else if (dobTxt.getText().toString().trim().equalsIgnoreCase("D.O.B *"))
                {
                    Toast.makeText(getActivity(), "Please enter date of birth", Toast.LENGTH_SHORT).show();
                    dobTxt.requestFocus();
                }else if (phoneNumberTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    phoneNumberTxt.setError("Please enter phone number");
                    phoneNumberTxt.requestFocus();
                }else if (addressTxt.getText().toString().trim().equalsIgnoreCase(""))
                {
                    addressTxt.setError("Please enter address");
                    addressTxt.requestFocus();
                }else if (counteryStr.equalsIgnoreCase("Please Select Country"))
                {
                    Toast.makeText(getActivity(), "Please select Country.", Toast.LENGTH_SHORT).show();
                }else if (stateStr.equalsIgnoreCase("Please Select State"))
                {
                    Toast.makeText(getActivity(), "Please select state", Toast.LENGTH_SHORT).show();
                }else
                {
                    if (radioBtnStr.equalsIgnoreCase("1")) {
                        if (paymentStatus.equalsIgnoreCase("0"))
                        {
                            if (checkClickLay.equalsIgnoreCase("Bank")) {
                                if (cancelCheckBase64.equalsIgnoreCase(""))
                                {
                                    Toast.makeText(getActivity(), "Please select cancel cheque image", Toast.LENGTH_SHORT).show();
                                }else if (amountTxt.getText().toString().trim().equalsIgnoreCase("")) {
                                    amountTxt.setError("Please enter amount");
                                    amountTxt.requestFocus();
                                } else if (Double.parseDouble(amountTxt.getText().toString())<100)
                                {
                                   amountTxt.setError("Please enter more than 100 voucher");
                                   amountTxt.requestFocus();

                                }else if (paymentTypeTxt.getText().toString().equalsIgnoreCase("")) {
                                    paymentTypeTxt.setError("Please enter payment type");
                                    paymentTypeTxt.requestFocus();
                                } else if (refrenceNoTxt.getText().toString().trim().equalsIgnoreCase("")) {
                                    refrenceNoTxt.setError("Please enter transaction reference number");
                                    refrenceNoTxt.requestFocus();
                                } else if (checkClickLay.equalsIgnoreCase("")) {
                                    Toast.makeText(getActivity(), "Please select funding type", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (conn.getConnectivityStatus() > 0) {
                                        setUserJoining(emailIdTxt.getText().toString().trim(),
                                                addressTxt.getText().toString().trim(),
                                                dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                                , firstBase64, secondBase64, cancelCheckBase64, nameTxt.getText().toString().trim(),
                                                amountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                                refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                                refrenceNoTxt.getText().toString().trim(), "", "", "",
                                                "", "", currentDate(),authKey,
                                                dateOfTransaction.getText().toString() +" 00:00:00");

                                    } else {
                                        conn.showNoInternetAlret();
                                    }
                                }
                            }else if (checkClickLay.equalsIgnoreCase("CryptoCurrency"))
                            {

                                if (cancelCheckBase64.equalsIgnoreCase(""))
                                {
                                    Toast.makeText(getActivity(), "Please select cancel cheque image", Toast.LENGTH_SHORT).show();
                                }else if (cryptoAmountTxt.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    cryptoAmountTxt.setError("Please enter amount");
                                    cryptoAmountTxt.requestFocus();
                                }else if (cryptoTransactionIdTxt.getText().toString().trim().equalsIgnoreCase(""))
                                {
                                    cryptoTransactionIdTxt.setError("Please enter transaction ID");
                                    cryptoTransactionIdTxt.requestFocus();
                                }else
                                    {
                                        if (conn.getConnectivityStatus() > 0) {
                                            setUserJoining(emailIdTxt.getText().toString().trim(),
                                                    addressTxt.getText().toString().trim(),
                                                    dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(),
                                                    userId,firstBase64, secondBase64, cancelCheckBase64,
                                                    nameTxt.getText().toString().trim(),cryptoAmountTxt.getText().toString(),
                                                    "", refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr,
                                                    checkClickLay, cryptoTransactionIdTxt.getText().toString().trim(),
                                                    "", "", "", "",
                                                    senderAddresssTxt.getText().toString().trim(),currentDate(),authKey,"");

                                        } else {
                                            conn.showNoInternetAlret();
                                        }
                                    }

                            }else
                                {
                                    if (cancelCheckBase64.equalsIgnoreCase("")) {
                                        Toast.makeText(getActivity(), "Please select cancel cheque image", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getActivity(), "Please select payment method", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        }else
                        {

                            if (cancelCheckBase64.equalsIgnoreCase(""))
                            {
                                Toast.makeText(getActivity(), "Please select cancel cheque image", Toast.LENGTH_SHORT).show();
                            }else {
                                if (conn.getConnectivityStatus() > 0) {

                                    setUserJoining(emailIdTxt.getText().toString().trim(),
                                            addressTxt.getText().toString().trim(),
                                            dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                            , firstBase64, secondBase64, cancelCheckBase64, nameTxt.getText().toString().trim(),
                                            amountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                            refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                            refrenceNoTxt.getText().toString().trim(), "", "", "",
                                            "","",currentDate(),authKey,"");


                                } else {
                                    conn.showNoInternetAlret();
                                }
                            }

                        }

                    }else if (radioBtnStr.equalsIgnoreCase("2"))
                    {
                        if (bankNameTxt.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            bankNameTxt.setError("Please enter bank name");
                            bankNameTxt.requestFocus();
                        }else if (accountNumberTxt.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            accountNumberTxt.setError("Please enter bank account number");
                            accountNumberTxt.requestFocus();
                        }else if (bankIfscCodeTxt.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            bankIfscCodeTxt.setError("Please enter bank IFSC code");
                            bankIfscCodeTxt.requestFocus();
                        }else if (bankBranchNameTxt.getText().toString().trim().equalsIgnoreCase(""))
                        {
                            bankBranchNameTxt.setError("Please enter bank branch name");
                            bankBranchNameTxt.requestFocus();
                        }else
                        {
                            if (paymentStatus.equalsIgnoreCase("0"))
                            {
                                if (checkClickLay.equalsIgnoreCase("Bank")) {
                                    if (amountTxt.getText().toString().trim().equalsIgnoreCase("")) {
                                        amountTxt.setError("Please enter amount");
                                        amountTxt.requestFocus();
                                    } else if (Double.parseDouble(amountTxt.getText().toString())<100)
                                    {
                                        amountTxt.setError("Please enter more than 100 voucher");
                                        amountTxt.requestFocus();

                                    }else if (paymentTypeTxt.getText().toString().equalsIgnoreCase("")) {
                                        paymentTypeTxt.setError("Please enter payment type");
                                        paymentTypeTxt.requestFocus();
                                    } else if (refrenceNoTxt.getText().toString().trim().equalsIgnoreCase("")) {
                                        refrenceNoTxt.setError("Please enter transaction reference number");
                                        refrenceNoTxt.requestFocus();
                                    } else if (checkClickLay.equalsIgnoreCase("")) {
                                        Toast.makeText(getActivity(), "Please select funding type", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (conn.getConnectivityStatus() > 0) {
                                            setUserJoining(emailIdTxt.getText().toString().trim(),
                                                    addressTxt.getText().toString().trim(),
                                                    dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                                    , firstBase64, secondBase64, "", nameTxt.getText().toString().trim(),
                                                    amountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                                    refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                                    refrenceNoTxt.getText().toString().trim(), bankNameTxt.getText().toString().trim(),
                                                    accountNumberTxt.getText().toString().trim(), bankIfscCodeTxt.getText().toString().trim(),
                                                    bankBranchNameTxt.getText().toString().trim(), "",
                                                    currentDate(),authKey,dateOfTransaction.getText().toString() +" 00:00:00");

                                        } else {
                                            conn.showNoInternetAlret();
                                        }
                                    }
                                }else if (checkClickLay.equalsIgnoreCase("CryptoCurrency"))
                                {

                                    if (cryptoAmountTxt.getText().toString().trim().equalsIgnoreCase(""))
                                    {
                                        cryptoAmountTxt.setError("Please enter amount");
                                        cryptoAmountTxt.requestFocus();
                                    }else if (cryptoTransactionIdTxt.getText().toString().trim().equalsIgnoreCase(""))
                                    {
                                        cryptoTransactionIdTxt.setError("Please enter transaction ID");
                                        cryptoTransactionIdTxt.requestFocus();
                                    }else
                                    {
                                        if (conn.getConnectivityStatus() > 0) {
                                            setUserJoining(emailIdTxt.getText().toString().trim(),
                                                    addressTxt.getText().toString().trim(),
                                                    dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                                    , firstBase64, secondBase64, "", nameTxt.getText().toString().trim(),
                                                    cryptoAmountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                                    refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                                    cryptoTransactionIdTxt.getText().toString().trim(), bankNameTxt.getText().toString().trim(),
                                                    accountNumberTxt.getText().toString().trim(), bankIfscCodeTxt.getText().toString().trim(),
                                                    bankBranchNameTxt.getText().toString().trim(), senderAddresssTxt.getText().toString().trim(),
                                                    currentDate(),authKey,"");

                                        } else {
                                            conn.showNoInternetAlret();
                                        }
                                    }

                                }else
                                {
                                    Toast.makeText(getActivity(), "Please select payment method", Toast.LENGTH_SHORT).show();
                                }
                            }else
                            {

                                if (conn.getConnectivityStatus()>0)
                                {

                                    setUserJoining(emailIdTxt.getText().toString().trim(),
                                            addressTxt.getText().toString().trim(),
                                            dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(),userId
                                            , firstBase64, secondBase64, "",nameTxt.getText().toString().trim(),
                                            amountTxt.getText().toString(),paymentTypeTxt.getText().toString().trim(),
                                            refrenceCodeStr,countryIdStr,stateIdStr,cityIdStr,checkClickLay,
                                            refrenceNoTxt.getText().toString().trim(),bankNameTxt.getText().toString().trim(),
                                            accountNumberTxt.getText().toString().trim(),bankIfscCodeTxt.getText().toString().trim(),
                                            bankBranchNameTxt.getText().toString().trim(),"",currentDate(),authKey,"");


                                }else
                                {
                                    conn.showNoInternetAlret();
                                }

                            }
                        }
                    }else
                        {
                            if (paymentStatus.equalsIgnoreCase("0"))
                            {
                                if (checkClickLay.equalsIgnoreCase("Bank")) {
                                    if (amountTxt.getText().toString().trim().equalsIgnoreCase("")) {
                                        amountTxt.setError("Please enter amount");
                                        amountTxt.requestFocus();
                                    } else if (Double.parseDouble(amountTxt.getText().toString())<100)
                                    {
                                        amountTxt.setError("Please enter more than 100 voucher");
                                        amountTxt.requestFocus();

                                    }else if (paymentTypeTxt.getText().toString().equalsIgnoreCase("")) {
                                        paymentTypeTxt.setError("Please enter payment type");
                                        paymentTypeTxt.requestFocus();
                                    } else if (refrenceNoTxt.getText().toString().trim().equalsIgnoreCase("")) {
                                        refrenceNoTxt.setError("Please enter transaction reference number");
                                        refrenceNoTxt.requestFocus();
                                    } else if (checkClickLay.equalsIgnoreCase("")) {
                                        Toast.makeText(getActivity(), "Please select funding type", Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (conn.getConnectivityStatus() > 0) {
                                            setUserJoining(emailIdTxt.getText().toString().trim(),
                                                    addressTxt.getText().toString().trim(),
                                                    dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                                    , firstBase64, secondBase64, "", nameTxt.getText().toString().trim(),
                                                    amountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                                    refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                                    refrenceNoTxt.getText().toString().trim(), "", "", "",
                                                    "", "", currentDate(),authKey,
                                                    dateOfTransaction.getText().toString() +" 00:00:00");

                                        } else {
                                            conn.showNoInternetAlret();
                                        }
                                    }
                                }else if (checkClickLay.equalsIgnoreCase("CryptoCurrency"))
                                {

                                    if (cryptoAmountTxt.getText().toString().trim().equalsIgnoreCase(""))
                                    {
                                        cryptoAmountTxt.setError("Please enter amount");
                                        cryptoAmountTxt.requestFocus();
                                    }else if (cryptoTransactionIdTxt.getText().toString().trim().equalsIgnoreCase(""))
                                    {
                                        cryptoTransactionIdTxt.setError("Please enter transaction ID");
                                        cryptoTransactionIdTxt.requestFocus();
                                    }else
                                    {
                                        if (conn.getConnectivityStatus() > 0) {
                                            setUserJoining(emailIdTxt.getText().toString().trim(),
                                                    addressTxt.getText().toString().trim(),
                                                    dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                                    , firstBase64, secondBase64, "", nameTxt.getText().toString().trim(),
                                                    cryptoAmountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                                    refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                                    cryptoTransactionIdTxt.getText().toString().trim(), "", "", "",
                                                    "", senderAddresssTxt.getText().toString().trim(),
                                                    currentDate(),authKey,"");

                                        } else {
                                            conn.showNoInternetAlret();
                                        }
                                    }

                                }else
                                {
                                    Toast.makeText(getActivity(), "Please select payment method", Toast.LENGTH_SHORT).show();
                                }
                            }else
                            {
                                if (conn.getConnectivityStatus() > 0) {

                                    setUserJoining(emailIdTxt.getText().toString().trim(),
                                                addressTxt.getText().toString().trim(),
                                                dobTxt.getText().toString().trim(), phoneNumberTxt.getText().toString().trim(), userId
                                                , firstBase64, secondBase64, "", nameTxt.getText().toString().trim(),
                                                amountTxt.getText().toString(), paymentTypeTxt.getText().toString().trim(),
                                                refrenceCodeStr, countryIdStr, stateIdStr, cityIdStr, checkClickLay,
                                                refrenceNoTxt.getText().toString().trim(), "", "", "",
                                                "","",currentDate(),authKey,"");


                                    } else {
                                        conn.showNoInternetAlret();
                                    }
                                }


                        }


                }

            }
        });

        //show address qr code
        convertQrCode();

        return rootView;
    }

    public void convertQrCode()
    {
        String qrCodeStr = walletaddressTxt.getText().toString().trim();
        // MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
           /* BitMatrix bitMatrix = multiFormatWriter.encode(qrCodeStr, BarcodeFormat.QR_CODE,400,400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);*/
            walletAddressQrImg.setImageBitmap(encodeAsBitmap(qrCodeStr,BarcodeFormat.QR_CODE,400,400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        // Map<EncodeHintType, Object>  = null;
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.MARGIN, new Integer(1));
      /*  String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            //hints.put(EncodeHintType.CHARACTER_SET, encoding);
            hints.put(EncodeHintType.MARGIN, 0); *//* default = 4 *//*
        }*/
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : Color.TRANSPARENT;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    /*API WORK*/
   /* public void getPaymentTypeData()
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, typeUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Pyament Type", response);

                    if (paymentTypeList.size()>0)
                    {
                        paymentTypeList.clear();
                    }
                    paymentTypeList.add(new JoiningNameListModel("Please Select Payment Type","0"));

                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String TypeId = object.getString("TypeId");
                        String TypeName = object.getString("TypeName");

                        paymentTypeList.add(new JoiningNameListModel(TypeName,TypeId));
                    }

                    typeAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        });
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");


    }*/

    public void getStateList(final String counteryId)
    {
        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, stateUrl + "?CountryId=" + counteryId , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("State Name", response);
                    Log.e("State Name", counteryId);

                    if (stateList.size()>0)
                    {
                        stateList.clear();
                    }
                    stateList.add(new StateListModel("0","Please Select State"));
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String name = object.getString("name");

                        stateList.add(new StateListModel(id,name));
                    }

                    stateAdapter.notifyDataSetChanged();
                    //  pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                // pDialog.dismiss();


            }
        });
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }


    public void getCityList(final String stateId)
    {
        StringRequest historyInquiry = new StringRequest(
                Request.Method.GET, cityUrl + "?stateId=" + stateId , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("City Name", response);

                    if (cityList.size()>0)
                    {
                        cityList.clear();
                    }
                    cityList.add(new CitySpinnerModel("0","Please Select City"));
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length(); i++)
                    {
                        JSONObject object = jsonArray.getJSONObject(i);
                        String id = object.getString("id");
                        String name = object.getString("name");

                        cityList.add(new CitySpinnerModel(id,name));
                    }

                    Log.e("checking the city list" , cityList.size() + "");

                    if (cityList.size() == 1)
                    {
                        cityLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }else
                    {
                        cityLay.setVisibility(View.VISIBLE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }

                    cityAdapter.notifyDataSetChanged();
                    //pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                //pDialog.dismiss();


            }
        });
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }

    public void setUserJoining(final String emailId, final String address, final String dob, final String phoneNo,
                                final String userId, final String idproof1, final String idproof2,
                                final String cancelCheck, final String name,final String amount,
                                final String paymentType, final String refrenceCode, final String counteryId,
                               final String stateId, final String cityId, final String fundingType,
                               final String trnsactionRefNo, final String bankName, final String accountNo,final String ifscCode,
                               final String bankBranchName,final String senderCryptoAdd, final String date,
                               final String authKey, final String transactionDate)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, joiningUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("registerUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_layout, new DashboardFragment())
                                .addToBackStack(null)
                                .commit();

                        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            conn.showLogoutDialog(Message, getActivity());
                        }
                    }else if (Status.equalsIgnoreCase("5"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            showCustomDialog(Message);
                        }
                    }else
                    {
                        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                    }

                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Address", address);
                params.put("CancelCheque", cancelCheck);
                params.put("Dob", dob);
                params.put("EmailId", emailId);
                params.put("Idproof1", idproof1);
                params.put("Idproof2", idproof2);
                params.put("Name", name);
                params.put("PhoneNumber", phoneNo);
                params.put("amount", amount);
                params.put("paymentType", paymentType);
                params.put("referenceCode", refrenceCode);
                params.put("usrId", userId);
                params.put("CountryID", counteryId);
                params.put("StateID", stateId);
                params.put("CityID", cityId);
                params.put("fundingtype", fundingType);
                params.put("transactionreferenceno", trnsactionRefNo);
                params.put("BankName", bankName);
                params.put("AccountNo", accountNo);
                params.put("IFSCcode", ifscCode);
                params.put("BankBranchName", bankBranchName);
                params.put("SenderCryptoAddress", senderCryptoAdd);
                params.put("TransactionDateTime", date);
                params.put("LoginAuthKey",authKey);
                params.put("BankTransactionDate",transactionDate);

                Log.e("Prams Check", params.toString());

                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Joining Details");
    }


    private void selectImage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_CAMERA_REQUEST_CODE);
            }else {

                try {
                    PackageManager pm = getActivity().getPackageManager();
                    int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
                    if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Select Option");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (options[item].equals("Take Photo")) {
                                    dialog.dismiss();
                                    cameraIntent();
                                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                                } else if (options[item].equals("Choose From Gallery")) {
                                    dialog.dismiss();
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                                } else if (options[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    } else
                        Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        }else
            {
                try {
                    PackageManager pm = getActivity().getPackageManager();
                    int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getActivity().getPackageName());
                    if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle("Select Option");
                        builder.setItems(options, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int item) {
                                if (options[item].equals("Take Photo")) {
                                    dialog.dismiss();
                                    cameraIntent();
                                   /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                                } else if (options[item].equals("Choose From Gallery")) {
                                    dialog.dismiss();
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                                } else if (options[item].equals("Cancel")) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        builder.show();
                    } else
                        Toast.makeText(getActivity(), "Camera Permission error", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            try {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Select Option");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {
                                dialog.dismiss();
                                cameraIntent();
                              /*  Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                            } else if (options[item].equals("Choose From Gallery")) {
                                dialog.dismiss();
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                            } else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();

                } else {

                    Toast.makeText(getActivity(), "camera permission denied", Toast.LENGTH_LONG).show();

                }
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (checkingStrForClickDoc.equalsIgnoreCase("1"))
        {
            cancelCheckLay.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(constraintLayout);
        }else {
            imageLay.setVisibility(View.VISIBLE);
            TransitionManager.beginDelayedTransition(constraintLayout);
        }
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
               /* Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");*/

               String checkStatus = "";

                if (data != null) {
                    image = data.getData();
                    checkStatus = "close";
                }else
                    {
                        checkStatus = "Open";
                    }
                if (image == null && mCameraFileName != null) {
                    image = Uri.fromFile(new File(mCameraFileName));
                }
                File file = new File(mCameraFileName);
                if (!file.exists()) {
                    file.mkdir();
                }

                InputStream image_stream = getActivity().getContentResolver().openInputStream(image);
                bitmap= BitmapFactory.decodeStream(image_stream );
                bitmap = Bitmap.createScaledBitmap(bitmap, 640, 360, true);


                if (checkStatus.equalsIgnoreCase("Open")) {
                    if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                        cancelCheckBase64 = convert(bitmap);
                        cancelCheckThumbnail.setImageBitmap(bitmap);
                        image = null;

                    } else {

                        if (firstBase64.equalsIgnoreCase("")) {
                            firstBase64 = convert(bitmap);
                            firstIdImage.setImageBitmap(bitmap);
                            image = null;
                            imageLay.setVisibility(View.VISIBLE);
                            image1Lay.setVisibility(View.VISIBLE);

                        } else {
                            secondBase64 = convert(bitmap);
                            secondImage.setImageBitmap(bitmap);
                            image = null;
                            imageLay.setVisibility(View.VISIBLE);
                            image2Lay.setVisibility(View.VISIBLE);
                        }

                    }
                }else
                    {
                        if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                            cancelCheckLay.setVisibility(View.GONE);
                            TransitionManager.beginDelayedTransition(constraintLayout);
                        }else
                            {
                                if (!firstBase64.equalsIgnoreCase("") && !secondBase64.equalsIgnoreCase(""))
                                {
                                    imageLay.setVisibility(View.VISIBLE);
                                    image1Lay.setVisibility(View.VISIBLE);
                                    image2Lay.setVisibility(View.VISIBLE);
                                    TransitionManager.beginDelayedTransition(constraintLayout);

                                }else if (!firstBase64.equalsIgnoreCase(""))
                                {
                                    imageLay.setVisibility(View.VISIBLE);
                                    image1Lay.setVisibility(View.VISIBLE);
                                    image2Lay.setVisibility(View.GONE);
                                    TransitionManager.beginDelayedTransition(constraintLayout);
                                }else if (!secondBase64.equalsIgnoreCase(""))
                                {

                                    imageLay.setVisibility(View.VISIBLE);
                                    image1Lay.setVisibility(View.GONE);
                                    image2Lay.setVisibility(View.VISIBLE);
                                    TransitionManager.beginDelayedTransition(constraintLayout);

                                }else
                                {
                                    imageLay.setVisibility(View.GONE);
                                    TransitionManager.beginDelayedTransition(constraintLayout);
                                }

                            }
                    }

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY) {
            try {
                Uri selectedImage = data.getData();
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);

                //compressed image
               // int nh = (int) ( bitmap.getHeight() * (700.0 / bitmap.getWidth()) );
                Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 600, 500, true);

              /*  imgPath = getRealPathFromURI(selectedImage);
                destination = new File(imgPath.toString());*/

                if (checkingStrForClickDoc.equalsIgnoreCase("1"))
                {
                    cancelCheckBase64 = convert(scaled);
                    cancelCheckThumbnail.setImageBitmap(scaled);
                }else {

                    if (firstBase64.equalsIgnoreCase("")) {
                        firstBase64 = convert(scaled);
                        firstIdImage.setImageBitmap(scaled);
                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.VISIBLE);
                    } else {
                        secondBase64 = convert(scaled);
                        secondImage.setImageBitmap(scaled);
                        imageLay.setVisibility(View.VISIBLE);
                        image2Lay.setVisibility(View.VISIBLE);
                    }
                }

            } catch (Exception e) {

                if (checkingStrForClickDoc.equalsIgnoreCase("1")) {
                    cancelCheckLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);
                }else
                {
                    if (!firstBase64.equalsIgnoreCase("") && !secondBase64.equalsIgnoreCase(""))
                    {
                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.VISIBLE);
                        image2Lay.setVisibility(View.VISIBLE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                    }else if (!firstBase64.equalsIgnoreCase(""))
                    {
                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.VISIBLE);
                        image2Lay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }else if (!secondBase64.equalsIgnoreCase(""))
                    {

                        imageLay.setVisibility(View.VISIBLE);
                        image1Lay.setVisibility(View.GONE);
                        image2Lay.setVisibility(View.VISIBLE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                    }else
                    {
                        imageLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                    }

                }
                e.printStackTrace();
            }
        }
    }

   /* public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }*/


    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static String convert(Bitmap bitmap)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
    }

    /* validation method*/
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPassword(String s) {
        Pattern PASSWORD_PATTERN
                = Pattern.compile(
                "[a-zA-Z0-9\\!\\@\\#\\$]{8,24}");

        return !TextUtils.isEmpty(s) && PASSWORD_PATTERN.matcher(s).matches();
    }

    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

     /*   Date date = new Date();
        DateFormat df = new SimpleDateFormat("-mm-ss");*/

        String newPicFile = fileNameStr + ".jpg";
        String outPath = "/sdcard/" + newPicFile;
        File outFile = new File(outPath);

        mCameraFileName = outFile.toString();
        Uri outuri = Uri.fromFile(outFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outuri);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);
    }

    private void showCustomDialog(String message) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

            }
        });
    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
