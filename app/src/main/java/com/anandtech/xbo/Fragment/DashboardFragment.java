package com.anandtech.xbo.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import io.fabric.sdk.android.Fabric;
import io.supercharge.shimmerlayout.ShimmerLayout;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import android.os.CountDownTimer;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.Activity.HomeActivity;
import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.ProceedToPayActivity;
import com.anandtech.xbo.Activity.ProfileActivity;
import com.anandtech.xbo.Activity.SupportActivity;
import com.anandtech.xbo.Activity.TransactionHistoryActivity;
import com.anandtech.xbo.Activity.VerificationActivity;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import static android.Manifest.permission.CAMERA;
import static android.content.Context.CLIPBOARD_SERVICE;
import static android.graphics.Color.BLACK;
import static android.graphics.Color.RED;
import static com.google.android.gms.common.util.Strings.capitalize;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DashboardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DashboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment implements ZXingScannerView.ResultHandler  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    public LinearLayout joiningBtn,myStackBtn,profileBtn,settingBtn,transactionBtn,fundingTile,supportTile,achivmentTileCard,aboutCardBtn;
    public TextView sendBtn,reciveBtn,aboutTxt,joiningTxt,statusTxtBtn,achivmentTxt,profileTxt,fundingTxt,transactionTxt,
            supportTxt,autoCalTxt;
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    me.dm7.barcodescanner.zxing.ZXingScannerView zxScanner;
    public androidx.appcompat.app.AlertDialog diloge;
    //font Path
    public String fontPath = "Font/nokiafc.ttf";
    public String convertUrl = SettingConstant.BASEURL + "api/WalletApi/AddXbotoken";
    public String updateBalanceUrl = SettingConstant.BASEURL + "api/UserWebApi/UserTotalBalanceByuserID";
    public TextView balancetxt, amountTxt,accountTxt, typeTxt, statusTxt,scanTxt,qrTxt,volBlncTxt,ROIBalnceTxt;
    public Button convertBtn;
    public ImageView logoImg,qrCodeImg;
    public ShimmerLayout shimmerFrameLayout;
    public ConnectionDetector conn;
    public String userId = "", authKey = "",sendBoxStatus = "";
    public int i = 0;


    public DashboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mListener.onFragmentInteraction(0,"XBO ETT Wallet");

        //solved the full Mode open issue in home Button
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //Checking forced crash for crashlytics firebase report
        Fabric.with(getActivity(), new Crashlytics());

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_dashboard, container, false);

        conn = new ConnectionDetector(getActivity());
        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(getActivity())));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(getActivity())));

        // Loading Font Face
        Typeface m_typeFace = Typeface.createFromAsset(getActivity().getAssets(), fontPath);

        joiningBtn = (LinearLayout) rootview.findViewById(R.id.joining_btn);
        myStackBtn = (LinearLayout) rootview.findViewById(R.id.my_stack_btn);
        profileBtn = (LinearLayout) rootview.findViewById(R.id.profile_btn);
        aboutCardBtn = (LinearLayout) rootview.findViewById(R.id.about_btn);
        transactionBtn = (LinearLayout) rootview.findViewById(R.id.transaction_btn);
        fundingTile = (LinearLayout) rootview.findViewById(R.id.funding_tile);
        supportTile = (LinearLayout) rootview.findViewById(R.id.support_tile);
        achivmentTileCard = (LinearLayout) rootview.findViewById(R.id.achivementtile);
        balancetxt = (TextView) rootview.findViewById(R.id.balancetxt);
        amountTxt = (TextView) rootview.findViewById(R.id.amount_txt);
        typeTxt = (TextView) rootview.findViewById(R.id.type_txt);
        ROIBalnceTxt = (TextView) rootview.findViewById(R.id.ROI_blnc);
        statusTxt = (TextView) rootview.findViewById(R.id.status_online);
        //accountTxt = (TextView) rootview.findViewById(R.id.account_txt);
        scanTxt = (TextView) rootview.findViewById(R.id.scantxt);
        qrTxt = (TextView) rootview.findViewById(R.id.qr);
        aboutTxt = (TextView)rootview.findViewById(R.id.about_txt);
        joiningTxt = (TextView)rootview.findViewById(R.id.joining_txt);
        statusTxtBtn = (TextView)rootview.findViewById(R.id.status_txt);
        achivmentTxt = (TextView)rootview.findViewById(R.id.achivment_txt);
        profileTxt = (TextView)rootview.findViewById(R.id.profile_txt);
        fundingTxt = (TextView)rootview.findViewById(R.id.funding_txt);
        transactionTxt = (TextView)rootview.findViewById(R.id.transaction_txt);
        supportTxt = (TextView)rootview.findViewById(R.id.support_txt);
        qrCodeImg = (ImageView) rootview.findViewById(R.id.qr_code);
        logoImg = (ImageView) rootview.findViewById(R.id.logo);
        shimmerFrameLayout = (ShimmerLayout) rootview.findViewById(R.id.shimmer_view_container);
        reciveBtn = (TextView) rootview.findViewById(R.id.reciveBtn);
        sendBtn = (TextView) rootview.findViewById(R.id.sendBtn);
        volBlncTxt = (TextView) rootview.findViewById(R.id.vol_blnc);
        convertBtn = (Button) rootview.findViewById(R.id.convertbtn);

        //SHINING EFFACE
        shimmerFrameLayout.startShimmerAnimation();

        balancetxt.setTypeface(m_typeFace);
        amountTxt.setTypeface(m_typeFace);
        typeTxt.setTypeface(m_typeFace);
        ROIBalnceTxt.setTypeface(m_typeFace);
        statusTxt.setTypeface(m_typeFace);
       // accountTxt.setTypeface(m_typeFace);
        scanTxt.setTypeface(m_typeFace);
        qrTxt.setTypeface(m_typeFace);
        volBlncTxt.setTypeface(m_typeFace);
        convertBtn.setTypeface(m_typeFace);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sendBoxStatus.equalsIgnoreCase("0"))
                    conn.showSendBoxDialog("Sending restricted as per partnership program rules.", getActivity());
                else
                    ScanQrCode();

            }
        });

        reciveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showQrCodeDialog();
            }
        });

        achivmentTileCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(3,"MY ACHIEVEMENT");

                //Stacks Fragment
                loadFragment(new AchivementFragment());
            }
        });

        achivmentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(3,"MY ACHIEVEMENT");

                //Stacks Fragment
                loadFragment(new AchivementFragment());
            }
        });

        transactionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), TransactionHistoryActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        transactionTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(), TransactionHistoryActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        myStackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(2,"MY STATUS");

                //Stacks Fragment
                loadFragment(new MyStackFragment());
            }
        });

        statusTxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(2,"MY STATUS");

                //Stacks Fragment
                loadFragment(new MyStackFragment());
            }
        });
        joiningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(1,"JOINING FORM");

                //joining Fragment
                loadFragment(new JoiningFragment());
            }
        });

        joiningTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(1,"JOINING FORM");

                //joining Fragment
                loadFragment(new JoiningFragment());
            }
        });

        profileBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        profileTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        aboutCardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (getDeviceName().equalsIgnoreCase("Samsung"))
                {
                    String pdf = "http://52.16.118.90/Document/XBOETT.pdf";
                    String loadPdf = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(loadPdf));
                    startActivity(i);
                }
                else {

                    mListener.onFragmentInteraction(5,"About XBO");

                    AboutXBOFragment fragment = new AboutXBOFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Nav", "1");
                    fragment.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_layout, fragment)
                            .addToBackStack(null)
                            .commit();
                }
            }
        });

        aboutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (getDeviceName().equalsIgnoreCase("Samsung"))
                {
                    String pdf = "http://52.16.118.90/Document/XBOETT.pdf";
                    String loadPdf = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdf;
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(loadPdf));
                    startActivity(i);
                }
                else {

                    mListener.onFragmentInteraction(5,"About XBO");

                    AboutXBOFragment fragment = new AboutXBOFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Nav", "1");
                    fragment.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_layout, fragment)
                            .addToBackStack(null)
                            .commit();
                }

               /* //joining Fragment
                loadFragment(new AboutXBOFragment());*/
            }
        });

        fundingTile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(4,"Funding");

                //Funding Fragment
                loadFragment(new FundingFragment());
            }
        });

        fundingTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(4,"Funding");

                //Funding Fragment
                loadFragment(new FundingFragment());
            }
        });

        supportTile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SupportActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        supportTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getActivity(), SupportActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);
            }
        });

        convertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                convertVouToXBO();
            }
        });

        shimmerFrameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getDeviceName().equalsIgnoreCase("Samsung"))
                {
                    String url = "http://anandtechs.com/xbo/";
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);

                }
                else {

                    mListener.onFragmentInteraction(6,"Xbond.io");


                    AboutXBOFragment fragment = new AboutXBOFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Nav", "2");
                    fragment.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container_layout, fragment)
                            .addToBackStack(null)
                            .commit();
                }

            }
        });


        //QR Code Generator
        convertQrCode();

        if (conn.getConnectivityStatus()>0)
        {
            updateBalance(userId,authKey);
        }else
        {
            conn.showNoInternetAlret();
        }

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();

        hideKeyboard(getActivity());

        if (conn.getConnectivityStatus()>0)
        {
            updateBalance(userId,authKey);
        }else
        {
            conn.showNoInternetAlret();
        }

    }

    private Fragment loadFragment(Fragment fragment) {

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_layout, fragment)
                .addToBackStack(null)
                .commit();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void showQrCodeDialog() {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_qr_code_layout, null);

        final TextView walletAddress = (TextView)view.findViewById(R.id.wallet_address);
        ImageView qrCodeImg = (ImageView) view.findViewById(R.id.qr_code);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);
        Button copyBtn = (Button) view.findViewById(R.id.copyBtn);

        walletAddress.setText(UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getWalletAddress(getActivity()))));
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(walletAddress.getText().toString().trim(), BarcodeFormat.QR_CODE,400,400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qrCodeImg.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }


        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);

        copyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //copy wallet address
                // place your TextView's text in clipboard
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(walletAddress.getText().toString());

                Toast.makeText(getActivity(), "Wallet Address Copied", Toast.LENGTH_SHORT).show();
            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();
            }
        });
    }

    private void ScanQrCode() {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_scan_code_layout, null);

        zxScanner = (me.dm7.barcodescanner.zxing.ZXingScannerView)view.findViewById(R.id.zxing_barcode_scanner);
        final EditText walletAddressTxt = (EditText) view.findViewById(R.id.wallet_address);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);
        Button sendBtn = (Button) view.findViewById(R.id.sendBtn);

        zxScanner.setFormats(Collections.singletonList(BarcodeFormat.QR_CODE));
        zxScanner.setAutoFocus(true);
        zxScanner.setLaserColor(R.color.second_gradient);
        zxScanner.setMaskColor(R.color.gray_color);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_CAMERA_REQUEST_CODE);
            }else
                {
                    //new IntentIntegrator(getActivity()).initiateScan();

                    zxScanner.setResultHandler(this);
                    zxScanner.startCamera(camId);

                }

        }

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        diloge = builder.show();
        diloge.setCancelable(false);
        diloge.setCanceledOnTouchOutside(true);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (walletAddressTxt.getText().toString().equalsIgnoreCase(""))
                {
                   walletAddressTxt.setError("Please enter Wallet Address");
                   // walletAddressTxt.requestFocus();
                }else {

                    Intent intent = new Intent(getActivity(), ProceedToPayActivity.class);
                    intent.putExtra("WalletAddres", walletAddressTxt.getText().toString().trim());
                    getActivity().startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);

                    diloge.dismiss();
                }

            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                diloge.dismiss();

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_CAMERA_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted){

                        zxScanner.setResultHandler(this);
                        zxScanner.startCamera(camId);
                        Toast.makeText(getActivity(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getActivity(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            MY_CAMERA_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void handleResult(Result result) {

        final String result1 = result.getText();
        Log.e("QRCodeScanner", result.getText());
        Log.e("QRCodeScanner", result.getBarcodeFormat().toString());

        Intent intent = new Intent(getActivity(), ProceedToPayActivity.class);
        intent.putExtra("WalletAddres",result1.trim());
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);

        diloge.dismiss();


    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new androidx.appcompat.app.AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    public void convertQrCode()
    {
        String qrCodeStr = (UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getWalletAddress(getActivity()))));
       /* MultiFormatWriter multiFormatWriter = new MultiFormatWriter();*/
        try {
           /* BitMatrix bitMatrix = multiFormatWriter.encode(qrCodeStr, BarcodeFormat.QR_CODE,100,100);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);*/
            qrCodeImg.setImageBitmap(encodeAsBitmap(qrCodeStr,BarcodeFormat.QR_CODE,400,400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
       // Map<EncodeHintType, Object>  = null;
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.MARGIN, new Integer(1));
      /* String encoding = guessAppropriateEncoding(contentsToEncode);
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            //hints.put(EncodeHintType.CHARACTER_SET, encoding);
            hints.put(EncodeHintType.MARGIN, 0); *//* default = 4 *//*
        }*/
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : Color.TRANSPARENT;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }


    private void convertVouToXBO() {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custom_convert_dialog_layout, null);

        final EditText noOfVoucherTxt = (EditText) view.findViewById(R.id.number_voucher);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);
        Button convertBtn = (Button) view.findViewById(R.id.convert_btn);
        autoCalTxt = (TextView) view.findViewById(R.id.auto_calculate_txt);

        noOfVoucherTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {
                    float cal = Float.parseFloat(s.toString()) / 7;
                    autoCalTxt.setText("~"+String.valueOf(cal) + " XBO");
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    autoCalTxt.setText("");
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);

        convertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (noOfVoucherTxt.getText().toString().equalsIgnoreCase(""))
                {
                    noOfVoucherTxt.setError("Please enter voucher");

                }else {
                    if (conn.getConnectivityStatus() > 0) {
                        convertVoucherToXBOApi(userId,noOfVoucherTxt.getText().toString().trim(),authKey,ad);
                    } else {
                        conn.showNoInternetAlret();
                    }
                }

            }
        });

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();
                getActivity().getWindow().setSoftInputMode(
                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                );
            }
        });
    }

    public void convertVoucherToXBOApi(final String userId, final String voucherAmount, final String authKey,
                                       final androidx.appcompat.app.AlertDialog ad)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, convertUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Convert Voucher", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        //update balance
                        updateBalance(userId,authKey);

                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();

                        ad.dismiss();
                        getActivity().getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                        );
                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            conn.showLogoutDialog(Message, getActivity());
                        }
                    }else
                    {
                        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                    }

                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("voucheramount", voucherAmount);
                params.put("LoginAuthKey",authKey);

                Log.e("Prams Check", params.toString());

                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Convert Voucher");
    }

    //update balance API
    public void updateBalance(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, updateBalanceUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("UpdateBalance", response);

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String message = jsonObject.getString("Message");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        String UserTotalBalance = jsonObject.getString("UserTotalBalance");
                        String TotalVoucher = jsonObject.getString("TotalVoucher");
                        String UserTypeName = jsonObject.getString("UserTypeName");
                        String UserTypeId = jsonObject.getString("UserTypeId");
                        sendBoxStatus = jsonObject.getString("SendxboStatus");
                        String PlotROI = jsonObject.getString("PlotROI");

                        //ROI XBO
                        if (sendBoxStatus.equalsIgnoreCase("1"))
                            ROIBalnceTxt.setVisibility(View.VISIBLE);
                        else
                            ROIBalnceTxt.setVisibility(View.GONE);

                        amountTxt.setText("XB0: "+UserTotalBalance);
                        volBlncTxt.setText("Voucher: "+TotalVoucher);
                        typeTxt.setText("A/C TYPE: " + UserTypeName);
                        ROIBalnceTxt.setText("ROI: " + PlotROI);



                        UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeID(getActivity(),UserTypeId)));

                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            conn.showLogoutDialog(message, getActivity());
                        }
                    }else
                    {
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }


                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("UserId", userId);
                params.put("LoginAuthKey",authKey);

                Log.e("Checking Params", params.toString());
                return params;
            }
        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Send Token");
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(int index, String titleName);
    }

    //get device name
    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer);
        }
    }

}
