package com.anandtech.xbo.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.transition.TransitionManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anandtech.xbo.Activity.CryptoCurrencyPaymentGateway;
import com.anandtech.xbo.Activity.HomeActivity;
import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.ProfileActivity;
import com.anandtech.xbo.Activity.RegisterActivity;
import com.anandtech.xbo.Model.JoiningNameListModel;
import com.anandtech.xbo.R;
import com.anandtech.xbo.Source.AndroidBug5497Workaround;
import com.anandtech.xbo.Source.AppController;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static android.content.Context.CLIPBOARD_SERVICE;
import static android.graphics.Color.BLACK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FundingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FundingFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FundingFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    public SearchableSpinner joiningListSpiner,joinerSpinnerForCrypto;
    public ArrayAdapter<JoiningNameListModel> JoiningAdapter;
    public ArrayList<JoiningNameListModel> joiningList = new ArrayList<>();
    private SimpleDateFormat dateFormatter;
    public String joingUrl = SettingConstant.BASEURL + "api/JoinApi/JoiningListdetails";
    public String fundingUrl = SettingConstant.BASEURL + "api/FundingApi/UpdateMakeFund";
    public ConnectionDetector conn;
    public String userId = "",refrenceCodeStr = "",authKey = "";
    public Button subBtn,copyBtn,clickLinkBtn;
    public EditText amountTxt, paymentTypeTxt, refrenceNumberTxt,cryptoAmountTxt,cryptoTransactionIdTxt,
            senderAddresssTxt,autoCalInrTxt,autocalXBoTxt;
    public LinearLayout bankLay,cryptoLay,cardLay,dateTimeBtn;
    public String joinIdStr = "",checkClickLay = "";
    private DatePickerDialog fromDatePickerDialog;
    public TextView bankbtn,cryptoBtn,cardBtn,walletaddressTxt,dateOfTransaction;
    public ImageView walletAddressQrImg;
    public ConstraintLayout constraintLayout;
    public boolean flag = true,flag1 = true,flag2 = true;

    private OnFragmentInteractionListener mListener;

    public FundingFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FundingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FundingFragment newInstance(String param1, String param2) {
        FundingFragment fragment = new FundingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //solved the full Mode open issue in home Button
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //Checking forced crash for crashlytics firebase report
        Fabric.with(getActivity(), new Crashlytics());

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_funding, container, false);

        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        userId  = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getUserId(getActivity())));
        refrenceCodeStr = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getRefrenceId(getActivity())));
        authKey = UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.getAuthKey(getActivity())));


        conn = new ConnectionDetector(getActivity());
        joiningListSpiner = (SearchableSpinner) rootView.findViewById(R.id.joinner_spiner);
        joinerSpinnerForCrypto = (SearchableSpinner) rootView.findViewById(R.id.joinner_spiner_for_crypto);
        subBtn = (Button) rootView.findViewById(R.id.sub_btn);
        refrenceNumberTxt = (EditText) rootView.findViewById(R.id.refrence_no);
        amountTxt = (EditText) rootView.findViewById(R.id.amount);
        autocalXBoTxt = (EditText) rootView.findViewById(R.id.auto_cal_xbo);
        paymentTypeTxt = (EditText) rootView.findViewById(R.id.payment_type);
        cryptoAmountTxt = (EditText) rootView.findViewById(R.id.amount_for_crypto);
        cryptoTransactionIdTxt = (EditText) rootView.findViewById(R.id.crypto_refrence_no);
        senderAddresssTxt = (EditText) rootView.findViewById(R.id.sender_address);
        autoCalInrTxt = (EditText) rootView.findViewById(R.id.auto_cal_vou);
        bankLay = (LinearLayout) rootView.findViewById(R.id.bank_lay);
        cryptoLay = (LinearLayout) rootView.findViewById(R.id.crypto_lay);
        cardLay = (LinearLayout) rootView.findViewById(R.id.card_lay);
        dateTimeBtn = (LinearLayout) rootView.findViewById(R.id.date_time_btn);
        bankbtn = (TextView) rootView.findViewById(R.id.bank_btn);
        cryptoBtn = (TextView) rootView.findViewById(R.id.cryptoBtn);
        cardBtn = (TextView) rootView.findViewById(R.id.card_btn);
        walletaddressTxt = (TextView) rootView.findViewById(R.id.wallet_address_txt);
        dateOfTransaction = (TextView) rootView.findViewById(R.id.date_of_trans);
        walletAddressQrImg = (ImageView) rootView.findViewById(R.id.qr_for_crypto);
        constraintLayout = (ConstraintLayout) rootView.findViewById(R.id.constraint_lay);
        clickLinkBtn = (Button) rootView.findViewById(R.id.clcikable_link);
        copyBtn = (Button) rootView.findViewById(R.id.copybtn);

       /* senderAddresssTxt.setVisibility(View.GONE);
        cryptoTransactionIdTxt.setVisibility(View.GONE);*/


        bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
        bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
        bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

        cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
        cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
        cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

        cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
        cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
        cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

        clickLinkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://remitano.com/btcusdt/in"));
                startActivity(browserIntent);
            }
        });

        bankbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag) {
                    bankbtn.setBackgroundResource(R.drawable.round_shape_layout);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_white_icon, 0, R.drawable.ic_uparrow_white, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.white));
                    bankLay.setVisibility(View.VISIBLE);




                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    if (!flag1)
                    {
                        cryptoLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                        flag1 = true;
                    }else if (!flag2)
                    {
                        cardLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        flag2 = true;
                    }

                    flag = false;
                    checkClickLay = "Bank";

                }else
                    {
                        bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                        bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                        bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));
                        bankLay.setVisibility(View.GONE);
                        cryptoLay.setVisibility(View.GONE);
                        cardLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                        cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                        cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                        cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                        cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                        cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));


                        flag = true;
                        checkClickLay = "";
                    }


                cryptoAmountTxt.setText("");
                cryptoTransactionIdTxt.setText("");
                senderAddresssTxt.setText("");
                joinerSpinnerForCrypto.setSelection(0);
            }
        });

        cryptoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag1) {
                    cryptoBtn.setBackgroundResource(R.drawable.round_shape_layout);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_white_icon, 0, R.drawable.ic_uparrow_white, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.white));
                    cryptoLay.setVisibility(View.VISIBLE);
                    bankLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));


                    if (!flag)
                    {
                        bankLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                        flag = true;
                    }else if (!flag2)
                    {
                        cardLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        flag2 = true;
                    }


                    flag1 = false;
                    checkClickLay = "CryptoCurrency";

                }else
                {
                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);

                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    flag1 = true;
                    checkClickLay = "";

                }


                amountTxt.setText("");
                paymentTypeTxt.setText("");
                refrenceNumberTxt.setText("");
                joiningListSpiner.setSelection(0);
            }
        });

        cardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag2) {
                    cardBtn.setBackgroundResource(R.drawable.round_shape_layout);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_white_icon, 0, R.drawable.ic_uparrow_white, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.white));
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    cardLay.setVisibility(View.VISIBLE);
                    TransitionManager.beginDelayedTransition(constraintLayout);


                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));


                    if (!flag1)
                    {
                        cryptoLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);
                        flag1 = true;
                    }else if (!flag)
                    {
                        bankLay.setVisibility(View.GONE);
                        TransitionManager.beginDelayedTransition(constraintLayout);

                        flag = true;
                    }

                    flag2 = false;
                    checkClickLay = "Card";


                }else
                {
                    cardBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cardBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_creditcard_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cardBtn.setTextColor(getResources().getColor(R.color.blue_for_text));
                    cardLay.setVisibility(View.GONE);
                    bankLay.setVisibility(View.GONE);
                    cryptoLay.setVisibility(View.GONE);
                    TransitionManager.beginDelayedTransition(constraintLayout);


                    bankbtn.setBackgroundResource(R.drawable.login_edit_shape);
                    bankbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bank_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    bankbtn.setTextColor(getResources().getColor(R.color.blue_for_text));

                    cryptoBtn.setBackgroundResource(R.drawable.login_edit_shape);
                    cryptoBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bitcoin_blue_icon, 0, R.drawable.ic_downarrow_blue_icon, 0);
                    cryptoBtn.setTextColor(getResources().getColor(R.color.blue_for_text));


                    flag2 = true;
                    checkClickLay = "";

                }

            }
        });

        dateTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                // TODO Auto-generated method stub
                Calendar newCalendar = Calendar.getInstance();
                fromDatePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        dateOfTransaction.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH),
                        newCalendar.get(Calendar.DAY_OF_MONTH));
                Calendar cal = Calendar.getInstance();

                cal.add(Calendar.DAY_OF_MONTH, -5);
                Date result = cal.getTime();

                fromDatePickerDialog.getDatePicker().setMinDate(result.getTime());
                fromDatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                fromDatePickerDialog.show();
            }
        });

        amountTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                try {

                    Double cal = Double.parseDouble(s.toString()) * 1;
                    autoCalInrTxt.setText(String.valueOf(cal) + " (" + "INR" + ")");

                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    autoCalInrTxt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cryptoAmountTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    float cal = Float.parseFloat(s.toString()) * 10;
                    autocalXBoTxt.setText(String.valueOf(cal) + " ("+"XBO" +")");
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    autocalXBoTxt.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        copyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //copy wallet address
                // place your TextView's text in clipboard
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);
                clipboard.setText(walletaddressTxt.getText().toString().trim());

                Toast.makeText(getActivity(), "Wallet Address Copied", Toast.LENGTH_SHORT).show();
            }
        });

        //Payment Type Spinner
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //joiningList.add(new JoiningNameListModel("Please Select Joiner Person", "0"));
            JoiningAdapter = new ArrayAdapter<JoiningNameListModel>(getActivity(), R.layout.customizespinner, joiningList);
            JoiningAdapter.setDropDownViewResource(R.layout.customizespinner);
            joiningListSpiner.setAdapter(JoiningAdapter);
            joinerSpinnerForCrypto.setAdapter(JoiningAdapter);
        }else
            {
                //joiningList.add(new JoiningNameListModel("Please Select Joiner Person", "0"));
                JoiningAdapter = new ArrayAdapter<JoiningNameListModel>(getActivity(), R.layout.support_simple_spinner_dropdown_item, joiningList);
                JoiningAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                joiningListSpiner.setAdapter(JoiningAdapter);
                joinerSpinnerForCrypto.setAdapter(JoiningAdapter);
            }

        joiningListSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                joinIdStr = joiningList.get(position).getJoinerId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        joinerSpinnerForCrypto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                joinIdStr = joiningList.get(position).getJoinerId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //loadFragment(new AboutXBOFragment());
                if (checkClickLay.equalsIgnoreCase("Bank")) {

                    if (amountTxt.getText().toString().equalsIgnoreCase("")) {
                        amountTxt.setError("Please enter voucher");
                        amountTxt.requestFocus();
                    } else if (Double.parseDouble(amountTxt.getText().toString())<100)
                    {
                        amountTxt.setError("Please enter more than 100 voucher");
                        amountTxt.requestFocus();

                    }else if (paymentTypeTxt.getText().toString().equalsIgnoreCase("")) {
                        paymentTypeTxt.setError("Please enter payment type");
                        paymentTypeTxt.requestFocus();
                    } else if (refrenceNumberTxt.getText().toString().trim().equalsIgnoreCase("")) {
                        refrenceNumberTxt.setError("Please enter transaction reference number");
                        refrenceNumberTxt.requestFocus();

                    } else if (dateOfTransaction.getText().toString().equalsIgnoreCase(""))
                    {
                        Toast.makeText(getActivity(), "Please select date of transaction", Toast.LENGTH_SHORT).show();
                    }else if (joinIdStr.equalsIgnoreCase("0")) {
                        Toast.makeText(getActivity(), "Please Select Joining Person", Toast.LENGTH_SHORT).show();
                    } else if (checkClickLay.equalsIgnoreCase("")) {
                        Toast.makeText(getActivity(), "Please select funding type", Toast.LENGTH_SHORT).show();
                    } else {
                        if (conn.getConnectivityStatus() > 0) {
                           // loadFragment(new AboutXBOFragment());

                            setFunding(joinIdStr, amountTxt.getText().toString().trim(),
                                    paymentTypeTxt.getText().toString().trim(), refrenceCodeStr, userId, checkClickLay,
                                    refrenceNumberTxt.getText().toString().trim(),"",
                                    dateOfTransaction.getText().toString() +" 00:00:00",authKey);
                        } else {
                            conn.showNoInternetAlret();
                        }
                    }
                }else if (checkClickLay.equalsIgnoreCase("CryptoCurrency"))
                {
                    if (cryptoAmountTxt.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        cryptoAmountTxt.setError("Please enter amount");
                        cryptoAmountTxt.requestFocus();
                    }else if (cryptoTransactionIdTxt.getText().toString().trim().equalsIgnoreCase(""))
                    {
                        cryptoTransactionIdTxt.setError("Please enter transaction ID");
                        cryptoTransactionIdTxt.requestFocus();
                    }else if (joinIdStr.equalsIgnoreCase("0")) {
                        Toast.makeText(getActivity(), "Please Select Joining Person", Toast.LENGTH_SHORT).show();
                    }else
                        {

                          /*  Intent intent = new Intent(getActivity(), CryptoCurrencyPaymentGateway.class);
                            intent.putExtra("Amount", cryptoAmountTxt.getText().toString().trim());
                            intent.putExtra("JoinId", joinIdStr);
                            getActivity().startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.push_right_in, R.anim.push_left_out);*/
                            if (conn.getConnectivityStatus() > 0) {
                               // loadFragment(new AboutXBOFragment());

                                setFunding(joinIdStr, cryptoAmountTxt.getText().toString().trim(),
                                        "", refrenceCodeStr, userId, checkClickLay,
                                        cryptoTransactionIdTxt.getText().toString().trim(),
                                        senderAddresssTxt.getText().toString().trim(), currentDate(),authKey);
                            } else {
                                conn.showNoInternetAlret();
                            }
                        }
                }else
                    {
                        Toast.makeText(getActivity(), "Please select payment method", Toast.LENGTH_SHORT).show();
                    }
            }
        });



        convertQrCode();

        //call api for joiner name spinner
        if (conn.getConnectivityStatus()>0)
        {
            getJoiningPerson(userId,authKey);
        }else
            {
                conn.showNoInternetAlret();
            }




        return rootView;
    }

    public void convertQrCode()
    {
        String qrCodeStr = walletaddressTxt.getText().toString().trim();
       // MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
           /* BitMatrix bitMatrix = multiFormatWriter.encode(qrCodeStr, BarcodeFormat.QR_CODE,400,400);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);*/
            walletAddressQrImg.setImageBitmap(encodeAsBitmap(qrCodeStr,BarcodeFormat.QR_CODE,400,400));
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }


    private Fragment loadFragment(Fragment fragment) {

        Bundle bundle = new Bundle();
        bundle.putString("Amount", cryptoAmountTxt.getText().toString().trim());
        fragment.setArguments(bundle);

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_layout, fragment)
                .addToBackStack(null)
                .commit();
        return fragment;
    }


    /*API WORK*/
    public void getJoiningPerson(final String userId, final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait Loading...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, joingUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("Pyament Type", response);

                    if (joiningList.size()>0)
                    {
                        joiningList.clear();
                    }
                   // joiningList.add(new JoiningNameListModel("Please Select Joining Person","0"));

                    JSONObject jsonObject = new JSONObject(response);
                    String Status = jsonObject.getString("Status");
                    String Message = jsonObject.getString("Message");
                    if (Status.equalsIgnoreCase("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            String TypeId = object.getString("JoinId");
                            String TypeName = object.getString("Name");

                            joiningList.add(new JoiningNameListModel(TypeName, TypeId));
                        }
                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            conn.showLogoutDialog(Message, getActivity());
                        }
                    }else
                        {
                            Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                        }
                    JoiningAdapter.notifyDataSetChanged();
                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());

                pDialog.dismiss();


            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("ReferenceId", userId);
                params.put("LoginAuthKey",authKey);
                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");


    }

    public void setFunding(final String joinId, final String amount, final String paymentType,
                           final String refrenceCode,final String userId, final String fundingType,
                           final String transactionRefrenceNo,final String senderCryptoAddress,
                           final String time,final String authKey)
    {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Please Wait data is inserting...");
        pDialog.show();

        StringRequest historyInquiry = new StringRequest(
                Request.Method.POST, fundingUrl , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    Log.e("registerUser", response);
                    JSONObject jsonObject = new JSONObject(response);

                    String Message = jsonObject.getString("Message");
                    String Status = jsonObject.getString("Status");

                    if (Status.equalsIgnoreCase("1"))
                    {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_layout, new DashboardFragment())
                                .addToBackStack(null)
                                .commit();

                        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                    }else if (Status.equalsIgnoreCase("41"))
                    {
                        if(!((Activity) getActivity()).isFinishing()) {
                            conn.showLogoutDialog(Message, getActivity());
                        }
                    }else
                    {
                        Toast.makeText(getActivity(), Message, Toast.LENGTH_SHORT).show();
                    }

                    pDialog.dismiss();

                } catch (JSONException e) {
                    Log.e("checking json excption" , e.getMessage());
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("Login", "Error: " + error.getMessage());
                // Log.e("checking now ",error.getMessage());
                pDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("JoinId", joinId);
                params.put("amount", amount);
                params.put("paymentType", paymentType);
                params.put("referenceCode", refrenceCode);
                params.put("UserId", userId);
                params.put("fundingtype", fundingType);
                params.put("transactionreferenceno", transactionRefrenceNo);
                params.put("SenderCryptoAddress", senderCryptoAddress);
                params.put("TransactionDateTime", time);
                params.put("LoginAuthKey",authKey);

                Log.e("Checking type", params.toString());

                return params;
            }

        };
        historyInquiry.setRetryPolicy(new DefaultRetryPolicy(SettingConstant.Retry_Time,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(historyInquiry, "Register");

    }

    //GET CURRENT TIME
    public String currentDate() {
        //AM PM TIME
        /*"yyyy-MM-dd hh:mm:ss a"*/
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:MM:ss");
        return dateFormat.getDateTimeInstance().format(new Date());
    }


    public Bitmap encodeAsBitmap(String contents, BarcodeFormat format, int img_width, int img_height) throws WriterException {
        String contentsToEncode = contents;
        if (contentsToEncode == null) {
            return null;
        }
        // Map<EncodeHintType, Object>  = null;
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.MARGIN, new Integer(1));
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result;
        try {
            result = writer.encode(contentsToEncode, format, img_width, img_height, hints);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : Color.TRANSPARENT;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
