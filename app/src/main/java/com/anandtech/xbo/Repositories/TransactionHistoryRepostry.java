package com.anandtech.xbo.Repositories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;

import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.Activity.TransactionHistoryActivity;
import com.anandtech.xbo.Model.TransactionHistoryResultListModel;
import com.anandtech.xbo.Model.TransactionListModel;
import com.anandtech.xbo.R;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.anandtech.xbo.Source.SettingConstant;
import com.anandtech.xbo.Source.SharedPrefs;
import com.anandtech.xbo.Source.UtilsMethods;
import com.google.gson.Gson;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionHistoryRepostry
{
    private static TransactionHistoryRepostry instance;
    public MutableLiveData<TransactionListModel> data = new MutableLiveData<>();
    public static RetrofitInterface retrofitInterface;
    public ConnectionDetector conn;
    Activity activityObject;

    public static TransactionHistoryRepostry getInstance()
    {
        if (instance == null)
        {
            instance = new TransactionHistoryRepostry();
            retrofitInterface = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);
        }
        return instance;
    }

    public MutableLiveData<TransactionListModel> getTransactionHistory(String userId,String authkey, Activity activity)
    {
        this.activityObject = activity;
        conn = new ConnectionDetector(activityObject);

        if (conn.getConnectivityStatus()>0) {
            setTransactionList(userId, authkey, activityObject);
        }else
            {
                conn.showNoInternetAlret();
            }

        return data;
    }


    public void setTransactionList(String userId, String authKey, final Activity activity)
    {
        final ProgressDialog pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        Call<TransactionListModel> call = retrofitInterface.getTransactionHistory(userId, authKey);
        call.enqueue(new Callback<TransactionListModel>() {
            @Override
            public void onResponse(Call<TransactionListModel> call, Response<TransactionListModel> response) {


                try {
                    Log.e("TAG", "response 33: "+new Gson().toJson(response.body()) );
                    Log.e("Checking the response", response.body().getStatus() + " Null");
                    pDialog.dismiss();

                    if (response.body().getStatus().equalsIgnoreCase("1"))
                    {
                        data.setValue(response.body());
                    }else if (response.body().getStatus().equalsIgnoreCase("41"))
                    {
                        conn.showLogoutDialog(response.body().getMessage(), activity);
                    }else
                        {
                            Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TransactionListModel> call, Throwable t) {

                pDialog.dismiss();
            }
        });
    }

    //clear all holding references
    public void removeAllHoldingReference()
    {
        this.activityObject = null;
    }
}
