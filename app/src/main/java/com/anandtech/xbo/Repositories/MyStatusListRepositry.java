package com.anandtech.xbo.Repositories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;
import androidx.lifecycle.MutableLiveData;
import com.anandtech.xbo.Model.MyStatusDataModel;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.ConnectionDetector;
import com.google.gson.Gson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyStatusListRepositry
{
    private static MyStatusListRepositry instance;
    MutableLiveData<MyStatusDataModel> data = new MutableLiveData<>();
    private static RetrofitInterface retrofitInterface;
    private ConnectionDetector conn;
    private Activity activityObject;

    public static MyStatusListRepositry getInstance()
    {
        if (instance == null)
        {
            instance = new MyStatusListRepositry();
            retrofitInterface = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);
        }

        return instance;
    }

    public MutableLiveData<MyStatusDataModel> getData(String userId, String authKey, Activity activity)
    {
        this.activityObject = activity;

        conn = new ConnectionDetector(activityObject);

        if (conn.getConnectivityStatus()>0)
        {
            getStatusData(userId,authKey,activityObject);
        }else
            {
                conn.showNoInternetAlret();
            }
        return data;
    }

    private void getStatusData(String userId, String authKey, final Activity activity)
    {
        final ProgressDialog pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        Call<MyStatusDataModel> call = retrofitInterface.getStatusList(userId, authKey);
        call.enqueue(new Callback<MyStatusDataModel>() {
            @Override
            public void onResponse(Call<MyStatusDataModel> call, Response<MyStatusDataModel> response) {

                Log.e("TAG", "response 33: "+new Gson().toJson(response.body().getMyStatusList()) );

                try {

                    if (response.body().getStatus().equalsIgnoreCase("1"))
                    {
                       data.setValue(response.body());

                    }else if (response.body().getStatus().equalsIgnoreCase("41"))
                    {
                        if(!((Activity) activity).isFinishing()) {
                            conn.showLogoutDialog(response.body().getMessage(), activity);
                        }
                    }else if (response.body().getStatus().equalsIgnoreCase("2"))
                        {
                            data.setValue(response.body());
                            Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                } catch (NullPointerException e) {
                    //e.printStackTrace();
                    Toast.makeText(activity, "Server are not respond..", Toast.LENGTH_SHORT).show();

                }

                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MyStatusDataModel> call, Throwable t) {

                pDialog.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    //clear all holding references
    public void removeAllHoldingReference()
    {
        this.activityObject = null;
    }
}
