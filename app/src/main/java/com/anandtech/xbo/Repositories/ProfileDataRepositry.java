package com.anandtech.xbo.Repositories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.widget.Toast;
import androidx.lifecycle.MutableLiveData;
import com.anandtech.xbo.Model.ProfileModel;
import com.anandtech.xbo.RetrofitClasses.RetrofitClientInstance;
import com.anandtech.xbo.RetrofitClasses.RetrofitInterface;
import com.anandtech.xbo.Source.ConnectionDetector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileDataRepositry
{
    private static ProfileDataRepositry instance;
    MutableLiveData<ProfileModel> data = new MutableLiveData<>();
    private static RetrofitInterface retrofitInterface;
    private ConnectionDetector conn;
    public Activity activityObj;

    public static ProfileDataRepositry getInstance()
    {
        if (instance == null)
        {
            instance = new ProfileDataRepositry();
            retrofitInterface = RetrofitClientInstance.getRetrofitInstances().create(RetrofitInterface.class);
        }

        return instance;
    }

    public MutableLiveData<ProfileModel> getData(String userId, String authKey, Activity activity)
    {
        this.activityObj = activity;
        conn = new ConnectionDetector(activity);

        if (conn.getConnectivityStatus()>0)
        {
            getProfileData(userId,authKey,activity);
        }


        return data;
    }

    private void getProfileData(String userId, String authKey, final Activity activity)
    {
        final ProgressDialog pDialog = new ProgressDialog(activity);
        pDialog.setMessage("Please Wait...");
        pDialog.show();

        Call<ProfileModel> call = retrofitInterface.getProfileData(userId,authKey);
        call.enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {

                //Log.e("TAG", "response 33: "+new Gson().toJson(response.body().getResultModels()));

                try {

                    if (response.body().getStatus().equalsIgnoreCase("1"))
                    {
                        data.setValue(response.body());

                    }else if (response.body().getStatus().equalsIgnoreCase("41"))
                    {
                        if(!((Activity) activity).isFinishing()) {
                            conn.showLogoutDialog(response.body().getMessage(), activity);
                        }
                    }else if (response.body().getStatus().equalsIgnoreCase("2"))
                    {
                        data.setValue(response.body());
                        Toast.makeText(activity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException e) {
                    Toast.makeText(activity, "Server are not respond..", Toast.LENGTH_SHORT).show();

                }

                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {

                pDialog.dismiss();
                Toast.makeText(activity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    //clear all holding references
    public void removeAllHoldingReference()
    {
        this.activityObj = null;
    }
}
