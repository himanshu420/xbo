package com.anandtech.xbo.Source;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.anandtech.xbo.Activity.LoginActivity;
import com.anandtech.xbo.R;

public class ConnectionDetector extends Activity
{
    public final static int TYPE_WIFI = 1;
    public final static int TYPE_MOBILE = 2;
    public final static int TYPE_NOT_CONNECTED = 0;
    private Context _context;
    public ConnectionDetector(Context context){
        this._context = context;
    }
    public int getConnectivityStatus()
    {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context
                .CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork && activeNetwork.isConnected())
        {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;
            else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }
    public String getConnectivityStatusString()
    {
        int conn = getConnectivityStatus();
        String status = null;
        if (conn == ConnectionDetector.TYPE_WIFI)
        {
            status = "Wifi enabled";
        } else if (conn == ConnectionDetector.TYPE_MOBILE)
        {
            status = "Mobile data enabled";
        } else if (conn == ConnectionDetector.TYPE_NOT_CONNECTED)
        {
            status = "Not connected to Internet";
        }
        return status;
    }
    public void showNoInternetAlret(){
        final AlertDialog internet_dialog = new AlertDialog.Builder(
                _context).create();
        // Setting Dialog Title
        internet_dialog.setTitle("No Internet");
        internet_dialog.setCancelable(true);
        // Setting Dialog Message
        internet_dialog.setMessage("Active Internet Connection Required.");

        // Setting OK Button
        internet_dialog.setButton(Dialog.BUTTON_POSITIVE,"Setting", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                internet_dialog.dismiss();
                _context.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
            }
        });
        // Showing Alert Message
        internet_dialog.show();
    }


    public void showLogoutDialog(String message, final Activity activity) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        // Get the layout inflater
        LayoutInflater inflater = activity.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

                Intent intent = new Intent(activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
                activity.finish();

                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setLoginStatus(activity,
                        "0")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setVerified(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserId(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setRefrencId(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setEmailId(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setOldPassword(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserName(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAddress(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setWalletAmount(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setAuthKey(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setPassCode(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setFingerPrintEnable(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeName(activity,
                        "")));
                UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setUserTypeID(activity,
                        "")));


            }
        });
    }

    public void showSendBoxDialog(String message, final Activity activity) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity);
        // Get the layout inflater
        LayoutInflater inflater = activity.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.custome_dialog, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);

        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();
            }
        });
    }


}
