package com.anandtech.xbo.Source;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.anandtech.xbo.Model.CounterySpinnerModerl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPrefs
{
    private static SharedPreferences getSetting(Context context)
    {
        SharedPreferences sp = context.getSharedPreferences(
                SettingConstant.SP_NAME, Context.MODE_PRIVATE);
        return sp;
    }

    // Login Status
    public static String getLoginStatus(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.LoginStatus, null);
    }
    public static boolean setLoginStatus(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.LoginStatus, authKey);
        return editor.commit();
    }

    //userId
    public static String getUserId(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.UserId, null);
    }
    public static boolean setUserId(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.UserId, authKey);
        return editor.commit();
    }

    //save user Name
    public static String getUserName(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.userName, null);
    }
    public static boolean setUserName(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.userName, authKey);
        return editor.commit();
    }

    //Refrence Id
    public static String getRefrenceId(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.RefrenceId, null);
    }
    public static boolean setRefrencId(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.RefrenceId, authKey);
        return editor.commit();
    }

    //Verified Id
    public static String getVerified(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.Verified, null);
    }
    public static boolean setVerified(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.Verified, authKey);
        return editor.commit();
    }

    //Email  Id
    public static String getEmailId(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.EmailId, null);
    }
    public static boolean setEmailId(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.EmailId, authKey);
        return editor.commit();
    }

    //save old password
    public static String getOldPassword(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.oldPassword, null);
    }
    public static boolean setOldPassword(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.oldPassword, authKey);
        return editor.commit();
    }

    //share refer Code
    public static String getShareRefer(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.ShareRefer, null);
    }
    public static boolean setShareRefer(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.ShareRefer, authKey);
        return editor.commit();
    }

    //WalletAddress
    public static String getWalletAddress(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.WalletAddress, null);
    }
    public static boolean setWalletAddress(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.WalletAddress, authKey);
        return editor.commit();
    }

    //wallet Amount
    public static String getWalletAmount(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.WalletAmount, null);
    }
    public static boolean setWalletAmount(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.WalletAmount, authKey);
        return editor.commit();
    }

    //Auth key
    public static String getAuthKey(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.AuthKey, null);
    }
    public static boolean setAuthKey(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.AuthKey, authKey);
        return editor.commit();
    }

    //Screen On
    public static String getScreenStatus(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.Screen, null);
    }
    public static boolean setScreenStatus(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.Screen, authKey);
        return editor.commit();
    }

    //set passcode
    public static String getPassCode(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.passCode, null);
    }
    public static boolean setPassCode(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.passCode, authKey);
        return editor.commit();
    }

    //set fingerPrint enable
    public static String getFingerPrintEnable(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.fingerPrintEnable, null);
    }
    public static boolean setFingerPrintEnable(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.fingerPrintEnable, authKey);
        return editor.commit();
    }

    //set usertype name
    public static String getUserTypeName(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.UserTypeName, null);
    }
    public static boolean setUserTypeName(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.UserTypeName, authKey);
        return editor.commit();
    }

    //set user type id
    public static String getUserTypeID(Context context)
    {
        SharedPreferences sp = getSetting(context);
        return  sp.getString(SettingConstant.UserTypeId, null);
    }
    public static boolean setUserTypeID(Context context, String authKey)
    {
        SharedPreferences sp = getSetting(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(SettingConstant.UserTypeId, authKey);
        return editor.commit();
    }

    //save Country spinner
    public static ArrayList<CounterySpinnerModerl> getCountryList(Context context)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = sharedPrefs.getString(SettingConstant.CountryList, "");
        Type type = new TypeToken<ArrayList<CounterySpinnerModerl>>() {}.getType();
        ArrayList<CounterySpinnerModerl> arrayList = gson.fromJson(json, type);
        return arrayList;
    }
    public static void setCountryList(Context context, ArrayList<CounterySpinnerModerl> arrayList)
    {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        Gson gson = new Gson();

        String json = gson.toJson(arrayList);

        editor.putString(SettingConstant.CountryList, json);
        editor.commit();
    }

}
