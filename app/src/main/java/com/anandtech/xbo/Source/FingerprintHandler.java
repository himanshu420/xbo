package com.anandtech.xbo.Source;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


import com.anandtech.xbo.Activity.FingerPrintEnableActivity;
import com.anandtech.xbo.Activity.PassCodeActivity;
import com.anandtech.xbo.R;

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {


    private Activity context;
    public int count = 0;

    // Constructor
    public FingerprintHandler(Activity mContext) {
        context = mContext;
    }


    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString, false);

        Log.e("Checking the message", errString.toString());

        if(!((Activity) context).isFinishing())
        {
            //show dialog
            showFingerNotAuth(errString.toString());
        }
    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Fingerprint Authentication help\n" + helpString, false);

        if(!((Activity) context).isFinishing())
        {
            //show dialog
            showFingerNotAuth(helpString.toString());
        }
    }


    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);

        count++;

        if (count == 3)
        {
            /*Intent intent = new Intent(context, PassCodeActivity.class);
            context.startActivity(intent);
            context.finish();*/

            if(!((Activity) context).isFinishing())
            {
                //show dialog
                showFingerNotAuth("You have tried maximum attempt! Please go to passcode and enter your 4-digit key to access this page");

            }


        }
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);

    }


    public void update(String e, Boolean success){
        TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
        textView.setText(e);
        if(success){
            textView.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
            UtilsMethods.getBlankIfStringNull(String.valueOf(SharedPrefs.setScreenStatus(context,
                    "False")));
            context.finish();
        }
    }

    private void showFingerNotAuth(String message) {

        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(context);
        // Get the layout inflater
        LayoutInflater inflater = context.getLayoutInflater();
        // Inflate the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.fingerprint_popup, null);

        TextView msgTxt = (TextView)view.findViewById(R.id.msg_txt);
        Button okBtn = (Button) view.findViewById(R.id.buttonOk);

        msgTxt.setText(message);


        // Set the dialog layout
        builder.setView(view);
        builder.create();

        final androidx.appcompat.app.AlertDialog ad = builder.show();
        ad.setCancelable(false);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ad.dismiss();

                Intent intent = new Intent(context, PassCodeActivity.class);
                intent.putExtra("Nav","1");
                context.startActivity(intent);
                context.finish();


            }
        });
    }

}
