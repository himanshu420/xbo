package com.anandtech.xbo.Model;

import androidx.annotation.NonNull;

public class StateListModel
{
    public String stateId;
    public String stateName;

    public StateListModel(String stateId, String stateName) {
        this.stateId = stateId;
        this.stateName = stateName;
    }

    public String getStateId() {
        return stateId;
    }

    public String getStateName() {
        return stateName;
    }

    @NonNull
    @Override
    public String toString() {
        return getStateName();
    }
}
