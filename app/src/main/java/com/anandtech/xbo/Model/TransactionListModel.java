package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class TransactionListModel
{
    @SerializedName("updatedbalance")
    @Expose
    public String updateBlnc;

    @SerializedName("Message")
    @Expose
    public String message;

    @SerializedName("Status")
    @Expose
    public String status;

    @SerializedName("result")
    @Expose
    public ArrayList<TransactionHistoryResultListModel> result;

    public String getUpdateBlnc() {
        return updateBlnc;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<TransactionHistoryResultListModel> getResult() {
        return result;
    }

}
