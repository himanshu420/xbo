package com.anandtech.xbo.Model;

import androidx.annotation.NonNull;

public class CitySpinnerModel
{
    public String cityId;
    public String cityName;

    public CitySpinnerModel(String cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public String getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    @NonNull
    @Override
    public String toString() {
        return getCityName();
    }
}
