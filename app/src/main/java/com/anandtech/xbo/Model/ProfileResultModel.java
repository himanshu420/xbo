package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResultModel
{
    @SerializedName("uniqecode")
    @Expose
    String uniqecode;

    @SerializedName("UserId")
    @Expose
    String UserId;

    @SerializedName("Name")
    @Expose
    String Name;

    @SerializedName("EmailId")
    @Expose
    String EmailId;

    @SerializedName("address1")
    @Expose
    String Address;

    @SerializedName("PhoneNumber")
    @Expose
    String PhoneNumber;

    @SerializedName("Idproof1")
    @Expose
    String Idproof1;

    @SerializedName("Idproof2")
    @Expose
    String Idproof2;

    @SerializedName("CancelCheque")
    @Expose
    String CancelCheque;

    @SerializedName("ProfilePicture")
    @Expose
    String ProfilePicture;

    @SerializedName("VerificationStatus")
    @Expose
    String VerificationStatus;

    @SerializedName("CountryName")
    @Expose
    String CountryName;

    @SerializedName("StateName")
    @Expose
    String StateName;

    @SerializedName("CityName")
    @Expose
    String CityName;

    @SerializedName("amount")
    @Expose
    String amount;

    @SerializedName("TotalXboToken")
    @Expose
    String TotalXboToken;

    @SerializedName("BankName")
    @Expose
    String BankName;

    @SerializedName("AccountNo")
    @Expose
    String AccountNo;

    @SerializedName("IFSCcode")
    @Expose
    String IFSCcode;

    @SerializedName("BankBranchName")
    @Expose
    String BankBranchName;

    @SerializedName("Dob")
    @Expose
    String Dob;

    @SerializedName("Plot_ROI")
    @Expose
    String ROI;

    public String getROI() {
        return ROI;
    }

    public String getUniqecode() {
        return uniqecode;
    }

    public String getUserId() {
        return UserId;
    }

    public String getName() {
        return Name;
    }

    public String getEmailId() {
        return EmailId;
    }

    public String getAddress() {
        return Address;
    }

    public String getDob() {
        return Dob;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public String getIdproof1() {
        return Idproof1;
    }

    public String getIdproof2() {
        return Idproof2;
    }

    public String getCancelCheque() {
        return CancelCheque;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public String getVerificationStatus() {
        return VerificationStatus;
    }

    /*public String getCountryID() {
        return CountryID;
    }

    public String getStateID() {
        return StateID;
    }

    public String getCityID() {
        return CityID;
    }*/

    public String getCountryName() {
        return CountryName;
    }

    public String getStateName() {
        return StateName;
    }

    public String getCityName() {
        return CityName;
    }

    public String getAmount() {
        return amount;
    }

    public String getTotalXboToken() {
        return TotalXboToken;
    }

    public String getBankName() {
        return BankName;
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public String getIFSCcode() {
        return IFSCcode;
    }

    public String getBankBranchName() {
        return BankBranchName;
    }
}
