package com.anandtech.xbo.Model;

public class TransactionHistoryModel
{
    public String date;
    String amount;
    String refNo;
    String paymentType;

    public TransactionHistoryModel(String date, String amount, String refNo, String paymentType) {
        this.date = date;
        this.amount = amount;
        this.refNo = refNo;
        this.paymentType = paymentType;
    }

    public String getDate() {
        return date;
    }

    public String getAmount() {
        return amount;
    }

    public String getRefNo() {
        return refNo;
    }

    public String getPaymentType() {
        return paymentType;
    }
}
