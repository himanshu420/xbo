package com.anandtech.xbo.Model;

public class MyStackListModel
{
    String joinId ;
    String Name ;
    String EmailId ;
    String PhoneNumber;
    String amount;
    String ProfilePicture;
    String ReferenceId ;
    String CountryName;
    String StateName;
    String CityName;
    String TotalXboToken;
    String WalletAddress;
    String CreatedDateTime;
    String RefrenceUserName;
    String AgentReferenceId;

    public MyStackListModel(String joinId, String name, String emailId, String phoneNumber, String amount, String profilePicture, String referenceId, String countryName, String stateName, String cityName, String totalXboToken,
                            String walletAddress, String createdDateTime, String RefrenceUserName, String AgentReferenceId) {
        this.joinId = joinId;
        Name = name;
        EmailId = emailId;
        PhoneNumber = phoneNumber;
        this.amount = amount;
        ProfilePicture = profilePicture;
        ReferenceId = referenceId;
        CountryName = countryName;
        StateName = stateName;
        CityName = cityName;
        TotalXboToken = totalXboToken;
        WalletAddress = walletAddress;
        CreatedDateTime = createdDateTime;
        this.RefrenceUserName = RefrenceUserName;
        this.AgentReferenceId = AgentReferenceId;
    }

    public String getAgentReferenceId() {
        return AgentReferenceId;
    }

    public String getRefrenceUserName() {
        return RefrenceUserName;
    }

    public String getJoinId() {
        return joinId;
    }

    public String getName() {
        return Name;
    }

    public String getEmailId() {
        return EmailId;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public String getAmount() {
        return amount;
    }

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public String getCountryName() {
        return CountryName;
    }

    public String getStateName() {
        return StateName;
    }

    public String getCityName() {
        return CityName;
    }

    public String getTotalXboToken() {
        return TotalXboToken;
    }

    public String getWalletAddress() {
        return WalletAddress;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }
}
