package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MyStatusDataModel
{
    @SerializedName("Message")
    @Expose
    public String Message;

    @SerializedName("Status")
    @Expose
    public String Status;

    @SerializedName("result")
    @Expose
    public ArrayList<MyStatusListDataModel> myStatusList;

    public String getMessage() {
        return Message;
    }

    public String getStatus() {
        return Status;
    }

    public ArrayList<MyStatusListDataModel> getMyStatusList() {
        return myStatusList;
    }
}
