package com.anandtech.xbo.Model;

import android.graphics.Bitmap;

public class ImageListModel
{
    public String imageBase64;
    public Bitmap image;

    public ImageListModel(String imageBase64, Bitmap image) {
        this.imageBase64 = imageBase64;
        this.image = image;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getImageBase64() {
        return imageBase64;
    }
}
