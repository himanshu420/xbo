package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyStatusListDataModel
{

    @SerializedName("joinId")
    @Expose
    public String joinId;

    @SerializedName("Name")
    @Expose
    public String Name;

    @SerializedName("EmailId")
    @Expose
    public String EmailId;

    @SerializedName("PhoneNumber")
    @Expose
    public String PhoneNumber;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("ReferenceId")
    @Expose
    public String ReferenceId;

    @SerializedName("CountryName")
    @Expose
    public String CountryName;

    @SerializedName("StateName")
    @Expose
    public String StateName;

    @SerializedName("CityName")
    @Expose
    public String CityName;

    @SerializedName("TotalXboToken")
    @Expose
    public String TotalXboToken;

    @SerializedName("WalletAddress")
    @Expose
    public String WalletAddress;

    @SerializedName("CreatedDateTime")
    @Expose
    public String CreatedDateTime;

    @SerializedName("RefrenceUserName")
    @Expose
    public String RefrenceUserName;

    @SerializedName("AgentReferenceId")
    @Expose
    public String AgentReferenceId;

    @SerializedName("ProfilePicture")
    @Expose
    public String ProfilePicture;

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public String getJoinId() {
        return joinId;
    }

    public String getName() {
        return Name;
    }

    public String getEmailId() {
        return EmailId;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public String getAmount() {
        return amount;
    }

    public String getReferenceId() {
        return ReferenceId;
    }

    public String getCountryName() {
        return CountryName;
    }

    public String getStateName() {
        return StateName;
    }

    public String getCityName() {
        return CityName;
    }

    public String getTotalXboToken() {
        return TotalXboToken;
    }

    public String getWalletAddress() {
        return WalletAddress;
    }

    public String getCreatedDateTime() {
        return CreatedDateTime;
    }

    public String getRefrenceUserName() {
        return RefrenceUserName;
    }

    public String getAgentReferenceId() {
        return AgentReferenceId;
    }
}
