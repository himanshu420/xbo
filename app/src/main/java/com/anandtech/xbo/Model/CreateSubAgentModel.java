package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateSubAgentModel
{
    @SerializedName("Message")
    @Expose
    String message;

    @SerializedName("Status")
    @Expose
    String status;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }
}
