package com.anandtech.xbo.Model;

import androidx.annotation.NonNull;

public class CounterySpinnerModerl
{
    public String counteryId;
    public String counteryName;

    public CounterySpinnerModerl(String counteryId, String counteryName) {
        this.counteryId = counteryId;
        this.counteryName = counteryName;
    }

    public String getCounteryId() {
        return counteryId;
    }

    public String getCounteryName() {
        return counteryName;
    }

    @NonNull
    @Override
    public String toString() {
        return getCounteryName();
    }
}
