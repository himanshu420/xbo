package com.anandtech.xbo.Model;

import androidx.annotation.NonNull;

public class JoiningNameListModel
{
    public String joinerName;
    public String joinerId;

    public JoiningNameListModel(String joinerName, String joinerId) {
        this.joinerName = joinerName;
        this.joinerId = joinerId;
    }

    public String getJoinerName() {
        return joinerName;
    }

    public String getJoinerId() {
        return joinerId;
    }

    @NonNull
    @Override
    public String toString() {
        return getJoinerName();
    }
}
