package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PasscodeModel
{
    @SerializedName("Status")
    @Expose
    public String Status;

    @SerializedName("Message")
    @Expose
    public String Message;

    public String getStatus() {
        return Status;
    }

    public String getMessage() {
        return Message;
    }
}
