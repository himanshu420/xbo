package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppVersionModel
{
    @SerializedName("AppVrsion")
    @Expose
    public String appVersion;

    @SerializedName("Status")
    @Expose
    public String status;

    public String getAppVersion() {
        return appVersion;
    }

    public String getStatus() {
        return status;
    }
}
