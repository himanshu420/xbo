package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileModel
{
    @SerializedName("Message")
    @Expose
    String message;

    @SerializedName("Status")
    @Expose
    String status;

    @SerializedName("result")
    @Expose
    public ProfileResultModel resultModels;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public ProfileResultModel getResultModels() {
        return resultModels;
    }
}
