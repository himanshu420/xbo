package com.anandtech.xbo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TransactionHistoryResultListModel
{
    @SerializedName("transId")
    @Expose
    public String transactionId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("RemainingAmount")
    @Expose
    public String remaningAmount;

    @SerializedName("CreatedDatetime")
    @Expose
    public String createDateTime;

    @SerializedName("TransactionType")
    @Expose
    public String transactionType;

    @SerializedName("PaymentStatus")
    @Expose
    public String paymentStatus;

    @SerializedName("FundingType")
    @Expose
    public String FundingType;

    @SerializedName("transactionreferenceno")
    @Expose
    public String transactionreferenceno;

    @SerializedName("paymentType")
    @Expose
    public String paymentType;

    @SerializedName("WalletAddress")
    @Expose
    public String walletAddress;

    public String getWalletAddress() {
        return walletAddress;
    }

    public String getFundingType() {
        return FundingType;
    }

    public String getTransactionreferenceno() {
        return transactionreferenceno;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }

    public String getRemaningAmount() {
        return remaningAmount;
    }

    public String getCreateDateTime() {
        return createDateTime;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }
}
