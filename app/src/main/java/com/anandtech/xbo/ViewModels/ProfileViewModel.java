package com.anandtech.xbo.ViewModels;

import android.app.Activity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.anandtech.xbo.Model.ProfileModel;
import com.anandtech.xbo.Repositories.ProfileDataRepositry;

public class ProfileViewModel extends ViewModel
{
    private MutableLiveData<ProfileModel> profileData;
    private ProfileDataRepositry mRepo;

    public void init(String userId, String authKey, Activity activity)
    {
        mRepo = ProfileDataRepositry.getInstance();
        profileData = mRepo.getData(userId, authKey, activity);
    }

    public LiveData<ProfileModel> getProfileLiveData()
    {
        return profileData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepo.removeAllHoldingReference();
    }
}
