package com.anandtech.xbo.ViewModels;

import android.app.Activity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.anandtech.xbo.Model.TransactionHistoryResultListModel;
import com.anandtech.xbo.Model.TransactionListModel;
import com.anandtech.xbo.Repositories.TransactionHistoryRepostry;
import java.util.ArrayList;

public class TransactionHistoryViewModel extends ViewModel
{
    private MutableLiveData<TransactionListModel> transactionHistoryLiveData;
    private TransactionHistoryRepostry mRepo;

    public void init(String userId, String authKey, Activity activity)
    {
        if (transactionHistoryLiveData != null)
        {
            return;
        }
        mRepo = TransactionHistoryRepostry.getInstance();
        transactionHistoryLiveData = mRepo.getTransactionHistory(userId, authKey,activity);
    }

    public LiveData<TransactionListModel> getTransactionHistoryData()
    {
        return transactionHistoryLiveData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepo.removeAllHoldingReference();
    }
}
