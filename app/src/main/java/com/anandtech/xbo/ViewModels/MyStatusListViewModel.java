package com.anandtech.xbo.ViewModels;

import android.app.Activity;
import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.anandtech.xbo.Model.MyStatusDataModel;
import com.anandtech.xbo.Repositories.MyStatusListRepositry;

public class MyStatusListViewModel extends ViewModel
{
    private MutableLiveData<MyStatusDataModel> myStatusData;
    private MyStatusListRepositry mRepo;

    public void init(String userId, String authKey, Activity activity)
    {
        mRepo = MyStatusListRepositry.getInstance();
        myStatusData = mRepo.getData(userId,authKey,activity);
    }

    public LiveData<MyStatusDataModel> getStatusList()
    {
        return  myStatusData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mRepo.removeAllHoldingReference();
    }
}
